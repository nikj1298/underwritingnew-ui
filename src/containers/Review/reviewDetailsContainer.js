import { connect } from 'react-redux'
import ReviewDetailsComponent from "../../components/Review/reviewDetailsComponent"

const mapStateToProps = (state) => {
    return {}
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}

const ReviewDetailsContainer = connect(mapStateToProps, mapDispatchToProps)(ReviewDetailsComponent)
export default ReviewDetailsContainer