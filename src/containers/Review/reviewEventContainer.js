import { connect } from 'react-redux'
import ReviewLandingPopup from "../../components/Review/reviewLandingPopup"
import {
  getAllEventsList, getAllEventsListSuccess, getAllEventsListFailure,
  updateGetAllEventsParam, updateGetPropertyListParam,
  fetchEventProperties, fetchEventPropertiesSuccess, fetchEventPropertiesFailure
} from "../../actions/Review/reviewListAction"
import { showLoader, updateUrl } from "../../actions/appAction"

const mapStateToProps = (state) => {
  return {
    getAllEvents: state.reviewListReducer.getAllEvents,
    allEventParam: state.reviewListReducer.allEventParam,
    getPropertiesParam: state.reviewListReducer.getPropertiesParam,
    showLoaderStatus: state.appReducer.loaderStatus,
    getPropertiesList: state.reviewListReducer.getPropertiesList,
    lastVisitedUrl: state.appReducer.lastVisitedUrl
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    /**
    * function to fetch all events list for review pop-up
    */
    getAllEventsList: (searchParam) => {
      return new Promise((resolve, reject) => {
        dispatch(getAllEventsList(searchParam)).then((response) => {
          dispatch(getAllEventsListSuccess(response.payload.data))
          resolve(response.payload.data)
        }).catch((error) => {
          dispatch(getAllEventsListFailure(error))
          reject(error)
        })
      })
    },
    /**
     * update event list param for review pop-up drop-down 
     */
    updateGetAllEventsParam: (updatedParam) => {
      dispatch(updateGetAllEventsParam(updatedParam))
    },
    /**
    * update filter property list param
    */
    updateGetPropertyListParam: (updatedParam) => {
      dispatch(updateGetPropertyListParam(updatedParam))
    },
    /**
    * gets property list on the basis of event selected and other filter applied
    */
    fetchEventProperties: (searchParam) => {
      dispatch(showLoader(true))
      return new Promise((resolve, reject) => {
        dispatch(fetchEventProperties(searchParam)).then((response) => {
          dispatch(fetchEventPropertiesSuccess(response.payload.data))
          dispatch(showLoader(false))
          resolve(response.payload.data)
        }).catch((error) => {
          dispatch(fetchEventPropertiesFailure(error))
          dispatch(showLoader(false))
          reject(error)
        })
      })
    },
    /**
     * updates the last visited url
     */
    updateUrl: (updatedUrl) => {
      dispatch(updateUrl(updatedUrl))
    }
  }
}

const ReviewEventContainer = connect(mapStateToProps, mapDispatchToProps)(ReviewLandingPopup)
export default ReviewEventContainer
