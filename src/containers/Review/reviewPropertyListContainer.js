import { connect } from 'react-redux'
import ReviewListComponent from "../../components/Review/reviewListComponent"
import {
  getAllEventsList, getAllEventsListSuccess, getAllEventsListFailure,
  updateGetAllEventsParam, updateGetPropertyListParam,
  fetchEventProperties, fetchEventPropertiesSuccess, fetchEventPropertiesFailure,
  updateEventPropertyPageNumber, fetchAllLevels, fetchAllLevelsSuccess, fetchAllLevelsFailure,
  getAllGeneralLandUseCode, getAllGeneralLandUseCodeSuccess, getAllGeneralLandUseCodeFailure,
  getAllInternalLandUseCode, getAllInternalLandUseCodeSuccess, getAllInternalLandUseCodeFailure
} from "../../actions/Review/reviewListAction"
import { showLoader, updateUrl } from "../../actions/appAction"

const mapStateToProps = (state) => {
  return {
    getAllEvents: state.reviewListReducer.getAllEvents,
    allEventParam: state.reviewListReducer.allEventParam,
    getPropertiesParam: state.reviewListReducer.getPropertiesParam,
    getPropertiesList: state.reviewListReducer.getPropertiesList,
    eventPropertyPageNumber: state.reviewListReducer.eventPropertyPageNumber,
    eventPropertyCount: state.reviewListReducer.getPropertiesList ? state.reviewListReducer.getPropertiesList.eventPropertyCount : 0,
    showLoaderStatus: state.appReducer.loaderStatus,
    lastVisitedUrl: state.appReducer.lastVisitedUrl,
    allLevelList: state.reviewListReducer.allLevelList,
    allGeneralLandUseCode: state.reviewListReducer.allGeneralLandUseCode,
    allInternalLandUseCode: state.reviewListReducer.allInternalLandUseCode
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    /**
     * function to fetch all events list for review pop-up
     */
    getAllEventsList: (searchParam) => {
      return new Promise((resolve, reject) => {
        dispatch(getAllEventsList(searchParam)).then((response) => {
          dispatch(getAllEventsListSuccess(response.payload.data))
          resolve(response.payload.data)
        }).catch((error) => {
          dispatch(getAllEventsListFailure(error))
          reject(error)
        })
      })
    },
    /**
     * update event list param for review pop-up drop-down 
     */
    updateGetAllEventsParam: (updatedParam) => {
      dispatch(updateGetAllEventsParam(updatedParam))
    },

    /**
     * update filter property list param
     */
    updateGetPropertyListParam: (updatedParam) => {
      dispatch(updateGetPropertyListParam(updatedParam))
    },

    /**
     * gets property list on the basis of event selected and other filter applied
     */
    fetchEventProperties: (searchParam) => {
      dispatch(showLoader(true))
      return new Promise((resolve, reject) => {
        dispatch(fetchEventProperties(searchParam)).then((response) => {
          dispatch(fetchEventPropertiesSuccess(response.payload.data))
          dispatch(showLoader(false))
          resolve(response.payload.data)
        }).catch((error) => {
          dispatch(fetchEventPropertiesFailure(error))
          dispatch(showLoader(false))
          reject(error)
        })
      })
    },
    /**
     * updates the page number of properties list
     */
    updateEventPropertyPageNumber: (pageNumber) => {
      dispatch(updateEventPropertyPageNumber({ pageNumber }))
    },
    /**
     * updates the last visited url
     */
    updateUrl: (updatedUrl) => {
      dispatch(updateUrl(updatedUrl))
    },
    /**
     * fetch level list on the basis of selected event Id
     */
    fetchAllLevels: (eventId) => {
      return new Promise((resolve, reject) => {
        dispatch(fetchAllLevels(eventId)).then((response) => {
          dispatch(fetchAllLevelsSuccess(response.payload.data))
          resolve(response.payload.data)
        }).catch((error) => {
          dispatch(fetchAllLevelsFailure(error))
          reject(error)
        })
      })
    },
    /**
     * fetch all general land use code list
     */
    getAllGeneralLandUseCode: () => {
      return new Promise((resolve, reject) => {
        dispatch(getAllGeneralLandUseCode()).then((response) => {
          dispatch(getAllGeneralLandUseCodeSuccess(response.payload.data))
          resolve(response.payload.data)
        }).catch((error) => {
          dispatch(getAllGeneralLandUseCodeFailure(error))
          reject(error)
        })
      })
    },
    /**
    * fetch all internal land use code list
    */
    getAllInternalLandUseCode: () => {
      return new Promise((resolve, reject) => {
        dispatch(getAllInternalLandUseCode()).then((response) => {
          dispatch(getAllInternalLandUseCodeSuccess(response.payload.data))
          resolve(response.payload.data)
        }).catch((error) => {
          dispatch(getAllInternalLandUseCodeFailure(error))
          reject(error)
        })
      })
    }
  }
}

const ReviewPropertyListContainer = connect(mapStateToProps, mapDispatchToProps)(ReviewListComponent)
export default ReviewPropertyListContainer