import { connect } from 'react-redux'
import EventDetailsFormComponent from './../../components/EventsDetails/eventDetailsFormComponent'
import {
    getRulesList, getRulesListSuccess, getRulesListFailure,
    postUpdateAppliedRuled, postUpdateAppliedRuledSuccess, postUpdateAppliedRuledFailure,
    getRulesPossiblityList, getRulesPossiblityListSuccess, getRulesPossiblityListFailure,
    getRulesAprroveReject, getRulesAprroveRejectSuccess, getRulesAprroveRejectFailure,
    postsaveNewDataRules, postsaveNewDataRulesSuccess, postsaveNewDataRulesFailure 
} from './../../actions/EventsDetails/eventDetailsFormActions';
import { showLoader } from "../../actions/appAction"

const mapStateToProps = (state) => {
    return {
        showLoaderStatus: state.appReducer.loaderStatus,
        rulesList: state.eventDetailsFormReducer.rulesList,
        rulesTypesList :  state.eventDetailsFormReducer.rulesTypesList
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAllRulesList: (eventIdSelected) => {
            dispatch(showLoader(true))
            return new Promise((resolve, reject) => {
                dispatch(getRulesList(eventIdSelected)).then((response) => {
                    dispatch(getRulesListSuccess(response.payload));
                    dispatch(showLoader(false))
                    resolve(response.payload);
                })
                    .catch((error) => {
                        dispatch(showLoader(false))
                        dispatch(getRulesListFailure(error));
                    });
            });
        },
        postUpdateAppliedRuled: (eventIdSelected, data) => {
            dispatch(showLoader(true))
            return new Promise((resolve, reject) => {
                dispatch(postUpdateAppliedRuled(eventIdSelected, data)).then((response) => {
                    dispatch(postUpdateAppliedRuledSuccess(response.payload));
                    dispatch(showLoader(false))
                    resolve(response.payload);
                })
                    .catch((error) => {
                        dispatch(showLoader(false))
                        dispatch(postUpdateAppliedRuledFailure(error));
                    });
            });
        },
        getRulesPossiblityListRules: () => {
            return new Promise((resolve, reject) => {
                dispatch(getRulesPossiblityList()).then((response) => {
                    dispatch(getRulesPossiblityListSuccess(response.payload));
                    resolve(response.payload);
                })
                    .catch((error) => {
                        dispatch(getRulesPossiblityListFailure(error));
                    });
            });
        },
        getRulesAprroveRejectTypes: () => {
            return new Promise((resolve, reject) => {
                dispatch(getRulesAprroveReject()).then((response) => {
                    dispatch(getRulesAprroveRejectSuccess(response.payload));
                    resolve(response.payload);
                })
                    .catch((error) => {
                        dispatch(getRulesAprroveRejectFailure(error));
                    });
            });
        },
        postsaveAllNewDataRules: (eventIdSelected,data) => {
            return new Promise((resolve, reject) => {
                dispatch(postsaveNewDataRules(eventIdSelected,data)).then((response) => {
                    dispatch(postsaveNewDataRulesSuccess(response.payload));
                    resolve(response.payload);
                })
                    .catch((error) => {
                        dispatch(postsaveNewDataRulesFailure(error));
                    });
            });
        },
    }
}

const EventDetailsFormContainer = connect(mapStateToProps, mapDispatchToProps)(EventDetailsFormComponent)
export default EventDetailsFormContainer