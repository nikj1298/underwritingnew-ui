import { connect } from 'react-redux'
import TaskComponent from "../../components/EventsDetails/taskComponent"
import {
  getTasksList, getTasksListSuccess, getTasksListFailure 
} from './../../actions/EventsDetails/taskActions';
import { showLoader } from "../../actions/appAction";

const mapStateToProps = (state) => {
  return {
    taskList: state.taskReducer.taskList,
    showLoaderStatus: state.appReducer.loaderStatus
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTasksList: (data) => {
      dispatch(showLoader(true))
      return new Promise((resolve, reject) => {
        dispatch(getTasksList(data)).then((response) => {
          dispatch(getTasksListSuccess(response.payload));
          dispatch(showLoader(false));
          resolve(response.payload);
        })
          .catch((error) => {
            dispatch(showLoader(false));
            dispatch(getTasksListFailure(error));
          });
      });
    },
  }
}

const TaskContainer = connect(mapStateToProps, mapDispatchToProps)(TaskComponent)
export default TaskContainer