import { connect } from 'react-redux'
import EventDetailsComponent from "../../components/EventsDetails/eventDetailsComponent"
import { updateUrl } from "../../actions/appAction"

const mapStateToProps = (state) => {
  return {
    lastVisitedUrl: state.appReducer.lastVisitedUrl

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    /**
     * updates the last visited url
     */
    updateUrl: (updatedUrl) => {
      dispatch(updateUrl(updatedUrl))
    }
  }
}

const EventDetailsContainer = connect(mapStateToProps, mapDispatchToProps)(EventDetailsComponent)
export default EventDetailsContainer