import { connect } from 'react-redux'
import EventListComponent from "../../components/Events/eventListComponent"
import {
  getAllStatesList, getAllStatesListSuccess, getAllStatesListFailure,
  getEventTypes, getEventTypesSuccess, getEventTypesFailure,
  getEventList, getEventListSuccess, getEventListFailure,
  updateEventsPageNumber, updateSearchEventParam, updateReviewReportData,
  validateReviewReportData, generateReviewReport,
  generateReviewReportSuccess, generateReviewReportFailure,
  getReviewReportUrl, getReviewReportUrlSuccess, getReviewReportUrlFailure,
  downloadGeneratedReport
} from './../../actions/Events/eventsListAction';
import { showLoader, updateUrl } from "../../actions/appAction"

const mapStateToProps = (state) => {
  return {
    showLoaderStatus: state.appReducer.loaderStatus,
    allStatesList: state.eventsListReducer.allStatesList,
    eventTypesList: state.eventsListReducer.eventTypesList,
    eventList: state.eventsListReducer.eventList.eventList,
    searchEventParam: state.eventsListReducer.searchEventParam,
    eventPageNumber: state.eventsListReducer.eventPageNumber,
    eventsCount: state.eventsListReducer.eventList.eventsCount,
    reviewReportData: state.eventsListReducer.reviewReportData,
    validateReviewReport: state.eventsListReducer.validateReviewReport,
    lastVisitedUrl: state.appReducer.lastVisitedUrl
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAllStatesList: () => {
      dispatch(showLoader(true))
      return new Promise((resolve, reject) => {
        dispatch(getAllStatesList()).then((response) => {
          dispatch(getAllStatesListSuccess(response.payload));
          dispatch(showLoader(false))
          resolve(response.payload);
        })
          .catch((error) => {
            dispatch(showLoader(false))
            dispatch(getAllStatesListFailure(error));
          });
      });
    },
    getEventsTypesList: () => {
      return new Promise((resolve, reject) => {
        dispatch(getEventTypes()).then((response) => {
          dispatch(getEventTypesSuccess(response.payload));
          resolve(response.payload);
        })
          .catch((error) => {
            dispatch(getEventTypesFailure(error));
          });
      });
    },
    getEventList: (searchParam) => {
      dispatch(showLoader(true))
      return new Promise((resolve, reject) => {
        dispatch(getEventList(searchParam))
          .then((response) => {
            dispatch(getEventListSuccess(response.payload));
            dispatch(showLoader(false))
            resolve(response.payload);
          })
          .catch((error) => {
            dispatch(showLoader(false))
            dispatch(getEventListFailure(error));
          });
      });
    },

		/**
     * updates the page number of events list
     */
    updateEventsPageNumber: (pageNumber) => {
      dispatch(updateEventsPageNumber({ pageNumber }))
    },
		/**
     * updates the searchParamQuery for fetching eventList
     */
    updateSearchEventParam: (params) => {
      dispatch(updateSearchEventParam({ params }))
    },
    /**
    * update save data of review report
    */
    updateReviewReportData: (data) => {
      dispatch(updateReviewReportData(data))
    },
    /**
  * update validate data of review report
  */
    validateReviewReportData: (data) => {
      dispatch(validateReviewReportData(data))
    },
    /**
     * generate report from event list pop-ups and returns key to download report
     */
    generateReviewReport: (data) => {
      return new Promise((resolve, reject) => {
        dispatch(generateReviewReport(data)).then((response) => {
          dispatch(generateReviewReportSuccess(response.payload.data))
          resolve(response.payload.data)
        }).catch((error) => {
          dispatch(generateReviewReportFailure(error))
          reject(error)
        })
      })
    },
    /**
     * get review report url
     */
    getReviewReportUrl: (id) => {
      return new Promise((resolve, reject) => {
        dispatch(getReviewReportUrl(id)).then((response) => {
          dispatch(getReviewReportUrlSuccess(response.payload.data))
          resolve(response.payload.data)
        }).catch((error) => {
          dispatch(getReviewReportUrlFailure(error))
          reject(error)
        })
      })
    },
    /**
     * download generated report
     */
    downloadGeneratedReport: (config) => {
      dispatch(downloadGeneratedReport(config))
    },

    /**
     * handles loader visibility
     */
    showLoader: (status) => {
      dispatch(showLoader(status))
    },

    /**
     * updates the last visited url
     */
    updateUrl: (updatedUrl) => {
      dispatch(updateUrl(updatedUrl))
    }
  }
}

const EventListContainer = connect(mapStateToProps, mapDispatchToProps)(EventListComponent)
export default EventListContainer