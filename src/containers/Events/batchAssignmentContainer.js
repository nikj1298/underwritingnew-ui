import { connect } from 'react-redux'
import BatchAssignmentComponent from './../../components/EventsDetails/batchAssignmentComponent'

const mapStateToProps = (state) => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

const BatchAssignmentContainer = connect(mapStateToProps, mapDispatchToProps)(BatchAssignmentComponent)
export default BatchAssignmentContainer