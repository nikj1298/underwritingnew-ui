import { connect } from 'react-redux'
import BidList from "../../components/EventsDetails/bidList"
import {
  getBidsList, getBidsListSuccess, getBidsListFailure ,
  postSaveBid, postSaveBidSuccess, postSaveBidFailure ,
  putUpdateBid, putUpdateBidSuccess, putUpdateBidFailure ,
  deleteBid, deleteBidSuccess, deleteBidFailure 
} from './../../actions/EventsDetails/bidNumberAction';
import { showLoader } from "../../actions/appAction"

const mapStateToProps = (state) => {
  return {
    showLoaderStatus: state.appReducer.loaderStatus,
    bidList: state.bidNumberReducer.bidList,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBidsListEventsList: (data) => {
      dispatch(showLoader(true))
      return new Promise((resolve, reject) => {
        dispatch(getBidsList(data)).then((response) => {
          dispatch(getBidsListSuccess(response.payload));
          dispatch(showLoader(false))
          resolve(response.payload);
        })
          .catch((error) => {
            dispatch(showLoader(false))
            dispatch(getBidsListFailure(error));
          });
      });
    },
    postSaveBidEventsList: (bidObject) => {
      dispatch(showLoader(true))
      return new Promise((resolve, reject) => {
        dispatch(postSaveBid(bidObject)).then((response) => {
          dispatch(postSaveBidSuccess(response.payload));
          dispatch(showLoader(false))
          resolve(response.payload);
        })
          .catch((error) => {
            dispatch(showLoader(false))
            dispatch(postSaveBidFailure(error));
          });
      });
    },
    putUpdateBidEventsList: (bidObject) => {
      dispatch(showLoader(true))
      return new Promise((resolve, reject) => {
        dispatch(putUpdateBid(bidObject)).then((response) => {
          dispatch(putUpdateBidSuccess(response.payload));
          dispatch(showLoader(false))
          resolve(response.payload);
        })
          .catch((error) => {
            dispatch(showLoader(false))
            dispatch(putUpdateBidFailure(error));
          });
      });
    },
    deleteBidEventsList: (bidObject) => {
      dispatch(showLoader(true))
      return new Promise((resolve, reject) => {
        dispatch(deleteBid(bidObject)).then((response) => {
          dispatch(deleteBidSuccess(response.payload));
          dispatch(showLoader(false));
          resolve(response.payload);
        })
          .catch((error) => {
            dispatch(showLoader(false));
            dispatch(deleteBidFailure(error));
          });
      });
    },
  }
}

const BidNumberContainer = connect(mapStateToProps, mapDispatchToProps)(BidList)
export default BidNumberContainer