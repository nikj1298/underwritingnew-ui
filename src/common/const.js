export const beforeExpiry = 60 * 1000 // 1 minute
export const checkInterval = 5 * 1000 // 5 seconds

export const eventsDropdownMenu = [
	{
		key: 1,
		name: 'Event',
		icon: 'event',
		navigate: '/underwritingnew/events'
	},
	{
		key: 2,
		name: 'Review',
		icon: 'review',
		navigate: '/underwritingnew/review'
	},
	{
		key: 3,
		name: 'Profile',
		icon: 'profile',
		navigate: '/underwritingnew/events'
	}
]

export const getMaxRows = () => {
	var total = window.innerHeight
	var top = 161
	var bottom = 72
	var rowHeight = 49
	return Math.floor((total - top - bottom) / rowHeight)
}


export const getRowsBids = () => {
	var total = window.innerHeight
	var top = 240;
	var bottom = 20;
	var rowHeight = 49;
	return Math.floor((total - top - bottom) / rowHeight)
}


export const addBidForm = {
	"Number": "",
	"Entity": "",
	"Portfolio": "",
	"EventId": ""
}

export const validateAddBidForm = {
	"Number": false,
	"Entity": false,
	"Portfolio": false,
}

export const eventsPageSize = getMaxRows()

export const searchEventsParam = {
	SortField: 'SaleDate',
	SortOrder: 'desc',
	FullSearch: '',
	Limit: getMaxRows(),
	offset: 0,
	Filter: {
		StateId: null,
		Type: null,
		IsLockedStatus: false,
		TaskType: null,
		AssignedTo: ""
	}
}

export const pageSize = getMaxRows()

export const formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
	minimumFractionDigits: 2
})

export const maskInputPatterns = {
	taxIdMask: [/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
	phoneNumberMask: ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
	ssnIdMask: [/\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
}

export const filterParamsBidNumber = {
	FullSearch: '',
	pageSize: getRowsBids(),
	pageNumber: 1,
	Offset: 0
}

export const sortEventsParams = [
	{
		name: "Event No.",
		value: "EventNumber",
		isSorted: null,
		isSortable: true
	},
	{
		name: "State",
		value: "State",
		isSorted: null,
		isSortable: true
	},
	{
		name: "County/Jurisdiction",
		value: "CountyName",
		isSorted: null,
		isSortable: true
	},
	{
		name: "Event Type",
		value: "EventType",
		isSorted: null,
		isSortable: true
	},
	{
		name: "Auction Type",
		value: "AuctionType",
		isSorted: null,
		isSortable: false
	},
	{
		name: "Sale Date",
		value: "SaleDate",
		isSorted: "desc",
		isSortable: true
	},
	{
		name: "Funding Date",
		value: "FundingDate",
		isSorted: null,
		isSortable: true
	},
	{
		name: "Progress",
		value: "IsLocked",
		isSorted: null,
		isSortable: false
	}
]

export const generateReviewReport = {
	SaleDateFrom: null,
	SaleDateTo: null,
	StateId: null
}

export const validateReviewReportData = {
	SaleDateFrom: false,
	SaleDateTo: false,
	StateId: false
}
export const tasksTableHead = [
	'Task Type',
	'DUE DATE',
	'ASSIGNED TO',
	'Completion Date',
	'Completed By',
	'STATUS'
]

export const filterParamsTasksList = {
	pageSize: getRowsBids(),
	pageNumber: 1,
	Offset: 0
}

export const getEventsParam = {
	Filter: {
		IsLocked: 0
	},
	offset: 0,
	limit: 2000
}

export const filterPropertiesParam = {
	offset: 0,
	limit: getMaxRows(),
	Filter: {
		MoveForward: true,
		ReviewDecision: 0,
		EventId: null,
		AssignmentByUser: true
	},
	FullSearch: "",
	SortField: 'AccountName',
	SortOrder: 'desc',
}

export const eventStatusMenu = [
	{
		Id: false,
		Name: "Active"
	}, {
		Id: true,
		Name: "Locked"
	},
	{
		Id: 0,
		Name: "All"
	}]

export const assignmentDropMenu = [{
	Id: true,
	Name: "Assigned to me"
}, {
	Id: false,
	Name: "All"
}]

export const eventPropertiesHeader = [
	{
		name: "Owner Name",
		value: "AccountName",
		isSorted: null,
		isSortable: false
	},
	{
		name: "Parcel ID",
		value: "ParcelId",
		isSorted: null,
		isSortable: false
	},
	{
		name: "Property Address",
		value: "Address",
		isSorted: null,
		isSortable: false
	},
	{
		name: "Assessed Value",
		value: "AppraisedValue",
		isSorted: null,
		isSortable: false
	},
	{
		name: "Amount Due",
		value: "AmountDue",
		isSorted: null,
		isSortable: false
	},
	{
		name: "LTV",
		value: "Ltv",
		isSorted: null,
		isSortable: false
	},
	{
		name: "RU LTV",
		value: "RuLtv",
		isSorted: null,
		isSortable: false
	},
	{
		name: "LUC",
		value: "LandUseCode",
		isSorted: null,
		isSortable: false
	},
	{
		name: "Current Decision",
		value: "CurrentDecision",
		isSorted: null,
		isSortable: false
	}
]

export const reviewPopupUrl = "/underwritingnew/review"

export const reviewPropertyAdvanceFilter = {
	"EventId": null,
	"CurrentDelenquencyId": null,
	"MoveForward": true,
	"AssignmentByUser": true,
	"LevelFilters": [{
		"LevelId": "",
		"ReviewId": null
	}],
	"ReviewDecision": 0,
	"MinAssessedValue": null,
	"MaxAssessedValue": null,
	"MinAmountDue": null,
	"MaxAmountDue": null,
	"ParcelID": null,
	"Owner": null,
	"PropertyAddress": null,
	"PropertyCity": null,
	"PropertyZipCode": null,
	"LandUseCode": null,
	"InternalLandUseCodes": [],
	"GeneralLandUseCodes": []
}

export const decisionDropDownMenu = [
	{
		Id: 0,
		Name: "All"
	},
	{
		Id: 1,
		Name: "With prior decisions"
	},
	{
		Id: 2,
		Name: "Without prior decisions"
	}
]

export const levelReviewStatus = [
	{
		Id: null,
		Name: "All"
	},
	{
		Id: 0,
		Name: "Unreviewed"
	},
	{
		Id: 1,
		Name: "Approved"
	},
	{
		Id: 2,
		Name: "Rejected"
	},
	{
		Id: 3,
		Name: "Research"
	},
	{
		Id: 4,
		Name: "Auto Approved"
	},
	{
		Id: 5,
		Name: "Auto Rejected"
	}
]