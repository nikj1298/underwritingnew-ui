import { doActionNotification } from '../services/httpRequest'

export const getNotificationAction = async (id) => {
  const response = await doActionNotification({ url: 'operations/' + id })
  return response
}