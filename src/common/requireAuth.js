import jwt_decode from "jwt-decode"
import { getAccessToken } from "../actions/getAuthAction"
import {  beforeExpiry, checkInterval } from "../common/const"
import { AUTH_URL } from "../services/appConfig"

/**
 * returns the token of current user
 * @param {*} currentUser 
 */
export const getSessionToken = (currentUser) => {
  const sessionCurrentUser = JSON.parse(currentUser)
  return "Bearer " + sessionCurrentUser.token
}
/**
 * decodes the access token for getting the data of current user
 * @param {*} token 
 */
export const getDecodedAccessToken = (token) => {
  try {
    return jwt_decode(token);
  } catch (Error) {
    return null;
  }
}
/**
 * maps the decoded data received from decoding the access token to the keys required
 * @param {*} userData 
 * @param {*} tokens 
 */
export const mapDecodedAccessToken = (userData, tokens) => {
  const nameKey = Object.keys(userData).find(key => key.indexOf('name') > -1 && key.indexOf('nameidentifier') === -1);
  const emailKey = Object.keys(userData).find(key => key.indexOf('email') > -1);
  const userIdKey = Object.keys(userData).find(key => key.indexOf('nameidentifier') > -1);
  const name = userData[nameKey];
  const email = userData[emailKey];
  const userId = userData[userIdKey];
  const departments = JSON.parse(userData['Departments'] || '[]');
  let permissions = JSON.parse(userData['Permissions']);
  return {
    token: tokens.access,
    refreshToken: tokens.refresh,
    name,
    email,
    userId,
    exp: userData.exp,
    refreshTokenExp: Math.floor(new Date(tokens.sessionExpires).getTime() / 1000),
    AppModule: userData['AppModule'],
    permissions,
    departments,
  };
}
/**
 * sets the current user data in session storage
 * @param {*} responseData 
 */
export const setUser = (responseData) => {
  const decodedAccessToken = getDecodedAccessToken(responseData.access);
  const user = mapDecodedAccessToken(decodedAccessToken, responseData);
  sessionStorage.setItem('currentUser', JSON.stringify(user));
  return user
}
/**
 * returns the new access token using refresh token
 */
export const getRefreshToken = () => {
  window.addEventListener('storage', event => {
    if (event.key === 'updateSessionStorage') {
      const data = JSON.parse(event.newValue);
      if (data) {
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            sessionStorage.setItem(key, data[key]);
          }
        }
        this.setUser(JSON.parse(sessionStorage.getItem('currentUser')));
      }
    }
  });
  const currentUser = sessionStorage.currentUser ? JSON.parse(sessionStorage.currentUser) : undefined
  // const token = currentUser ? currentUser.refreshToken : refreshToken
  const token = sessionStorage.parametersRefreshToken ? sessionStorage.parametersRefreshToken :
    currentUser ? currentUser.refreshToken : undefined
  if (token) {
    return getAccessToken({ token }).then((response) => {
      const { data } = response;
      if (data) {
        localStorage.setItem('updateSessionStorage', JSON.stringify({ currentUser: JSON.stringify(setUser(data)) }));
        localStorage.removeItem('updateSessionStorage');
        return setUser(data)
      }
    }).catch(() => {
      logout()
    })
  } else {
    logout()
  }
}

// export const redirectToPingOne = () => {
//   window.location.href = "https://desktop.pingone.com/Hunt/Selection?cmd=selection"
// }

export const checkExpiry = () => {
  const currentUser = sessionStorage.currentUser ? JSON.parse(sessionStorage.currentUser) : undefined;
  console.log("checking every 5 sec");
  if (currentUser && currentUser.refreshTokenExp) {
    const expiry = currentUser.refreshTokenExp * 1000; // ms
    const now = new Date().getTime(); // ms
    if (now > expiry - beforeExpiry) {
      alert("The Session has expired! Redirecting to pingone");
      console.log("in if")
      logout()
    }
  } else {
    console.log("in else")
    logout()
  }
  setTimeout(checkExpiry, checkInterval);
}

export const logout = () => {
  sessionStorage.removeItem('currentUser');
  sessionStorage.removeItem('parametersRefreshToken')
  window.location.assign(`${AUTH_URL}/SSO`);
}