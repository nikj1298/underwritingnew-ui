import { doActionGet } from '../../services/httpRequest';

export const GET_ALL_EVENTS = "GET_ALL_EVENTS"
export const GET_ALL_EVENTS_SUCCESS = "GET_ALL_EVENTS_SUCCESS"
export const GET_ALL_EVENTS_FAILURE = "GET_ALL_EVENTS_FAILURE"

export const UPDATE_ALL_EVENT_PARAM = "UPDATE_ALL_EVENT_PARAM"
export const UPDATE_GET_PROPERTIES_PARAM = "UPDATE_GET_PROPERTIES_PARAM"

export const GET_PROPERTIES_LIST = "GET_PROPERTIES_LIST"
export const GET_PROPERTIES_LIST_SUCCESS = "GET_PROPERTIES_LIST_SUCCESS"
export const GET_PROPERTIES_LIST_FAILURE = "GET_PROPERTIES_LIST_FAILURE"

export const UPDATE_EVENT_PROPERTY_PAGE_NUMBER = "UPDATE_EVENT_PROPERTY_PAGE_NUMBER"
export const FETCH_ALL_LEVELS = "FETCH_ALL_LEVELS"
export const FETCH_ALL_LEVELS_SUCCESS = "FETCH_ALL_LEVELS_SUCCESS"
export const FETCH_ALL_LEVELS_FAILURE = "FETCH_ALL_LEVELS_FAILURE"

export const GET_ALL_GENERAL_LAND_USE_CODE = "GET_ALL_GENERAL_LAND_USE_CODE"
export const GET_ALL_GENERAL_LAND_USE_CODE_SUCCESS = "GET_ALL_GENERAL_LAND_USE_CODE_SUCCESS"
export const GET_ALL_GENERAL_LAND_USE_CODE_FAILURE = "GET_ALL_GENERAL_LAND_USE_CODE_FAILURE"

export const GET_ALL_INTERNAL_LAND_USE_CODE = "GET_ALL_INTERNAL_LAND_USE_CODE"
export const GET_ALL_INTERNAL_LAND_USE_CODE_SUCCESS = "GET_ALL_INTERNAL_LAND_USE_CODE_SUCCESS"
export const GET_ALL_INTERNAL_LAND_USE_CODE_FAILURE = "GET_ALL_INTERNAL_LAND_USE_CODE_FAILURE"

/**
 * action to fetch event list for review landing pop-up
 * @param {*} searchParam 
 */
export const getAllEventsList = (searchParam) => {
  let urlEvent = `Dictionaries/events?offset=${searchParam.offset}&limit=${searchParam.limit}`
  if (searchParam.Filter.IsLocked !== 0) urlEvent += `&Filter.IsLocked=${searchParam.Filter.IsLocked}`
  const request = doActionGet({ url: urlEvent })
  return {
    type: GET_ALL_EVENTS,
    payload: request
  }
}

/**
 * success action to fetch event list for review landing pop-up
 */
export const getAllEventsListSuccess = (response) => {
  return {
    type: GET_ALL_EVENTS_SUCCESS,
    payload: response
  }
}

/**
 * failure action to fetch event list for review landing pop-up
 */
export const getAllEventsListFailure = (error) => {
  return {
    type: GET_ALL_EVENTS_FAILURE,
    payload: error
  }
}

/**
 * action to update get all events param
 * @param {*} searchParam 
 */
export const updateGetAllEventsParam = (searchParam) => {
  return {
    type: UPDATE_ALL_EVENT_PARAM,
    payload: searchParam
  }
}

/**
 * action to update filter properties param
 */
export const updateGetPropertyListParam = (searchParam) => {
  return {
    type: UPDATE_GET_PROPERTIES_PARAM,
    payload: searchParam
  }
}

/**
 * action to fetch properties list on the basis of event and other filters applied
 */
export const fetchEventProperties = (searchParam) => {
  const request = doActionGet({ url: `Properties?offset=${searchParam.offset}&SortField=${searchParam.SortField}&SortOrder=${searchParam.SortOrder}&limit=${searchParam.limit}&Filter.MoveForward=${searchParam.Filter.MoveForward}&Filter.ReviewDecision=${searchParam.Filter.ReviewDecision}&Filter.EventId=${searchParam.Filter.EventId}&Filter.AssignmentByUser=${searchParam.Filter.AssignmentByUser}&FullSearch=${searchParam.FullSearch}` })
  return {
    type: GET_PROPERTIES_LIST,
    payload: request
  }
}

/**
 * success action to fetch properties list on the basis of event and other filters applied
 */
export const fetchEventPropertiesSuccess = (response) => {
  return {
    type: GET_PROPERTIES_LIST_SUCCESS,
    payload: response
  }
}
/**
 * failure action to fetch properties list on the basis of event and other filters applied
 */
export const fetchEventPropertiesFailure = (error) => {
  return {
    type: GET_PROPERTIES_LIST_FAILURE,
    payload: error
  }
}

/**
 * updates the page number of the event property list
 * @param {*} pageNumber 
 */
export const updateEventPropertyPageNumber = (pageNumber) => {
  return {
    type: UPDATE_EVENT_PROPERTY_PAGE_NUMBER,
    payload: pageNumber
  }
}

/**
 * action to fetch all levels on the basis of eventId
 */
export const fetchAllLevels = (eventId) => {
  const request = doActionGet({ url: `Properties/levels?Filter.EventId=${eventId}` })
  return {
    type: FETCH_ALL_LEVELS,
    payload: request
  }
}

/**
 * success action to fetch all levels on the basis of eventId
 */
export const fetchAllLevelsSuccess = (response) => {
  return {
    type: FETCH_ALL_LEVELS_SUCCESS,
    payload: response
  }
}
/**
 * failure action to fetch all levels on the basis of eventId
 */
export const fetchAllLevelsFailure = (error) => {
  return {
    type: FETCH_ALL_LEVELS_FAILURE,
    payload: error
  }
}

/**
 * action to get all general land use code from table
 */
export const getAllGeneralLandUseCode = () => {
  const request = doActionGet({ url: `Dictionaries/GeneralLandUseCodes?offset=0` })
  return {
    type: GET_ALL_GENERAL_LAND_USE_CODE,
    payload: request
  }
}
/**
 * success action to get all general land use code from table
 */
export const getAllGeneralLandUseCodeSuccess = (response) => {
  return {
    type: GET_ALL_GENERAL_LAND_USE_CODE_SUCCESS,
    payload: response
  }
}
/**
 * failure action to get all general land use code from table
 */
export const getAllGeneralLandUseCodeFailure = (error) => {
  return {
    type: GET_ALL_GENERAL_LAND_USE_CODE_FAILURE,
    payload: error
  }
}

/**
 * action to get all internal land use code from table
 */
export const getAllInternalLandUseCode = () => {
  const request = doActionGet({ url: `Dictionaries/InternalLandUseCodes?offset=0` })
  return {
    type: GET_ALL_INTERNAL_LAND_USE_CODE,
    payload: request
  }
}
/**
 * success action to get all internal land use code from table
 */
export const getAllInternalLandUseCodeSuccess = (response) => {
  return {
    type: GET_ALL_INTERNAL_LAND_USE_CODE_SUCCESS,
    payload: response
  }
}
/**
 * failure action to get all internal land use code from table
 */
export const getAllInternalLandUseCodeFailure = (error) => {
  return {
    type: GET_ALL_INTERNAL_LAND_USE_CODE_FAILURE,
    payload: error
  }
}


