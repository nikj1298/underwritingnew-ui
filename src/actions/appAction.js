export const SHOW_LOADER = 'SHOW_LOADER'
export const UPDATED_URL = "UPDATED_URL"
/**
 * renders loader on screen on update of props
 * @param {*} showLoader 
 */
export const showLoader = (showLoaderStatus) => {
  return {
    type: SHOW_LOADER,
    payload: showLoaderStatus
  }
}

/**
 * action to update last visited url
 */
export const updateUrl = (updatedUrl) => {
  return {
    type: UPDATED_URL,
    payload: updatedUrl
  }
}