import { doActionGet, doActionGetLanding, doActionPost, doActionGetDownload } from '../../services/httpRequest';

export const FETCH_ALL_STATES_LIST = 'FETCH_ALL_STATES_LIST';
export const FETCH_ALL_STATES_LIST_SUCCESS = "FETCH_ALL_STATES_LIST_SUCCESS";
export const FETCH_ALL_STATES_LIST_FAILURE = "FETCH_ALL_STATES_LIST_FAILURE";

export const FETCH_EVENTS_TYPES = 'FETCH_EVENTS_TYPES';
export const FETCH_EVENTS_TYPES_SUCCESS = "FETCH_EVENTS_TYPES_SUCCESS";
export const FETCH_EVENTS_TYPES_FAILURE = "FETCH_EVENTS_TYPES_FAILURE";

export const FETCH_ALL_EVENTS_LIST = 'FETCH_ALL_EVENTS_LIST';
export const FETCH_ALL_EVENTS_LIST_SUCCESS = "FETCH_ALL_EVENTS_LIST_SUCCESS";
export const FETCH_ALL_EVENTS_LIST_FAILURE = "FETCH_ALL_EVENTS_LIST_FAILURE";

export const UPDATE_EVENT_PAGE_NUMBER = "UPDATE_EVENT_PAGE_NUMBER"
export const UPDATE_SEARCH_EVENT_PARAM = "UPDATE_SEARCH_EVENT_PARAM"
export const UPDATE_REVIEW_REPORT_DATA = "UPDATE_REVIEW_REPORT_DATA"
export const VALIDATE_REVIEW_REPORT_DATA = "VALIDATE_REVIEW_REPORT_DATA"

export const GENERATE_REVIEW_REPORT = "GENERATE_REVIEW_REPORT"
export const GENERATE_REVIEW_REPORT_SUCCESS = "GENERATE_REVIEW_REPORT_SUCCESS"
export const GENERATE_REVIEW_REPORT_FAILURE = "GENERATE_REVIEW_REPORT_FAILURE"

export const GET_REVIEW_REPORT_URL = "GET_REVIEW_REPORT_URL"
export const GET_REVIEW_REPORT_URL_SUCCESS = "GET_REVIEW_REPORT_URL_SUCCESS"
export const GET_REVIEW_REPORT_URL_FAILURE = "GET_REVIEW_REPORT_URL_FAILURE"

export const DOWNLOAD_GENERATED_REPORT = "DOWNLOAD_GENERATED_REPORT"

export const getAllStatesList = () => {
    const request = doActionGetLanding({ url: 'Dictionaries/states?offset=0' });
    return {
        type: FETCH_ALL_STATES_LIST,
        payload: request
    };
}

export const getAllStatesListSuccess = (data) => {
    return {
        type: FETCH_ALL_STATES_LIST_SUCCESS,
        payload: data
    };
}

export const getAllStatesListFailure = (error) => {
    return {
        type: FETCH_ALL_STATES_LIST_FAILURE,
        payload: error
    };
}

export const getEventTypes = () => {
    const request = doActionGetLanding({ url: 'Dictionaries/EventTypes?offset=0' });
    return {
        type: FETCH_EVENTS_TYPES,
        payload: request
    };
}

export const getEventTypesSuccess = (data) => {
    return {
        type: FETCH_EVENTS_TYPES_SUCCESS,
        payload: data
    };
}

export const getEventTypesFailure = (error) => {
    return {
        type: FETCH_EVENTS_TYPES_FAILURE,
        payload: error
    };
}

export const getEventList = ({ Limit, offset, FullSearch, SortOrder, SortField, Filter }) => {
    const { StateId, Type, TaskType, AssignedTo, IsLockedStatus } = Filter
    let urlData = 'Events?Limit=' + Limit + '&Offset=' + offset + '&SortField=' + SortField + '&SortOrder=' + SortOrder;
    if (StateId) {
        urlData += '&Filter.StateId=' + StateId;
    }
    if (Type) {
        urlData += '&Filter.Type=' + Type;
    }
    if (IsLockedStatus === true || IsLockedStatus === false) {
        urlData += '&Filter.IsLockedStatus=' + IsLockedStatus;
    }
    if (FullSearch) {
        urlData += '&FullSearch=' + FullSearch;
    }
    if (TaskType) {
        urlData += '&Filter.TaskType=' + TaskType
    }
    if (AssignedTo) {
        urlData += "&Filter.AssignedTo=" + AssignedTo
    }
    const request = doActionGet({
        url: urlData
    });
    return {
        type: FETCH_ALL_EVENTS_LIST,
        payload: request
    };
}

export const getEventListSuccess = (data) => {
    return {
        type: FETCH_ALL_EVENTS_LIST_SUCCESS,
        payload: data
    };
}

export const getEventListFailure = (error) => {
    return {
        type: FETCH_ALL_EVENTS_LIST_FAILURE,
        payload: error
    };
}

/**
 * updates the search query param of event list
 */
export const updateSearchEventParam = (searchParam) => {
    return {
        type: UPDATE_SEARCH_EVENT_PARAM,
        payload: searchParam.params
    }
}
/**
 * updates the page number of the event list
 * @param {*} pageNumber 
 */
export const updateEventsPageNumber = (pageNumber) => {
    return {
        type: UPDATE_EVENT_PAGE_NUMBER,
        payload: pageNumber
    }
}

/**
 * updates the review report data
 */
export const updateReviewReportData = (reviewData) => {
    return {
        type: UPDATE_REVIEW_REPORT_DATA,
        payload: reviewData
    }
}
/**
 * validates the review report data
 */
export const validateReviewReportData = (reviewData) => {
    return {
        type: VALIDATE_REVIEW_REPORT_DATA,
        payload: reviewData
    }
}

/**
 * action to generate review report
 */
export const generateReviewReport = (details) => {
    const request = doActionPost({ url: "events/reviewreport", data: details })
    return {
        type: GENERATE_REVIEW_REPORT,
        payload: request
    }
}

/**
 * success action to generate review report
 */
export const generateReviewReportSuccess = (data) => {
    return {
        type: GENERATE_REVIEW_REPORT_SUCCESS,
        payload: data
    }
}

/**
 * failure action to generate review report
 */
export const generateReviewReportFailure = (error) => {
    return {
        type: GENERATE_REVIEW_REPORT_FAILURE,
        payload: error
    }
}

/**
 * action to get review report url
 */
export const getReviewReportUrl = (id) => {
    const request = doActionGet({ url: `events/${id}/reviewreport` })
    return {
        type: GET_REVIEW_REPORT_URL,
        payload: request
    }
}

/**
 * success action to get review report url
 */
export const getReviewReportUrlSuccess = (response) => {
    return {
        type: GET_REVIEW_REPORT_URL_SUCCESS,
        payload: response
    }
}

/**
 * failure action to get review report url
 */
export const getReviewReportUrlFailure = (error) => {
    return {
        type: GET_REVIEW_REPORT_URL_FAILURE,
        payload: error
    }
}

/**
 * action to export CampaignData
 */
export const downloadGeneratedReport = (config) => {
    const request = doActionGetDownload(config)
    return {
        type: DOWNLOAD_GENERATED_REPORT,
        payload: request
    }
}


