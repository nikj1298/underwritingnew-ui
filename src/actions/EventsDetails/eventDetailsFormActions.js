
import { doActionGet, doActionGetLanding, doActionPost, doActionPut, doActionDelete } from '../../services/httpRequest';

export const FETCH_All_RULES_FIELDS = 'FETCH_All_RULES_FIELDS';
export const FETCH_All_RULES_FIELDS_SUCCESS = "FETCH_All_RULES_FIELDS_SUCCESS";
export const FETCH_All_RULES_FIELDS_FAILURE = "FETCH_All_RULES_FIELDS_FAILURE";

export const PUT_UPDATE_APPLIED_RULES = 'PUT_UPDATE_APPLIED_RULES';
export const PUT_UPDATE_APPLIED_RULES_SUCCESS = "PUT_UPDATE_APPLIED_RULES_SUCCESS";
export const PUT_UPDATE_APPLIED_RULES_FAILURE = "PUT_UPDATE_APPLIED_RULES_FAILURE";

export const FETCH_RULES_POSSIBLITY_FIELDS = 'FETCH_RULES_POSSIBLITY_FIELDS';
export const FETCH_RULES_POSSIBLITY_FIELDS_SUCCESS = "FETCH_RULES_POSSIBLITY_FIELDS_SUCCESS";
export const FETCH_RULES_POSSIBLITY_FIELDS_FAILURE = "FETCH_RULES_POSSIBLITY_FIELDS_FAILURE";

export const FETCH_RULES_APPROVE_REJECT = 'FETCH_RULES_APPROVE_REJECT';
export const FETCH_RULES_APPROVE_REJECT_SUCCESS = "FETCH_RULES_APPROVE_REJECT_SUCCESS";
export const FETCH_RULES_APPROVE_REJECT_FAILURE = "FETCH_RULES_APPROVE_REJECT_FAILURE";

export const POST_SAVE_DATA_RULES_NEW = 'POST_SAVE_DATA_RULES_NEW';
export const POST_SAVE_DATA_RULES_NEW_SUCCESS = "POST_SAVE_DATA_RULES_NEW_SUCCESS";
export const POST_SAVE_DATA_RULES_NEW_FAILURE = "POST_SAVE_DATA_RULES_NEW_FAILURE";

export const getRulesList = (eventIdSelected) => {

    const request = doActionGet({ url: 'Events/'+ eventIdSelected +'/Rules' });
    return {
        type: FETCH_All_RULES_FIELDS,
        payload: request
    };
}

export const getRulesListSuccess = (data) => {
    return {
        type: FETCH_All_RULES_FIELDS_SUCCESS,
        payload: data
    };
}

export const getRulesListFailure = (error) => {
    return {
        type: FETCH_All_RULES_FIELDS_FAILURE,
        payload: error
    };
}

export const postUpdateAppliedRuled = (eventIdSelected , data) => {

    const request = doActionPut({
        url: 'Events/'+ eventIdSelected +'/Rules' ,
        data: data
    });
    return {
        type: PUT_UPDATE_APPLIED_RULES,
        payload: request
    };
}

export const postUpdateAppliedRuledSuccess = (data) => {
    return {
        type: PUT_UPDATE_APPLIED_RULES_SUCCESS,
        payload: data
    };
}

export const postUpdateAppliedRuledFailure = (error) => {
    return {
        type: PUT_UPDATE_APPLIED_RULES_FAILURE,
        payload: error
    };
}

export const getRulesPossiblityList = () => {

    const request = doActionGet({ url: 'Dictionaries/RuleFields' });
    return {
        type: FETCH_RULES_POSSIBLITY_FIELDS,
        payload: request
    };
}

export const getRulesPossiblityListSuccess = (data) => {
    return {
        type: FETCH_RULES_POSSIBLITY_FIELDS_SUCCESS,
        payload: data
    };
}

export const getRulesPossiblityListFailure = (error) => {
    return {
        type: FETCH_RULES_POSSIBLITY_FIELDS_FAILURE,
        payload: error
    };
}


export const getRulesAprroveReject = () => {
    const request = doActionGet({ url: 'Dictionaries/ResultTypes' });
    return {
        type: FETCH_RULES_APPROVE_REJECT,
        payload: request
    };
}

export const getRulesAprroveRejectSuccess = (data) => {
    return {
        type: FETCH_RULES_APPROVE_REJECT_SUCCESS,
        payload: data
    };
}

export const getRulesAprroveRejectFailure = (error) => {
    return {
        type: FETCH_RULES_APPROVE_REJECT_FAILURE,
        payload: error
    };
}

export const postsaveNewDataRules = (eventIdSelected , data ) => {
    const request = doActionPost({ 
        url: 'events/rules',
        data : data    
    });
    return {
        type: POST_SAVE_DATA_RULES_NEW,
        payload: request
    };
}

export const postsaveNewDataRulesSuccess = (data) => {
    return {
        type: POST_SAVE_DATA_RULES_NEW_SUCCESS,
        payload: data
    };
}

export const postsaveNewDataRulesFailure = (error) => {
    return {
        type: POST_SAVE_DATA_RULES_NEW_FAILURE,
        payload: error
    };
}





