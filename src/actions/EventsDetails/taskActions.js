import { doActionWorkflow } from '../../services/httpRequest';

export const FETCH_All_TASKS_LIST = 'FETCH_All_TASKS_LIST';
export const FETCH_All_TASKS_LIST_SUCCESS = "FETCH_All_TASKS_LIST_SUCCESS";
export const FETCH_All_TASKS_LIST_FAILURE = "FETCH_All_TASKS_LIST_FAILURE";

export const getTasksList = (data) => {
    let url = 'tasks/underwriting?Filter.EventIds=' + data;
    const request = doActionWorkflow({ url: url });
    return {
        type: FETCH_All_TASKS_LIST,
        payload: request
    };
}

export const getTasksListSuccess = (data) => {
    return {
        type: FETCH_All_TASKS_LIST_SUCCESS,
        payload: data
    };
}

export const getTasksListFailure = (error) => {
    return {
        type: FETCH_All_TASKS_LIST_FAILURE,
        payload: error
    };
}