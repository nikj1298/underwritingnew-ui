import { doActionGet, doActionGetLanding, doActionPost, doActionPut, doActionDelete } from '../../services/httpRequest';

export const FETCH_All_BIDS = 'FETCH_All_BIDS';
export const FETCH_All_BIDS_SUCCESS = "FETCH_All_BIDS_SUCCESS";
export const FETCH_All_BIDS_FAILURE = "FETCH_All_BIDS_FAILURE";

export const POST_SAVE_BID = 'POST_SAVE_BID';
export const POST_SAVE_BID_SUCCESS = "POST_SAVE_BID_SUCCESS";
export const POST_SAVE_BID_FAILURE = "POST_SAVE_BID_FAILURE";

export const PUT_UPDATE_BID = 'PUT_UPDATE_BID';
export const PUT_UPDATE_BID_SUCCESS = "PUT_UPDATE_BID_SUCCESS";
export const PUT_UPDATE_BID_FAILURE = "PUT_UPDATE_BID_FAILURE";

export const DELETE_BID = 'DELETE_BID';
export const DELETE_BID_SUCCESS = "DELETE_BID_SUCCESS";
export const DELETE_BID_FAILURE = "DELETE_BID_FAILURE";

export const getBidsList = (data) => {
    let url = 'Bids?Filter.EventIds=' + data.EventId
    url += '&limit=' + data.pageSize
    url += '&offset=' + ((data.pageNumber-1) * data.pageSize);
    if (data.FullSearch) {
        url += '&FullSearch=' + data.FullSearch;
    }
    const request = doActionGet({ url: url });
    return {
        type: FETCH_All_BIDS,
        payload: request
    };
}

export const getBidsListSuccess = (data) => {
    return {
        type: FETCH_All_BIDS_SUCCESS,
        payload: data
    };
}

export const getBidsListFailure = (error) => {
    return {
        type: FETCH_All_BIDS_FAILURE,
        payload: error
    };
}

export const postSaveBid = (data) => {
    const request = doActionPost({
        url: 'Bids',
        data: data
    });
    return {
        type: POST_SAVE_BID,
        payload: request
    };
}

export const postSaveBidSuccess = (data) => {
    return {
        type: POST_SAVE_BID_SUCCESS,
        payload: data
    };
}

export const postSaveBidFailure = (error) => {
    return {
        type: POST_SAVE_BID_FAILURE,
        payload: error
    };
}
export const putUpdateBid = (data) => {
    let dataNew = {
        Entity: data.Entity,
        EventId: data.EventId,
        Number: data.Number,
        Portfolio: data.Portfolio,
    }
    const request = doActionPut({
        url: 'Bids/' + data.Id,
        data: dataNew
    });
    return {
        type: PUT_UPDATE_BID,
        payload: request
    };
}

export const putUpdateBidSuccess = (data) => {
    return {
        type: PUT_UPDATE_BID_SUCCESS,
        payload: data
    };
}

export const putUpdateBidFailure = (error) => {
    return {
        type: PUT_UPDATE_BID_FAILURE,
        payload: error
    };
}

export const deleteBid = (data) => {
    const request = doActionDelete({
        url: 'Bids',
        data: { BidIds: data }
    });
    return {
        type: DELETE_BID,
        payload: request
    };
}

export const deleteBidSuccess = (data) => {
    return {
        type: DELETE_BID_SUCCESS,
        payload: data
    };
}

export const deleteBidFailure = (error) => {
    return {
        type: DELETE_BID_FAILURE,
        payload: error
    };
}

