import { doAuthActionGet } from "../services/httpRequest";

/**
 * action to get access token for fetching data
 * @param {*} token 
 */
export const getAccessToken = (detail) => {
  const request = doAuthActionGet({ url: "token/" + detail.token })
  return request;
}