import React, { Component } from 'react'
import { Route } from 'react-router'
import { Switch, Redirect, BrowserRouter } from 'react-router-dom'
import App from '../../src/App'
import DynamicImport from '../common/lazyLoadComponent'
import LoaderComponent from './../common/loaderComponent'

const EventListLoader = (props) => {
  return (
    <DynamicImport load={() => import('../containers/Events/eventListContainer')}>
      {(Component) => (Component == null ? <LoaderComponent /> : <Component {...props} />)}
    </DynamicImport>
  )
}

const EventDetailsLoader = (props) => {
  return (
    <DynamicImport load={() => import('../containers/Events/eventDetailsContainer')}>
      {(Component) => (Component == null ? <LoaderComponent /> : <Component {...props} />)}
    </DynamicImport>
  )
}

const ReviewListLoader = (props) => {
  return (
    <DynamicImport load={() => import('../containers/Review/reviewEventContainer')}>
      {(Component) => (Component == null ? <LoaderComponent /> : <Component {...props} />)}
    </DynamicImport>
  )
}

const ReviewEventLoader = (props) => {
  return (<DynamicImport load={() => import('../containers/Review/reviewPropertyListContainer')}>
    {(Component) => (Component == null ? <LoaderComponent /> : <Component {...props} />)}
  </DynamicImport>
  )
}
const ReviewDetailsLoader = (props) => {
  return (
    <DynamicImport load={() => import('../containers/Review/reviewDetailsContainer')}>
      {(Component) => (Component == null ? <LoaderComponent /> : <Component {...props} />)}
    </DynamicImport>
  )
}

export default class AppRoute extends Component {
  render() {
    return (
      <BrowserRouter>
        {(window.location.pathname.indexOf('/index.html') > -1) &&
          <Redirect to={"/underwritingnew" + window.location.hash.slice(1)} />
        }
        <Switch>
          <Route exact path="/underwritingnew" component={App}>
            <Redirect from="/underwritingnew" to="/underwritingnew/events" />
          </Route>
          <Route exact path="/underwritingnew/events" component={EventListLoader} />
          <Route exact path="/underwritingnew/events-details/:id" component={EventDetailsLoader} />
          <Route exact path="/underwritingnew/review" component={ReviewListLoader} />
          <Route exact path="/underwritingnew/review/event/:id" component={ReviewEventLoader} />
          <Route exact path="/underwritingnew/review-details" component={ReviewDetailsLoader} />
        </Switch>
      </BrowserRouter>
    )
  }
}
