import React from "react";
import { render } from '@testing-library/react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { EventTableComponent } from "../../../components/Events/eventTableComponent"

configure({
  adapter: new Adapter()
})

describe("Event Table component", () => {
  it("renders event list table", () => {
    const eventList = [
      {
        "Id": "00865817-97d0-4a62-ada6-acfef59db7b3",
        "Number": "WY19-Lviv 100020-PUBLICSALE-B1",
        "County": {
          "Id": 1081,
          "Name": "Lviv 100020"
        },
        "State": {
          "Id": 51,
          "Name": "WY"
        },
        "Type": {
          "Id": 4,
          "Name": "Public Sale"
        },
        "AuctionType": {
          "Id": 4,
          "Name": "Negotiated Contract"
        },
        "FundingDate": "2019-05-31T00:00:00",
        "SaleDate": "2019-03-28T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": true,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      },
      {
        "Id": "04850a8e-627a-4f2e-a513-e247bc4d5b6d",
        "Number": "AK19-India-SUB-B1",
        "County": {
          "Id": 1146,
          "Name": "India"
        },
        "State": {
          "Id": 2,
          "Name": "AK"
        },
        "Type": {
          "Id": 2,
          "Name": "Sub"
        },
        "AuctionType": {
          "Id": 2,
          "Name": "Auction - Live"
        },
        "FundingDate": "2019-11-03T00:00:00",
        "SaleDate": "2019-11-04T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": true,
        "IsAssigned": false,
        "IsRejectReasonRequired": true,
        "AssignedTo": null
      },
      {
        "Id": "06be2618-fbf3-4947-b6e0-3d7cd2301131",
        "Number": "MD19-MD_County-NEW-B2",
        "County": {
          "Id": 1094,
          "Name": "MD_County"
        },
        "State": {
          "Id": 21,
          "Name": "MD"
        },
        "Type": {
          "Id": 1,
          "Name": "New"
        },
        "AuctionType": {
          "Id": 2,
          "Name": "Auction - Live"
        },
        "FundingDate": null,
        "SaleDate": "2019-05-01T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": true,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      },
      {
        "Id": "0890e447-3449-40f9-af04-ffea888fb9a8",
        "Number": "CA19-POLTAVA OBLAST-NEW-B2",
        "County": {
          "Id": 1105,
          "Name": "POLTAVA OBLAST"
        },
        "State": {
          "Id": 5,
          "Name": "CA"
        },
        "Type": {
          "Id": 1,
          "Name": "New"
        },
        "AuctionType": {
          "Id": 1,
          "Name": "RFP"
        },
        "FundingDate": "2019-08-13T00:00:00",
        "SaleDate": "2019-08-14T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": true,
        "IsAssigned": true,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      },
      {
        "Id": "0a004cf2-207d-4807-a977-7ca85072235e",
        "Number": "AL19-Baldwin-PORTFOLIO-B4",
        "County": {
          "Id": 2,
          "Name": "Baldwin"
        },
        "State": {
          "Id": 1,
          "Name": "AL"
        },
        "Type": {
          "Id": 3,
          "Name": "Portfolio"
        },
        "AuctionType": {
          "Id": 1,
          "Name": "RFP"
        },
        "FundingDate": "2019-03-26T00:00:00",
        "SaleDate": "2019-03-26T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": false,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      },
      {
        "Id": "0ae7ca9a-61a5-4200-baf9-a322eee5b1e5",
        "Number": "ME19-S11-25000-NEW-B1",
        "County": {
          "Id": 1021,
          "Name": "S11-25000"
        },
        "State": {
          "Id": 20,
          "Name": "ME"
        },
        "Type": {
          "Id": 1,
          "Name": "New"
        },
        "AuctionType": {
          "Id": 2,
          "Name": "Auction - Live"
        },
        "FundingDate": null,
        "SaleDate": "2019-04-03T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": false,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      },
      {
        "Id": "0d06d658-7d28-4cb0-9d8f-3564134be566",
        "Number": "AL19-POLTAVA OBLAST-PUBLICSALE-B3",
        "County": {
          "Id": 1098,
          "Name": "POLTAVA OBLAST"
        },
        "State": {
          "Id": 1,
          "Name": "AL"
        },
        "Type": {
          "Id": 4,
          "Name": "Public Sale"
        },
        "AuctionType": {
          "Id": 1,
          "Name": "RFP"
        },
        "FundingDate": "2019-09-19T00:00:00",
        "SaleDate": "2019-09-12T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": false,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      },
      {
        "Id": "0d2b25b3-2001-4669-bb77-89f69aba4246",
        "Number": "AL19-Autauga-NEW-B9",
        "County": {
          "Id": 1,
          "Name": "Autauga"
        },
        "State": {
          "Id": 1,
          "Name": "AL"
        },
        "Type": {
          "Id": 1,
          "Name": "New"
        },
        "AuctionType": {
          "Id": 1,
          "Name": "RFP"
        },
        "FundingDate": null,
        "SaleDate": "2019-08-29T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": false,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      },
      {
        "Id": "0d380c20-5ab6-487f-a288-124996d6f468",
        "Number": "MD19-MD_County-NEW-B7",
        "County": {
          "Id": 1094,
          "Name": "MD_County"
        },
        "State": {
          "Id": 21,
          "Name": "MD"
        },
        "Type": {
          "Id": 1,
          "Name": "New"
        },
        "AuctionType": {
          "Id": 3,
          "Name": "Auction - Online"
        },
        "FundingDate": null,
        "SaleDate": "2019-05-31T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": true,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      },
      {
        "Id": "17c74d2d-5abc-404f-877c-b8a19401e1f0",
        "Number": "AL19-Baldwin-PORTFOLIO-B2",
        "County": {
          "Id": 2,
          "Name": "Baldwin"
        },
        "State": {
          "Id": 1,
          "Name": "AL"
        },
        "Type": {
          "Id": 3,
          "Name": "Portfolio"
        },
        "AuctionType": {
          "Id": 1,
          "Name": "RFP"
        },
        "FundingDate": "2019-03-28T00:00:00",
        "SaleDate": "2019-03-28T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": false,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      },
      {
        "Id": "1b980571-342f-451e-b708-adc212bcaf63",
        "Number": "CA19-QUINCY-NEW-B2",
        "County": {
          "Id": 1100,
          "Name": "QUINCY"
        },
        "State": {
          "Id": 5,
          "Name": "CA"
        },
        "Type": {
          "Id": 1,
          "Name": "New"
        },
        "AuctionType": {
          "Id": 1,
          "Name": "RFP"
        },
        "FundingDate": null,
        "SaleDate": "2019-07-10T00:00:00",
        "CurrentTask": null, 
        
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": false,
        "IsRejectReasonRequired": false,
        "AssignedTo": null
      }
    ]
    render(<EventTableComponent eventList={eventList} />)
  })
})