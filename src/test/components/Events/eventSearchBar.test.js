import React from "react";
import { render, fireEvent } from '@testing-library/react'
import { EventsSearchBar } from "../../../components/Events/eventSearchBar"


describe("Events Search bar component", () => {
  it("renders without crashing", () => {
    render(<EventsSearchBar />)
  })
  it("fires click search icon event", () => {
    const searchEventParam = {
      SortField: 'SaleDate',
      SortOrder: 'desc',
      FullSearch: '',
      Limit: 10,
      offset: 0,
      Filter: {
        StateId: null,
        Type: null,
        IsLockedStatus: false,
        TaskType: null,
        AssignedTo: ""
      }
    }
    const mockFn1 = jest.fn()
    const mockFn2 = jest.fn()
    const mockFn3 = jest.fn()
    const container = render(<EventsSearchBar searchEventParam={searchEventParam} updateSearchEventParam={mockFn1}
      updateEventsPageNumber={mockFn2} getEventList={mockFn3} />)
    const searchClick = container.getByAltText("search")
    fireEvent.click(searchClick)
  })
})