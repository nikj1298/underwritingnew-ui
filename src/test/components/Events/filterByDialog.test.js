import React from 'react'
import { render } from '@testing-library/react'
import FilterByDialog from '../../../components/Events/filterByDialog'

const mockFn = jest.fn()


describe('Event List Component', () => {
    it('renders and matches snapshot', () => {
        const allStatesList = {
            allStatesList: [{}]
        }
        const data = {}
        const eventData = [{
            "Id": "00865817-97d0-4a62-ada6-acfef59db7b3",
            "Number": "WY19-Lviv 100020-PUBLICSALE-B1",
            "County": {
                "Id": 1081,
                "Name": "Lviv 100020"
            },
            "State": {
                "Id": 51,
                "Name": "WY"
            },
            "Type": {
                "Id": 4,
                "Name": "Public Sale"
            },
            "AuctionType": {
                "Id": 4,
                "Name": "Negotiated Contract"
            },
            "FundingDate": "2019-05-31T00:00:00",
            "SaleDate": "2019-03-28T00:00:00",
            "CurrentTask": null,
            "DueDate": null,
            "Progress": null,
            "IsLocked": false,
            "IsAssigned": true,
            "IsRejectReasonRequired": false,
            "AssignedTo": null
        },
        {
            "Id": "04850a8e-627a-4f2e-a513-e247bc4d5b6d",
            "Number": "AK19-India-SUB-B1",
            "County": {
                "Id": 1146,
                "Name": "India"
            },
            "State": {
                "Id": 2,
                "Name": "AK"
            },
            "Type": {
                "Id": 2,
                "Name": "Sub"
            },
            "AuctionType": {
                "Id": 2,
                "Name": "Auction - Live"
            },
            "FundingDate": "2019-11-03T00:00:00",
            "SaleDate": "2019-11-04T00:00:00",
            "CurrentTask": null,
            "DueDate": null,
            "Progress": null,
            "IsLocked": false,
            "IsAssigned": false,
            "IsRejectReasonRequired": true,
            "AssignedTo": null
        },
        {
            "Id": "06be2618-fbf3-4947-b6e0-3d7cd2301131",
            "Number": "MD19-MD_County-NEW-B2",
            "County": {
                "Id": 1094,
                "Name": "MD_County"
            },
            "State": {
                "Id": 21,
                "Name": "MD"
            },
            "Type": {
                "Id": 1,
                "Name": "New"
            },
            "AuctionType": {
                "Id": 2,
                "Name": "Auction - Live"
            },
            "FundingDate": null,
            "SaleDate": "2019-05-01T00:00:00",
            "CurrentTask": null,
            "DueDate": null,
            "Progress": null,
            "IsLocked": false,
            "IsAssigned": true,
            "IsRejectReasonRequired": false,
            "AssignedTo": null
        }]
        render(
            <FilterByDialog
                eventList={eventData}
                validateReviewReport={data}
                reviewReportData={data}
                allStatesList={allStatesList}
                getEventList={mockFn}
                updateSearchEventParam={mockFn}
                updateEventsPageNumber={mockFn}
                getAllStatesList={mockFn}
                getEventsTypesList={mockFn}
                updateReviewReportData={mockFn}
                validateReviewReportData={mockFn}
                searchEventParam={{
                    Filter : {
                        StateId : ''
                    }
                }}
                allStatesList={
                    {allStatesList:[]}
                }
                eventTypesList={
                    {eventTypesList:[]}
                }
            />
        )
    })
    it('renders and matches snapshot without eventList', () => {
        render(
            <FilterByDialog
                eventList={undefined}
                allStatesList={undefined}
                getEventList={mockFn}
                updateSearchEventParam={mockFn}
                updateEventsPageNumber={mockFn}
                getAllStatesList={mockFn}
                getEventsTypesList={mockFn}
                searchEventParam={{
                    Filter : {
                        StateId : ''
                    }
                }}
                allStatesList={
                    {allStatesList:[]}
                }
                eventTypesList={
                    {eventTypesList:[]}
                }
            />
        )
    })
})
