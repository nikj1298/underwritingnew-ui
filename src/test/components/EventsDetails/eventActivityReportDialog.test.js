import React from 'react'
import { shallow, configure } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import ActivityReportDialog from '../../../components/Events/eventActivityReportDialog'
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const ComponentUse = unwrap(ActivityReportDialog)
const mockFn = jest.fn()

const allStatesList = {
    allStatesList: [{}]
}

const classes = {
    formControl: {}
}

const data = {}


describe('Event List Component', () => {
    it('onclick to open dialog with userReport props', () => {
        const wrapper = shallow(<ComponentUse userReport={true} reviewReportData={data} validateReviewReport={{}}
            allStatesList={allStatesList} updateReviewReportData={mockFn} setOpen={mockFn}
            classes={{}} setDialogName={mockFn} />);
        wrapper.find("#activityReportButton").simulate('click')
    })
    it('onclick to open dialog without userReport props', () => {
        const wrapper = shallow(<ComponentUse userReport={false} reviewReportData={data}
            validateReviewReport={{}} setDialogName={mockFn} setOpen={mockFn}
            allStatesList={allStatesList} updateReviewReportData={mockFn} classes={{}} />);
        wrapper.find("#activityReportButton").simulate('click')
    })
    it("generate click of dialog with in-valid fields", () => {
        const reviewReportData = {
            SaleDateFrom: "12-12-2019",
            SaleDateTo: "12-11-2019",
            StateId: 2
        }
        const wrapper = shallow(<ComponentUse userReport={true} reviewReportData={reviewReportData}
            updateReviewReportData={mockFn} dialogName="Activity" setDialogName={mockFn}
            validateReviewReport={{}} validateReviewReportData={mockFn}
            classes={classes} allStatesList={allStatesList} />)
        wrapper.find("#addReminderDialog").find(".widthaddReminder").find("#reviewReportActions").find("#generateActivityReport").simulate('click')
    })
    it("generate click of dialog without in-valid fields", () => {
        const reviewReportData = {
            SaleDateFrom: "12-10-2019",
            SaleDateTo: "12-11-2019",
            StateId: 2,
            IsEventLocked: false
        }
        const wrapper = shallow(<ComponentUse userReport={true} reviewReportData={reviewReportData}
            updateReviewReportData={mockFn} dialogName="Activity" setDialogName={mockFn}
            validateReviewReport={{}} validateReviewReportData={mockFn} classes={classes}
            allStatesList={allStatesList} generateDownloadReport={mockFn} />)
        wrapper.find("#addReminderDialog").find(".widthaddReminder").find("#reviewReportActions").find("#generateActivityReport").simulate('click')
    })
})
