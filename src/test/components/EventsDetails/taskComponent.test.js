import React from 'react'
import { render } from '@testing-library/react'
import TaskComponent from '../../../components/EventsDetails/taskComponent'

const mockFn = jest.fn()


describe('Event List Component', () => {
    it('renders and matches snapshot', () => {
        render(
            <TaskComponent
                taskList={{ taskList: { List: [], TotalCount : 5 } }}
                getTasksList={jest.fn(() => {
                    return Promise.resolve({})
                })}
            />
        )
    })
    it('renders and matches snapshot without eventList', () => {
        render(
            <TaskComponent
                taskList={{
                    taskList: {
                        List: [
                            {
                                "Event": {
                                    "Id": "04850a8e-627a-4f2e-a513-e247bc4d5b6d",
                                    "Name": "AK19-India-SUB-B1"
                                },
                                "LogicType": {
                                    "Id": 21,
                                    "Name": "Certificates"
                                },
                                "EventType": {
                                    "Id": 2,
                                    "Name": "Sub"
                                },
                                "State": {
                                    "Id": 2,
                                    "Name": "AK"
                                },
                                "County": {
                                    "Id": 1146,
                                    "Name": "India"
                                },
                                "Id": "fa1b0cf9-a184-4004-b0c1-c88907389803",
                                "Name": "Certificates (Mail Merge)",
                                "Order": 0,
                                "InstanceNumber": 1,
                                "Status": {
                                    "Id": 3,
                                    "Name": "Overdue"
                                },
                                "CompletionType": {
                                    "Id": 2,
                                    "Name": "Any"
                                },
                                "DueDate": "2019-11-07T00:00:00Z",
                                "Users": [
                                    {
                                        "Id": "67e0c5a0-e878-4dde-8086-d5943ca281d6",
                                        "Name": "Ajay Kumar",
                                        "IsActive": true,
                                        "CompletedDate": null
                                    },
                                    {
                                        "Id": "e5cb304f-8b45-4e27-a2f5-e15d3d60fe80",
                                        "Name": "Aditi Srivastava",
                                        "IsActive": true,
                                        "CompletedDate": null
                                    }
                                ],
                                "Departments": [
                                    {
                                        "Id": 5,
                                        "Name": "Asset Management"
                                    }
                                ]
                            }
                        ], TotalCount : 5
                    }
                }}
                getTasksList={jest.fn(() => {
                    return Promise.resolve({})
                })}
            />
        )
    })
})
