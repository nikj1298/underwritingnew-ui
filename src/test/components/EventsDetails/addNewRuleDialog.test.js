import React from 'react'
import { render } from '@testing-library/react'
import AddNewRuleDialog from '../../../components/EventsDetails/addNewRuleDialog'

const mockFn = jest.fn()


describe('Event List Component', () => {
    it('renders and matches snapshot', () => {
        render(
            <AddNewRuleDialog postsaveAllNewDataRules={jest.fn(() => {
                return Promise.resolve({ data : {
                    List : [{"Id":2,"Name":"Data Approve"},{"Id":1,"Name":"Data Reject"}]
                } })
            })}
            refreshList={jest.fn(() => {
                return Promise.resolve({ data : {
                    List : []
                } })
            })}
            />
        )
    })
    it('renders and matches snapshot without eventList', () => {
        render(
            <AddNewRuleDialog postsaveAllNewDataRules={jest.fn(() => {
                return Promise.resolve({ data : {
                    List : []
                } })
            })} 
            refreshList={jest.fn(() => {
                return Promise.resolve({ data : {
                    List : []
                } })
            })}
            />
        )
    })
})
