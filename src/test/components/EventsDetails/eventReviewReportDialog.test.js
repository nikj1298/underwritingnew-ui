import React from 'react'
import { shallow, configure } from "enzyme";
import { unwrap } from "@material-ui/core/test-utils";
import EventReviewReportDialog from '../../../components/Events/eventReviewReportDialog'
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

const ComponentUse = unwrap(EventReviewReportDialog)
const mockFn = jest.fn()
describe('Event List Component', () => {
    it('onclick to open dialog', () => {
        const wrapper = shallow(<ComponentUse setDialogName={mockFn} reviewReportData={{}}
            updateReviewReportData={mockFn} classes={{}} setOpen={mockFn} />);
        wrapper.find("#openButton").simulate('click')
    })
    it("generate click of dialog with all fields", () => {
        const reviewReportData = {
            SaleDateFrom: "12-10-2019",
            SaleDateTo: "12-11-2019",
            StateId: 2
        }
        const wrapper = shallow(<ComponentUse dialogName="Review" reviewReportData={reviewReportData} updateReviewReportData={mockFn}
            validateReviewReport={{}} validateReviewReportData={mockFn} generateDownloadReport={mockFn} />)
        wrapper.find("#addReminderDialog").find(".widthaddReminder").find("#reportDialogAction").find("#reportGenButton").simulate('click')
    })
    it("generate click of dialog without in-valid fields", () => {
        const reviewReportData = {
            SaleDateFrom: "12-12-2019",
            SaleDateTo: "12-11-2019",
            StateId: 2
        }
        const wrapper = shallow(<ComponentUse generateDownloadReport={mockFn} setDialogName={mockFn}
            dialogName="Review" reviewReportData={reviewReportData} updateReviewReportData={mockFn}
            validateReviewReport={{}} validateReviewReportData={mockFn} />)
        wrapper.find("#addReminderDialog").find(".widthaddReminder").find("#reportDialogAction")
            .find("#reportGenButton").simulate('click')
    })
})
