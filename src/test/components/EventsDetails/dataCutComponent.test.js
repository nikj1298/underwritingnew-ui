
import React from 'react'
import { render } from '@testing-library/react'
import DataCutComponent from '../../../components/EventsDetails/dataCutComponent'

const mockFn = jest.fn()


describe('Event List Component', () => {
    it('renders and matches snapshot', () => {
        render(
            <DataCutComponent 
            getRulesAprroveRejectTypes={jest.fn(() => {
                return Promise.resolve({ data : {
                    List : [{"Id":2,"Name":"Data Approve"},{"Id":1,"Name":"Data Reject"}]
                } })
            })}
            getAllRulesList={jest.fn(() => {
                return Promise.resolve({ data : {
                    List : [{"Id":"81466144-d95e-4841-ae76-a176349e70be","Name":"TEST 2","IsAttached":false,"DataCutResultType":{"Id":2,"Name":"Data Approve"},"DataCutRuleItems":[{"Value":"1","DataCutLogicType":{"Id":2,"Name":"Does Not Contain"},"DataCutRuleField":{"Id":1,"Name":"Account Name"}},{"Value":"2","DataCutLogicType":{"Id":2,"Name":"Does Not Contain"},"DataCutRuleField":{"Id":5,"Name":"General Land Use Code"}},{"Value":"3","DataCutLogicType":{"Id":3,"Name":"Equal"},"DataCutRuleField":{"Id":2,"Name":"Property Address"}}]},{"Id":"afd42140-f8c1-4719-a5be-fa0d56960673","Name":"Test","IsAttached":false,"DataCutResultType":{"Id":2,"Name":"Data Approve"},"DataCutRuleItems":[{"Value":"10","DataCutLogicType":{"Id":5,"Name":"Less Than"},"DataCutRuleField":{"Id":12,"Name":"LTV%"}}]}]
                } })
            })}
            />
        )
    })
    it('renders and matches snapshot without eventList', () => {
        render(
            <DataCutComponent 
            getRulesAprroveRejectTypes={jest.fn(() => {
                return Promise.resolve({ data : {
                    List : [{"Id":2,"Name":"Data Approve"},{"Id":1,"Name":"Data Reject"}]
                } })
            })}
            getAllRulesList={jest.fn(() => {
                return Promise.resolve({ data : {
                    List : [{"Id":"81466144-d95e-4841-ae76-a176349e70be","Name":"TEST 2","IsAttached":false,"DataCutResultType":{"Id":2,"Name":"Data Approve"},"DataCutRuleItems":[{"Value":"1","DataCutLogicType":{"Id":2,"Name":"Does Not Contain"},"DataCutRuleField":{"Id":1,"Name":"Account Name"}},{"Value":"2","DataCutLogicType":{"Id":2,"Name":"Does Not Contain"},"DataCutRuleField":{"Id":5,"Name":"General Land Use Code"}},{"Value":"3","DataCutLogicType":{"Id":3,"Name":"Equal"},"DataCutRuleField":{"Id":2,"Name":"Property Address"}}]},{"Id":"afd42140-f8c1-4719-a5be-fa0d56960673","Name":"Test","IsAttached":false,"DataCutResultType":{"Id":2,"Name":"Data Approve"},"DataCutRuleItems":[{"Value":"10","DataCutLogicType":{"Id":5,"Name":"Less Than"},"DataCutRuleField":{"Id":12,"Name":"LTV%"}}]}]
                } })
            })}
            />
        )
    })
})
