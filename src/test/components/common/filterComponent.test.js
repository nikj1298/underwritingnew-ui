import React from "react";
import { render } from '@testing-library/react'
import { FilterActionButtons, FilterPopupHeader } from "../../../components/common/filterComponent"

describe("filter component", () => {
  it("FilterActionButtons", () => {
    render(<FilterActionButtons popupState={{}} applyOrClearFilter={jest.fn()} />)
  })
  it("FilterPopupHeader", () => {
    render(<FilterPopupHeader resetFilterLastApplied={jest.fn()} />)
  })
})