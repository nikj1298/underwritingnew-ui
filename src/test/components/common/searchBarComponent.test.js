import React from "react";
import { render } from '@testing-library/react'
import Adapter from 'enzyme-adapter-react-16'
import { SearchBarComponent } from "../../../components/common/searchBarComponent"
import { shallow, configure } from 'enzyme';

configure({ adapter: new Adapter() });

const fieldProps = {
  inputValue: '',
  showCross: false,
  handleInputValue: jest.fn(),
  setShowCross: jest.fn(),
  searchInputText: jest.fn(),
  clearGlobalSearch: jest.fn()
}

const fieldProps1 = {
  inputValue: '',
  showCross: true,
  handleInputValue: jest.fn(),
  setShowCross: jest.fn(),
  searchInputText: jest.fn(),
  clearGlobalSearch: jest.fn()
}

describe("Search Bar Component", () => {
  it("renders without crashing", () => {
    render(<SearchBarComponent {...fieldProps} />)
  })
  it("renders without crashing with showCross true", () => {
    render(<SearchBarComponent {...fieldProps1} />)
  })
  it('fires click event', () => {
    const wrapper = shallow(<SearchBarComponent {...fieldProps} />)
    const event = {}
    wrapper.find(".searchInput").find('.searchIcon').simulate('click', event)
  })
  it('fires keyPress event', () => {
    const wrapper = shallow(
      <SearchBarComponent {...fieldProps} />
    )
    let event = { key: 'Enter' }
    wrapper.find('#filled-dense-hidden-label').simulate('keyPress', event)
  })
  it('fires onChange event', () => {
    const wrapper = shallow(
      <SearchBarComponent {...fieldProps} />
    )
    let event = { target: { value: 'Regression' } }
    wrapper.find('#filled-dense-hidden-label').simulate('change', event)
  })
})