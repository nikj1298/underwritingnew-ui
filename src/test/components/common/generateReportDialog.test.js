
import React from 'react'
import { render } from '@testing-library/react'
import GenerateReportDialog from '../../../components/Events/generateReportDialog'

const mockFn = jest.fn()


describe('Event List Component', () => {
    it('renders and matches snapshot', () => {
       
        render(
            <generateReportDialog 
            reviewReportData={{}} 
            allStatesList={ {
                allStatesList:[]
            }} 
            classes={{}} 
            validateReviewReport={{}} 
            handleChange={jest.fn()} 
            handleDateChange={jest.fn()}
            />
        )
    })
    it('renders and matches snapshot without eventList', () => {
        render(
            <generateReportDialog
            reviewReportData={{}} 
            allStatesList={ {
                allStatesList:[]
            }} 
            classes={{}} 
            validateReviewReport={{}} 
            handleChange={jest.fn()} 
            handleDateChange={jest.fn()}
            />
        )
    })
})
