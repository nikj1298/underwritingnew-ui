import React from 'react'
import { shallow, configure } from 'enzyme'
import { cleanup } from '@testing-library/react'
import { DropDownMenu } from '../../../components/common/dropDownMenu'
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('DropDownMenu', () => {
	beforeEach(() => {
		document.body.innerHTML = '<div><div id="overLayFixed"  /> <div id="dropDownRecordMenu"  /></div>'
	})
	afterEach(cleanup)
	const fieldProps = {
		recordsDropdownMenu: {
			id: 1,
			icon: '',
			name: 'Record'
		},
		menuName: 'Event',
		recordsDropdownMenu: {
			key: 1,
			name: 'Name123'
		},
		data: [],
		updateUrl: jest.fn()
	}

	const container = shallow(<DropDownMenu />)
	const container1 = shallow(<DropDownMenu {...fieldProps} history={[]} />)
	it('renders a <div /> component with expected props', () => {
		expect(container.children().at(0).props().className).toEqual('loanListButton cursorClass')
		expect(container.children().at(0).children.length).toEqual(1)
	})
	it('renders a ul', () => {
		expect(container1.children().at(0).props().className).toEqual('loanListButton cursorClass')
		expect(container1.children().at(1).children.length).toEqual(1)
	})
	it('renders a component with expected props', () => {
		const mockFn1 = jest.fn()
		container.find('#loanListButtonId').props().onClick(mockFn1)
	})
	it('renders a component with expected props', () => {
		const mockFn1 = jest.fn()
		container.find('#overLayFixed').props().onClick(mockFn1)
	})
	it('renders a component with expected props', () => {
		const mockFn1 = jest.fn()
		container1.find('ul').find('#dropDownRecordMenu').find('li').at(0).props().onClick(mockFn1)
	})
})
