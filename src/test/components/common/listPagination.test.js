import React from "react";
import { render, fireEvent, cleanup } from '@testing-library/react'
import { ListPagination } from "../../../components/common/listPagination"
const searchParam = {
  SortField: 'SaleDate',
  SortOrder: 'desc',
  FullSearch: '',
  Limit: 10,
  offset: 0,
  Filter: {
    StateId: null,
    Type: null,
    IsLockedStatus: false,
    TaskType: null,
    AssignedTo: ""
  }
}

describe("List Pagination Component", () => {
  afterEach(cleanup)
  it("previous button click with listPageNumber=1 ", () => {
    const pageNumber = 1
    const mockFn = jest.fn(searchParam, pageNumber)
    const container = render(<ListPagination listPageNumber={pageNumber} listTotalCount={10}
      listPageSize={10} searchParam={searchParam} updateProps={mockFn}
    />)
    const previousClick = container.getByLabelText("previous page")
    fireEvent.click(previousClick)
  })
  it("previous button click with listPageNumber != 1", () => {
    const pageNumber = 3
    const mockFn = jest.fn()
    const container = render(<ListPagination listPageNumber={pageNumber} listTotalCount={40}
      listPageSize={10} searchParam={searchParam} updateProps={mockFn}
    />)
    const previousClick = container.getByLabelText("previous page")
    fireEvent.click(previousClick)
  })
  it("next button click with listPageSize < listTotalCount", () => {
    const pageNumber = 3
    const mockFn = jest.fn()
    const container = render(<ListPagination listPageNumber={pageNumber} listTotalCount={40}
      listPageSize={10} searchParam={searchParam} updateProps={mockFn}
    />)
    const previousClick = container.getByLabelText("next page")
    fireEvent.click(previousClick)
  })
  it("next button click with listPageSize > listTotalCount", () => {
    const pageNumber = 1
    const mockFn = jest.fn()
    const container = render(<ListPagination listPageNumber={pageNumber} listTotalCount={8}
      listPageSize={10} searchParam={searchParam} updateProps={mockFn}
    />)
    const previousClick = container.getByLabelText("next page")
    fireEvent.click(previousClick)
  })
})