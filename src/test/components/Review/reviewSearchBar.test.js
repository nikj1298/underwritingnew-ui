import React from "react";
import { render, fireEvent } from '@testing-library/react'
import { ReviewSearchBar } from "../../../components/Review/reviewSearchBar"


describe("Review Search bar component", () => {
  it("renders without crashing", () => {
    render(<ReviewSearchBar />)
  })
  it("fires click search icon event", () => {
    const searchEventParam = {
      offset: 0,
      limit: 10,
      Filter: {
        MoveForward: true,
        ReviewDecision: 0,
        EventId: null,
        AssignmentByUser: true
      },
      FullSearch: "",
      SortField: 'AccountName',
      SortOrder: 'desc'
    }
    const mockFn1 = jest.fn()
    const mockFn2 = jest.fn()
    const mockFn3 = jest.fn()
    const container = render(<ReviewSearchBar getPropertiesParam={searchEventParam} updateGetPropertyListParam={mockFn1}
      updateEventPropertyPageNumber={mockFn2} fetchEventProperties={mockFn3} />)
    const searchClick = container.getByAltText("search")
    fireEvent.click(searchClick)
  })
})