import React from 'react'
import { render } from '@testing-library/react'
import { ReviewFilterTab } from '../../../components/Review/reviewFilterTabs'

describe("Review Filter Tab", () => {
  it("renders without crashing", () => {
    const value = ""
    const handleChange = jest.fn()
    render(<ReviewFilterTab value={value} handleChange={handleChange} />)
  })
})