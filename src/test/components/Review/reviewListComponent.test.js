import React from 'react'
import { render } from '@testing-library/react'
import ReviewListComponent from '../../../components/Review/reviewListComponent'

const mockFn = jest.fn()


describe('Event List Component', () => {
  it('renders and matches snapshot', () => {

    const data = {}
    const eventData = [{
      "PropertyId": "be113672-6b4d-40e0-90f0-2ab83e497ee3",
      "Scoring": 2,
      "DisplayStrategy": [],
      "GeneralLandUseCode": {
        "Id": 1,
        "Name": "Residential"
      },
      "InternalLandUseCode": null,
      "DelinquencyYear": 2018,
      "County": {
        "Id": 390,
        "Name": "Pemberton Township (Burlington)"
      },
      "Event": {
        "Id": "97114ae4-7d78-40ca-8f06-208ae99b8a4d",
        "Number": "AZ19-Apache-NEW-B1",
        "County": null,
        "State": null,
        "Type": {
          "Id": 1,
          "Name": "New"
        },
        "AuctionType": {
          "Id": 3,
          "Name": "Auction - Online"
        },
        "FundingDate": "2019-09-16T00:00:00",
        "SaleDate": "2019-09-16T00:00:00",
        "CurrentTask": null,
        "DueDate": null,
        "Progress": null,
        "IsLocked": false,
        "IsAssigned": true,
        "IsRejectReasonRequired": true,
        "AssignedTo": null
      },
      "LandStateCode": "2",
      "ImprovementStateCode": null,
      "IsHomestead": null,
      "TaxRatio": 0.0020658530042918454,
      "TaxId": null,
      "CadId": null,
      "ThirdPartyForeclosure": false,
      "Bankruptcy": false,
      "Veteran": false,
      "PaymentPlan": false,
      "Mortgage": false,
      "DisabilityExemption": false,
      "Over65SurvivingSpouse": false,
      "LegalDescription": null,
      "LandAcres": null,
      "BuildingSqFt": null,
      "YearBuilt": null,
      "Latitude": null,
      "Longitude": null,
      "IsLatestPropertyData": true,
      "Supplemental": {
        "AdvertisementBatch": "C19-PEMB-00470",
        "RecentBuyerRate": null,
        "RecentBuyerName": null,
        "ClosedLiens": 100,
        "OpenLiens": null,
        "InspectorLawnMaintained": null,
        "InspectorRoofCondition": null,
        "InspectorOccupied": null,
        "InspectorAreaRating": null,
        "InspectorPropertyRating": null,
        "InspectorComment": null,
        "LastSaleAmount": null,
        "LastSaleDate": null,
        "AdvertisementNumber": "470",
        "AssessorURL": "https://tre-dotnet.state.nj.us/TYTR_TLSPS/TaxListSearch.aspx",
        "TreasurerURL": null,
        "GisURL": null,
        "MortgageList": [
          {
            "MortgageDataNumber": 1,
            "MortgageLender": null,
            "MortgageLoanAmount": null,
            "MortgageOriginationDate": null,
            "MortgageMaturityDate": null
          },
          {
            "MortgageDataNumber": 2,
            "MortgageLender": null,
            "MortgageLoanAmount": null,
            "MortgageOriginationDate": null,
            "MortgageMaturityDate": null
          }
        ]
      },
      "Decisions": [
        {
          "Id": "9c8ae5b4-0286-4f1d-8223-a05899c00f57",
          "UserId": "08b91054-216a-4e71-a36c-9168f8a2f455",
          "Type": {
            "Id": 1,
            "Name": "Reject"
          },
          "Comment": "",
          "Level": {
            "Id": "57a86766-4bd5-48e8-a1ad-e7f9d20b13a3",
            "Name": "Level 1",
            "Order": 0,
            "IsFinal": false
          },
          "DecisionDate": "2019-12-13T06:42:54.099917"
        },
        {
          "Id": "8e1af879-4c19-458e-8218-c5d87ba25718",
          "UserId": "f54d59dd-13ed-4e56-874d-aae0f1977951",
          "Type": null,
          "Comment": null,
          "Level": {
            "Id": "0d57a1db-cae3-404a-a660-878d42f8c0e5",
            "Name": "Level 2",
            "Order": 1,
            "IsFinal": false
          },
          "DecisionDate": null
        },
        {
          "Id": "c62c2bf4-7abf-4591-8e76-0f40d1b02643",
          "UserId": "af809548-f8a3-4d65-b661-21c67317dd69",
          "Type": null,
          "Comment": null,
          "Level": {
            "Id": "619e8fde-1316-44d4-b02d-e04f2739c989",
            "Name": "Level Final",
            "Order": 2,
            "IsFinal": true
          },
          "DecisionDate": null
        }
      ],
      "CreatedOn": "2019-09-18T14:39:21.379096",
      "CreatedBy": null,
      "ModifiedOn": "2019-12-16T06:55:26.408122",
      "ModifiedBy": null,
      "DeletedOn": null,
      "DataCutDecision": null,
      "Id": "fdf627e2-d95d-4a5e-97e5-60c2a59ce9ed",
      "ParcelId": "29-00635-00001",
      "City": "Pemberton",
      "ZipCode": null,
      "CountyId": 390,
      "Lead": {
        "Id": "130353d6-38a5-48f5-8c68-a29c60ce32a5",
        "Name": "FUENTES, OCTAVIO",
        "Address": {
          "State": {
            "Id": 31,
            "Name": "NJ"
          },
          "City": "BROWNS MILLS",
          "Address1": "323 SPRING LAKE BLVD",
          "Address2": null,
          "Address3": null,
          "Zip": "08015"
        }
      },
      "Address": {
        "State": {
          "Id": 31,
          "Name": "NJ"
        },
        "City": "Pemberton",
        "Address1": "323 SPRING LAKE BLVD",
        "Address2": null,
        "Address3": null,
        "Zip": null
      },
      "AppraisedValue": 186400,
      "AmountDue": 924.18,
      "Ltv": 0.0025,
      "RuLtv": 0,
      "RuAmount": 0,
      "LandUseCode": "2 - Residential (1-4 Units)",
      "LandValue": 38500,
      "ImprovementValue": 147900,
      "CurrentDecision": {
        "Id": 1,
        "Name": "Reject"
      },
      "PropertyAttachments": []
    }]
    const getPropertiesList = {
      getPropertiesList: eventData
    }
    render(
      <ReviewListComponent
        listTotalCount={2}
        validateReviewReport={data}
        getPropertiesList={getPropertiesList}
        updateGetPropertyListParam={mockFn}
        updateEventPropertyPageNumber={mockFn}
        getAllStatesList={mockFn}
        fetchEventProperties={mockFn}
        getAllEventsList={mockFn}
        fetchAllLevels={mockFn}
        getAllGeneralLandUseCode={mockFn}
        getAllInternalLandUseCode={mockFn}
      />
    )
  })
  it('renders and matches snapshot without eventList', () => {
    render(
      <ReviewListComponent
        listTotalCount={2}
        updateGetPropertyListParam={mockFn}
        updateEventPropertyPageNumber={mockFn}
        getAllStatesList={mockFn}
        fetchEventProperties={mockFn}
        getAllEventsList={mockFn}
        fetchAllLevels={mockFn}
        getAllGeneralLandUseCode={mockFn}
        getAllInternalLandUseCode={mockFn}
      />
    )
  })
})
