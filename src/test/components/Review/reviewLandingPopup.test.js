import React from 'react'
import { render } from '@testing-library/react'
import ReviewLandingPopup from '../../../components/Review/reviewLandingPopup'

describe("Review Landing Pop-up", () => {
  it("should render without crashing", () => {
    const fieldProps = {
      getAllEvents: {
        getAllEvents: [{}]
      },
      classes: {},
      getPropertiesParam: {
        offset: 0,
        limit: 10,
        Filter: {
          MoveForward: true,
          ReviewDecision: 0,
          EventId: null,
          AssignmentByUser: true
        },
        FullSearch: "",
        SortField: 'AccountName',
        SortOrder: 'desc'
      },
      allEventParam: {
        Filter: {
          IsLocked: false
        },
        offset: 0,
        limit: 2000
      },
      fetchEventProperties: jest.fn(),
      updateGetPropertyListParam: jest.fn(),
      updateGetAllEventsParam: jest.fn(),
      getAllEventsList: jest.fn(),
    }
    render(<ReviewLandingPopup {...fieldProps} />)
  })
})