import React from 'react'
import { render } from '@testing-library/react'
import BidTable from '../../../components/EventsDetails/bidTable'

const mockFn = jest.fn()


describe('Event List Component', () => {
    it('renders and matches snapshot', () => {
        render(
            <BidTable
                list={[{
                    Entity: "13213",
                    EventId: "04850a8e-627a-4f2e-a513-e247bc4d5b6d",
                    Id: "9b1565f8-d5a4-4f10-aebe-084886e1287e",
                    Number: "123",
                    Portfolio: "12343",
                }]}

            />
        )
    })
    it('renders and matches snapshot without eventList', () => {
        render(
            <BidTable
                list={[{
                    Entity: "13213",
                    EventId: "04850a8e-627a-4f2e-a513-e247bc4d5b6d",
                    Id: "9b1565f8-d5a4-4f10-aebe-084886e1287e",
                    Number: "123",
                    Portfolio: "12343",
                }]}
            />
        )
    })
})
