import React from 'react'
import { render } from '@testing-library/react'
import BidList from '../../../components/EventsDetails/bidList'

const mockFn = jest.fn()


describe('Event List Component', () => {
    it('renders and matches snapshot', () => {
        render(
            <BidList
                eventList={[]}
                getBidsListEventsList={jest.fn(() => Promise.resolve({ data: {} }))}
            />
        )
    })
    it('renders and matches snapshot without eventList', () => {
        render(
            <BidList
                eventList={undefined}
                getBidsListEventsList={jest.fn(() => Promise.resolve({ data: {} }))}
            />
        )
    })
})
