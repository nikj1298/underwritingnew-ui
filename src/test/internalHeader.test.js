import React from 'react'
import { shallow, configure } from 'enzyme'
import InternalHeader from '../pages/InternalHeader'
import { render } from '@testing-library/react'

import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('InternalHeader Component', () => {
  const fieldProps1 = {
    navigateToList: jest.fn(() => {
      return Promise.resolve([{}])
    })
  }
  it('renders without crashing', () => {
    render(<InternalHeader {...fieldProps1} />)
  })
  const container = shallow(<InternalHeader />)
  const container1 = shallow(<InternalHeader {...fieldProps1} />)
  it('renders component with expected props', () => {
    expect(container.children().at(0).props().className).toEqual('internalHeader headerAlignCenter')
  })
  it('renders a component with expected props', () => {
    let e = {}
    container.find('.notifyIcon').at(0).simulate('click', e)
  })
  it('renders a  component with expected props', () => {
    let e = {}
    container.find('.userBadge').at(0).simulate('click', e)
  })
  it('renders a  component with expected props', () => {
    const e = { preventDefault: jest.fn() }
    container1.find('.userBadge').at(0).simulate('click', e)
  })
  it('renders a  component with expected props', () => {
    const e = { preventDefault: jest.fn() }
    container1.find('.notifyIcon').at(0).simulate('click', e)
  })
})
