
import reducer from '../../reducers/Events/eventsListReducer';
import * as actions from '../../actions/Events/eventsListAction';
import expect from 'expect';
import { searchEventsParam, generateReviewReport, validateReviewReportData } from "../../common/const"

describe('event list reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(
            {
                allStatesList: { allStatesList: undefined, error: null, loading: false },
                eventTypesList: { eventTypesList: undefined, error: null, loading: false },
                eventList: { eventList: undefined, error: null, loading: false },
                eventsCount: 0,
                searchEventParam: searchEventsParam,
                eventPageNumber: 1,
                reviewReportData: generateReviewReport,
                validateReviewReport: validateReviewReportData,
                generateReviewReportData: undefined
            }
        );
    });

    it('should handle FETCH_ALL_STATES_LIST', () => {
        const startAction = {
            type: actions.FETCH_ALL_STATES_LIST,
            payload: undefined
        }
        expect(reducer({}, startAction)).toBeDefined()
    });

    it('should handle FETCH_ALL_STATES_LIST_SUCCESS', () => {
        const successAction = {
            type: actions.FETCH_ALL_STATES_LIST_SUCCESS,
            payload: {
                data: {}
            },
        };
        expect(reducer({}, successAction)).toBeDefined()
    });

    it('should handle FETCH_ALL_STATES_LIST_FAILURE', () => {
        const failAction = {
            type: actions.FETCH_ALL_STATES_LIST_FAILURE,
            payload: { "message": "some error" },
        };
        expect(reducer({}, failAction)).toBeDefined()
    });
    it('should handle FETCH_EVENTS_TYPES', () => {
        const startAction = {
            type: actions.FETCH_EVENTS_TYPES,
            payload: undefined
        }
        expect(reducer({}, startAction)).toBeDefined()
    });

    it('should handle FETCH_EVENTS_TYPES_SUCCESS', () => {
        const successAction = {
            type: actions.FETCH_EVENTS_TYPES_SUCCESS,
            payload: {
                data: {}
            },
        };
        expect(reducer({}, successAction)).toBeDefined()
    });

    it('should handle FETCH_EVENTS_TYPES_FAILURE', () => {
        const failAction = {
            type: actions.FETCH_EVENTS_TYPES_FAILURE,
            payload: { "message": "some error" },
        };
        expect(reducer({}, failAction)).toBeDefined()
    });

    it('should handle FETCH_ALL_EVENTS_LIST', () => {
        const startAction = {
            type: actions.FETCH_ALL_EVENTS_LIST,
            payload: undefined
        }
        expect(reducer({}, startAction)).toBeDefined()
    });

    it('should handle FETCH_ALL_EVENTS_LIST_SUCCESS', () => {
        const successAction = {
            type: actions.FETCH_ALL_EVENTS_LIST_SUCCESS,
            payload: {
                data: {}
            },
        };
        expect(reducer({}, successAction)).toBeDefined()
    });

    it('should handle FETCH_ALL_EVENTS_LIST_FAILURE', () => {
        const failAction = {
            type: actions.FETCH_ALL_EVENTS_LIST_FAILURE,
            payload: { "message": "some error" },
        };
        expect(reducer({}, failAction)).toBeDefined()
    });
    it("should handle UPDATE_EVENT_PAGE_NUMBER", () => {
        const action = {
            type: actions.UPDATE_EVENT_PAGE_NUMBER,
            payload: {
                pageNumber: 2
            }
        }
        expect(reducer({}, action)).toEqual({ eventPageNumber: 2 })
    })
    it("should handle UPDATE_SEARCH_EVENT_PARAM", () => {
        const action = {
            type: actions.UPDATE_SEARCH_EVENT_PARAM,
            payload: {
                SortField: 'SaleDate',
                SortOrder: 'desc',
                FullSearch: '',
                Limit: 10,
                offset: 0,
                Filter: {
                    StateId: null,
                    Type: null,
                    IsLockedStatus: false,
                    TaskType: null,
                    AssignedTo: ""
                }
            }
        }
        expect(reducer({}, action)).toEqual({ searchEventParam: action.payload })
    })
    it("should handle UPDATE_REVIEW_REPORT_DATA", () => {
        const action = {
            type: actions.UPDATE_REVIEW_REPORT_DATA,
            payload: generateReviewReport
        }
        expect(reducer({}, action)).toEqual({ reviewReportData: generateReviewReport })
    })
    it("should handle VALIDATE_REVIEW_REPORT_DATA", () => {
        const action = {
            type: actions.VALIDATE_REVIEW_REPORT_DATA,
            payload: validateReviewReportData
        }
        expect(reducer({}, action)).toEqual({ validateReviewReport: validateReviewReportData })
    })
    it("should handle GENERATE_REVIEW_REPORT", () => {
        const action = {
            type: actions.GENERATE_REVIEW_REPORT,
            payload: undefined
        }
        expect(reducer({}, action)).toEqual({ generateReviewReportData: undefined })
    })
    it("should handle GENERATE_REVIEW_REPORT_FAILURE", () => {
        const action = {
            type: actions.GENERATE_REVIEW_REPORT_FAILURE,
            payload: undefined
        }
        expect(reducer({}, action)).toEqual({ generateReviewReportData: undefined })
    })

});
