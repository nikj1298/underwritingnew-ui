import reducer from '../../reducers/Review/reviewListReducer';
import * as actions from '../../actions/Review/reviewListAction';
import expect from 'expect';
import { getEventsParam, filterPropertiesParam } from "../../common/const"

describe("review list reducer", () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        getAllEvents: undefined,
        allEventParam: getEventsParam,
        getPropertiesParam: filterPropertiesParam,
        getPropertiesList: undefined,
        eventPropertyPageNumber: 1
      }
    );
  })
  it('should handle GET_ALL_EVENTS', () => {
    const startAction = {
      type: actions.GET_ALL_EVENTS,
      payload: undefined
    }
    expect(reducer({}, startAction)).toBeDefined()
  });

  it('should handle GET_ALL_EVENTS_SUCCESS', () => {
    const successAction = {
      type: actions.GET_ALL_EVENTS_SUCCESS,
      payload: {
        data: {}
      },
    };
    expect(reducer({}, successAction)).toBeDefined()
  });

  it('should handle GET_ALL_EVENTS_FAILURE', () => {
    const failAction = {
      type: actions.GET_ALL_EVENTS_FAILURE,
      payload: { "message": "some error" },
    };
    expect(reducer({}, failAction)).toBeDefined()
  });
  it("should handle UPDATE_ALL_EVENT_PARAM", () => {
    const action = {
      type: actions.UPDATE_ALL_EVENT_PARAM,
      payload: {
        SortField: 'SaleDate',
        SortOrder: 'desc',
        FullSearch: '',
        Limit: 10,
        offset: 0,
        Filter: {
          StateId: null,
          Type: null,
          IsLockedStatus: null,
          TaskType: null,
          AssignedTo: ""
        }
      }
    }
    expect(reducer({}, action)).toEqual({ allEventParam: action.payload })
  })
  it("should handle UPDATE_GET_PROPERTIES_PARAM", () => {
    const action = {
      type: actions.UPDATE_GET_PROPERTIES_PARAM,
      payload: {
        offset: 0,
        limit: 10,
        Filter: {
          MoveForward: true,
          ReviewDecision: 0,
          EventId: null,
          AssignmentByUser: true
        },
        FullSearch: "",
        SortField: 'AccountName',
        SortOrder: 'desc'
      }
    }
    expect(reducer({}, action)).toEqual({ getPropertiesParam: action.payload })
  })
  it('should handle GET_PROPERTIES_LIST', () => {
    const startAction = {
      type: actions.GET_PROPERTIES_LIST,
      payload: undefined
    }
    expect(reducer({}, startAction)).toBeDefined()
  });

  it('should handle GET_PROPERTIES_LIST_SUCCESS', () => {
    const successAction = {
      type: actions.GET_PROPERTIES_LIST_SUCCESS,
      payload: {
        data: {}
      },
    };
    expect(reducer({}, successAction)).toBeDefined()
  });

  it('should handle GET_PROPERTIES_LIST_FAILURE', () => {
    const failAction = {
      type: actions.GET_PROPERTIES_LIST_FAILURE,
      payload: { "message": "some error" },
    };
    expect(reducer({}, failAction)).toBeDefined()
  });
  it("should handle UPDATE_EVENT_PROPERTY_PAGE_NUMBER", () => {
    const action = {
      type: actions.UPDATE_EVENT_PROPERTY_PAGE_NUMBER,
      payload: {
        pageNumber: 2
      }
    }
    expect(reducer({}, action)).toEqual({ eventPropertyPageNumber: 2 })
  })

})