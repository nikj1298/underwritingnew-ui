import appReducer from '../../reducers/appReducer'
import * as appActions from '../../actions/appAction'
import expect from 'expect'

describe("App Reducer", () => {
  it("should return the initial state", () => {
    expect(appReducer(undefined, {})).toEqual({
      loaderStatus: false
    })
  })
  it('should handle SHOW_LOADER', () => {
    const startAction = {
      type: appActions.SHOW_LOADER,
      payload: true
    }
    expect(appReducer({}, startAction)).toEqual({ loaderStatus: true })
  })
})