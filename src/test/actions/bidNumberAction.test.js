import * as actions from '../../actions/EventsDetails/bidNumberAction';
import { apiMiddleware, ApiError } from 'redux-api-middleware';
import configureMockStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';
import reducers from '../../reducers/Events/eventsListReducer';

describe("getEventList Loaders", () => {
    afterEach(() => {
        fetchMock.reset()
        fetchMock.restore()
    })
    let data = {}

    it('getBidsList withfilters', () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const searchEventsParam = {
            FullSearch: '',
            pageSize : 2,
            pageNumber : 1 ,
            Offset : 0
        }
        const expectedActions = [
            { type: 'FETCH_All_BIDS', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.getBidsList(searchEventsParam));
        expect(store.getActions()).toEqual(expectedActions);
    })

    it("getBidsListSuccess", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'FETCH_All_BIDS_SUCCESS', 'payload': data }
        ]
        store.dispatch(actions.getBidsListSuccess(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    let error = {
        message: "something wrong"
    };
    it("getBidsListFailure", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'FETCH_All_BIDS_FAILURE', 'payload': error }
        ]
        store.dispatch(actions.getBidsListFailure(error));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it('postSaveBid', () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'POST_SAVE_BID', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.postSaveBid({}));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("postSaveBidSuccess", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'POST_SAVE_BID_SUCCESS', 'payload': data }
        ]
        store.dispatch(actions.postSaveBidSuccess(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("postSaveBidFailure", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'POST_SAVE_BID_FAILURE', 'payload': error }
        ]
        store.dispatch(actions.postSaveBidFailure(error));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it('putUpdateBid', () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'PUT_UPDATE_BID', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.putUpdateBid({}));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("putUpdateBidSuccess", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'PUT_UPDATE_BID_SUCCESS', 'payload': data }
        ]
        store.dispatch(actions.putUpdateBidSuccess(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("putUpdateBidFailure", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'PUT_UPDATE_BID_FAILURE', 'payload': error }
        ]
        store.dispatch(actions.putUpdateBidFailure(error));
        expect(store.getActions()).toEqual(expectedActions);
    })

    it('deleteBid', () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'DELETE_BID', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.deleteBid({}));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("deleteBidSuccess", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'DELETE_BID_SUCCESS', 'payload': data }
        ]
        store.dispatch(actions.deleteBidSuccess(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("deleteBidFailure", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'DELETE_BID_FAILURE', 'payload': error }
        ]
        store.dispatch(actions.deleteBidFailure(error));
        expect(store.getActions()).toEqual(expectedActions);
    })

})