import * as actions from '../../actions/Events/eventsListAction';
import { apiMiddleware, ApiError } from 'redux-api-middleware';
import configureMockStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';
import reducers from '../../reducers/Events/eventsListReducer';

describe("getEventList Loaders", () => {
    afterEach(() => {
        fetchMock.reset()
        fetchMock.restore()
    })
    let data = {}

    it('getEventList', () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const searchEventsParam = {
            SortField: 'SaleDate',
            SortOrder: 'desc',
            FullSearch: 'A',
            Limit: 10,
            offset: 0,
            Filter: {
                StateId: 5,
                Type: "New",
                IsLockedStatus: false,
                TaskType: "EnterSaleInfo",
                AssignedTo: "01"
            }
        }
        const expectedActions = [
            { type: 'FETCH_ALL_EVENTS_LIST', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.getEventList(searchEventsParam));
        expect(store.getActions()).toEqual(expectedActions);
    })

    it('getEventList without filters', () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const searchEventsParam = {
            SortField: 'SaleDate',
            SortOrder: 'desc',
            FullSearch: 'A',
            Limit: 10,
            offset: 0,
            Filter: {
                StateId: null,
                Type: null,
                IsLockedStatus: true,
                TaskType: null,
                AssignedTo: ""
            }
        }
        const expectedActions = [
            { type: 'FETCH_ALL_EVENTS_LIST', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.getEventList(searchEventsParam));
        expect(store.getActions()).toEqual(expectedActions);
    })

    it("getEventListSuccess", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'FETCH_ALL_EVENTS_LIST_SUCCESS', 'payload': data }
        ]
        store.dispatch(actions.getEventListSuccess(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    let error = {
        message: "something wrong"
    };
    it("getEventListFailure", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'FETCH_ALL_EVENTS_LIST_FAILURE', 'payload': error }
        ]
        store.dispatch(actions.getEventListFailure(error));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it('getAllStatesList', () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'FETCH_ALL_STATES_LIST', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.getAllStatesList());
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("getAllStatesListSuccess", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'FETCH_ALL_STATES_LIST_SUCCESS', 'payload': data }
        ]
        store.dispatch(actions.getAllStatesListSuccess(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("getAllStatesListFailure", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'FETCH_ALL_STATES_LIST_FAILURE', 'payload': error }
        ]
        store.dispatch(actions.getAllStatesListFailure(error));
        expect(store.getActions()).toEqual(expectedActions);
    })

    it('getEventTypes', () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        let urlData = 'Dictionaries/EventTypes?offset=0';
        const expectedActions = [
            { type: 'FETCH_EVENTS_TYPES', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.getEventTypes());
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("getEventTypesSuccess", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'FETCH_EVENTS_TYPES_SUCCESS', 'payload': data }
        ]
        store.dispatch(actions.getEventTypesSuccess(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("getEventTypesFailure", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'FETCH_EVENTS_TYPES_FAILURE', 'payload': error }
        ]
        store.dispatch(actions.getEventTypesFailure(error));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("updateEventsPageNumber", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'UPDATE_EVENT_PAGE_NUMBER', 'payload': 2 }
        ]
        store.dispatch(actions.updateEventsPageNumber(2));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("updateSearchEventParam", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const searchEventsParam = {
            params: {
                SortField: 'SaleDate',
                SortOrder: 'desc',
                FullSearch: 'A',
                Limit: 10,
                offset: 0,
                Filter: {
                    StateId: 5,
                    Type: "New",
                    IsLockedStatus: false,
                    TaskType: "EnterSaleInfo",
                    AssignedTo: ""
                }
            }

        }
        const expectedActions = [
            { type: 'UPDATE_SEARCH_EVENT_PARAM', 'payload': searchEventsParam.params }
        ]
        store.dispatch(actions.updateSearchEventParam(searchEventsParam));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("updateReviewReportData ", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'UPDATE_REVIEW_REPORT_DATA', 'payload': data }
        ]
        store.dispatch(actions.updateReviewReportData(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("validateReviewReportData ", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'VALIDATE_REVIEW_REPORT_DATA', 'payload': data }
        ]
        store.dispatch(actions.validateReviewReportData(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("generateReviewReport ", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'GENERATE_REVIEW_REPORT', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.generateReviewReport(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("generateReviewReportSuccess ", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'GENERATE_REVIEW_REPORT_SUCCESS', 'payload': data }
        ]
        store.dispatch(actions.generateReviewReportSuccess(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("generateReviewReportFailure", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'GENERATE_REVIEW_REPORT_FAILURE', 'payload': error }
        ]
        store.dispatch(actions.generateReviewReportFailure(error));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("getReviewReportUrl ", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'GET_REVIEW_REPORT_URL', 'payload': Promise.resolve() }
        ]
        store.dispatch(actions.getReviewReportUrl("123"));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("getReviewReportUrlSuccess ", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'GET_REVIEW_REPORT_URL_SUCCESS', 'payload': data }
        ]
        store.dispatch(actions.getReviewReportUrlSuccess(data));
        expect(store.getActions()).toEqual(expectedActions);
    })
    it("getReviewReportUrlFailure", () => {
        const createStore = configureMockStore([apiMiddleware])
        const store = createStore(reducers.initialState)
        const expectedActions = [
            { type: 'GET_REVIEW_REPORT_URL_FAILURE', 'payload': error }
        ]
        store.dispatch(actions.getReviewReportUrlFailure(error));
        expect(store.getActions()).toEqual(expectedActions);
    })
})