import * as actions from '../../actions/Review/reviewListAction';
import { apiMiddleware, ApiError } from 'redux-api-middleware';
import configureMockStore from 'redux-mock-store';
import fetchMock from 'fetch-mock';
import reducers from '../../reducers/Review/reviewListReducer';

describe("Review List Action ", () => {
  afterEach(() => {
    fetchMock.reset()
    fetchMock.restore()
  })
  let data = {}
  let error = {
    message: "something wrong"
  };
  it("getAllEventsList", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const searchEventsParam = {
      SortField: 'SaleDate',
      SortOrder: 'desc',
      FullSearch: '',
      Limit: 10,
      offset: 0,
      Filter: {
        StateId: null,
        Type: null,
        IsLockedStatus: null,
        TaskType: null,
        AssignedTo: ""
      }
    }
    const expectedActions = [
      { type: 'GET_ALL_EVENTS', 'payload': Promise.resolve() }
    ]
    store.dispatch(actions.getAllEventsList(searchEventsParam));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("getAllEventsListSuccess", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_ALL_EVENTS_SUCCESS', 'payload': data }
    ]
    store.dispatch(actions.getAllEventsListSuccess(data));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("getAllEventsListFailure", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_ALL_EVENTS_FAILURE', 'payload': error }
    ]
    store.dispatch(actions.getAllEventsListFailure(error));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("updateGetAllEventsParam", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const searchEventsParam = {
      params: {
        SortField: 'SaleDate',
        SortOrder: 'desc',
        FullSearch: '',
        Limit: 10,
        offset: 0,
        Filter: {
          StateId: null,
          Type: null,
          IsLockedStatus: null,
          TaskType: null,
          AssignedTo: ""
        }
      }

    }
    const expectedActions = [
      { type: 'UPDATE_ALL_EVENT_PARAM', 'payload': searchEventsParam }
    ]
    store.dispatch(actions.updateGetAllEventsParam(searchEventsParam));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("updateGetPropertyListParam", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const searchEventsParam = {
      offset: 0,
      limit: 10,
      Filter: {
        MoveForward: true,
        ReviewDecision: 0,
        EventId: null,
        AssignmentByUser: true
      },
      FullSearch: "",
      SortField: 'AccountName',
      SortOrder: 'desc',
    }
    const expectedActions = [
      { type: 'UPDATE_GET_PROPERTIES_PARAM', 'payload': searchEventsParam }
    ]
    store.dispatch(actions.updateGetPropertyListParam(searchEventsParam));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("fetchEventProperties", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const searchEventsParam = {
      offset: 0,
      limit: 10,
      Filter: {
        MoveForward: true,
        ReviewDecision: 0,
        EventId: null,
        AssignmentByUser: true
      },
      FullSearch: "",
      SortField: 'AccountName',
      SortOrder: 'desc',
    }
    const expectedActions = [
      { type: 'GET_PROPERTIES_LIST', 'payload': Promise.resolve() }
    ]
    store.dispatch(actions.fetchEventProperties(searchEventsParam));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("fetchEventPropertiesSuccess", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_PROPERTIES_LIST_SUCCESS', 'payload': data }
    ]
    store.dispatch(actions.fetchEventPropertiesSuccess(data));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("fetchEventPropertiesFailure", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_PROPERTIES_LIST_FAILURE', 'payload': error }
    ]
    store.dispatch(actions.fetchEventPropertiesFailure(error));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("updateEventPropertyPageNumber", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'UPDATE_EVENT_PROPERTY_PAGE_NUMBER', 'payload': 2 }
    ]
    store.dispatch(actions.updateEventPropertyPageNumber(2));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("fetchAllLevels ", () => {
    const eventId = "1234"
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'FETCH_ALL_LEVELS', 'payload': Promise.resolve() }
    ]
    store.dispatch(actions.fetchAllLevels(eventId));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("fetchAllLevelsSuccess ", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'FETCH_ALL_LEVELS_SUCCESS', 'payload': data }
    ]
    store.dispatch(actions.fetchAllLevelsSuccess(data));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("fetchAllLevelsFailure", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'FETCH_ALL_LEVELS_FAILURE', 'payload': error }
    ]
    store.dispatch(actions.fetchAllLevelsFailure(error));
    expect(store.getActions()).toEqual(expectedActions);
  })

  it("getAllGeneralLandUseCode ", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_ALL_GENERAL_LAND_USE_CODE', 'payload': Promise.resolve() }
    ]
    store.dispatch(actions.getAllGeneralLandUseCode());
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("getAllGeneralLandUseCodeSuccess", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_ALL_GENERAL_LAND_USE_CODE_SUCCESS', 'payload': data }
    ]
    store.dispatch(actions.getAllGeneralLandUseCodeSuccess(data));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("getAllGeneralLandUseCodeFailure", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_ALL_GENERAL_LAND_USE_CODE_FAILURE', 'payload': error }
    ]
    store.dispatch(actions.getAllGeneralLandUseCodeFailure(error));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("getAllInternalLandUseCode ", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_ALL_INTERNAL_LAND_USE_CODE', 'payload': Promise.resolve() }
    ]
    store.dispatch(actions.getAllInternalLandUseCode());
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("getAllInternalLandUseCodeSuccess", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_ALL_INTERNAL_LAND_USE_CODE_SUCCESS', 'payload': data }
    ]
    store.dispatch(actions.getAllInternalLandUseCodeSuccess(data));
    expect(store.getActions()).toEqual(expectedActions);
  })
  it("getAllInternalLandUseCodeFailure", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(reducers.initialState)
    const expectedActions = [
      { type: 'GET_ALL_INTERNAL_LAND_USE_CODE_FAILURE', 'payload': error }
    ]
    store.dispatch(actions.getAllInternalLandUseCodeFailure(error));
    expect(store.getActions()).toEqual(expectedActions);
  })
})