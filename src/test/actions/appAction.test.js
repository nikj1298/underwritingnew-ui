import { apiMiddleware } from 'redux-api-middleware';
import configureMockStore from 'redux-mock-store';
import * as appAction from "../../actions/appAction"
import appReducer from "../../reducers/appReducer"
describe("appAction", () => {

  it("showLoader", () => {
    const createStore = configureMockStore([apiMiddleware])
    const store = createStore(appReducer.initialState)
    const expectedActions = [
      { type: 'SHOW_LOADER', 'payload': true }
    ]
    store.dispatch(appAction.showLoader(true));
    expect(store.getActions()).toEqual(expectedActions);
  })
})