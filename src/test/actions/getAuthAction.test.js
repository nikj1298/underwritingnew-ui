import * as getAuthAction from "../../actions/getAuthAction";

describe("getAccessToken action", () => {
  it("getAccessToken", () => {
    const detail = {
      "token": "getAccessToken"
    }
    const expectedResult = {
      data: Promise.resolve()
    }
    getAuthAction.getAccessToken(detail);
    expect(getAuthAction.getAccessToken(detail)).toEqual(expectedResult.data);
  })
})