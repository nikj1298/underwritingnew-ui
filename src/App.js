import React, { Fragment, Component } from 'react';
import './App.css';
import AppRoute from './routes';
import { logout, getRefreshToken } from "./common/requireAuth";

export default class App extends Component {

  getParameterByName = (name) => {
    const url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  componentDidMount() {
    if (!localStorage.getItem('authApi') || !localStorage.getItem('crmApi') ||
      !localStorage.getItem('landingApi') || !localStorage.getItem('adminApi')) {
      setTimeout(() => {
        // window.location.reload()
      }, 1000)
    }
    const parametersRefreshToken = this.getParameterByName('token');

    if (parametersRefreshToken) {
      sessionStorage.setItem('parametersRefreshToken', parametersRefreshToken);
      getRefreshToken().then(() => {
        window.location.href = window.location.href.split('?')[0]
      })
    } else if (!sessionStorage.currentUser) {
      logout()
    }
  }

  render() {
    return (
      <Fragment>
        <div>{this.props.children}</div>
        <AppRoute />
      </Fragment>
    );
  }
}
