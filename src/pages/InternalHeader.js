import React, { Component, Fragment } from 'react'
import './layout.css'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import ImgLogo from '../assets/logo.svg'
import Back from '../assets/back.svg'
import { getInitialsOfName } from './../components/common/utils'
import Button from '@material-ui/core/Button'
import NotifyIcon from './../assets/notify_icon.svg'
import Badge from '@material-ui/core/Badge'
import { DropDownMenu } from './../components/common/dropDownMenu'

class InternalHeader extends Component {
	navigate = (event) => {
		event.preventDefault()
		this.props.navigateToList()
	}
	render() {
		return (
			<Fragment>
				<div className="internalHeader headerAlignCenter">
					<div className="headerBar">
						<AppBar position="fixed">
							<Toolbar variant="dense">
								<span className="notifyIcon">
									<IconButton
										edge="start"
										color="inherit"
										aria-label="menu"
										onClick={(event) => this.navigate(event)}>
										<img src={Back} alt="" />
									</IconButton>
								</span>
								<span>
									<DropDownMenu {...this.props} menuName={this.props.menuName} />
								</span>
								<Typography variant="h6" color="inherit" className="logoBox">
									<img src={ImgLogo} alt="Alinea" />
								</Typography>
								<span className="notifyIcon">
									<IconButton aria-label="show 12 new notifications" color="inherit">
										<Badge badgeContent={12} color="secondary" className="notificationHeaderIcon">
											<img src={NotifyIcon} alt="notifications" />
										</Badge>
									</IconButton>
								</span>

								<span className="userBadge">
									<Button
										id="btn"
										aria-controls="simple-menu"
										aria-haspopup="true"
										onClick={this.handleClick}>
										{JSON.parse(sessionStorage.getItem('currentUser')) ? (
											getInitialsOfName(JSON.parse(sessionStorage.getItem('currentUser'))['name'])
										) : (
												''
											)}
										{/* {getInitialsOfName(JSON.parse(sessionStorage.getItem('currentUser'))['name'])} */}
									</Button>
								</span>
							</Toolbar>
						</AppBar>
					</div>
				</div>
			</Fragment>
		)
	}
}

export default InternalHeader
