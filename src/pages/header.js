import React, { Component } from 'react';
import './layout.css';
import NavBar from './navBar';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
// import MenuIcon from '@material-ui/icons/Menu';
import Badge from '@material-ui/core/Badge';
import ImgLogo from './../assets/logo.svg';
import NotifyIcon from './../assets/notify_icon.svg';
import Icon from '@material-ui/core/Icon';
import {DropDownMenu} from './../components/common/dropDownMenu'


class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClick(event) {
    this.setState({ anchorEl: event.currentTarget });
  }

  handleClose() {
    this.setState({ anchorEl: null });
  }

  render() {
    return (
      <div className='headerBar'>
        <AppBar position="fixed">
          <Toolbar variant="dense">
            {!window.location.href.includes("events-details") ?
              <NavBar />
              : <div className="headArrowContainer">
                <IconButton edge="start" aria-label="menu" onClick={() => {
                  this.props.history.push({
                    pathname: '/underwritingnew/events'
                  })
                }}>
                  <Icon>arrow_back</Icon>
                </IconButton>
                <div className="headerDropdownTop">
                  <DropDownMenu menuName="Event" {...this.props}  />
                </div>
              </div>
            }

            <Typography variant="h6" color="inherit" className='logoBox'>
              <img src={ImgLogo} alt="Alinea" />
            </Typography>
            <span className='notifyIcon'>
              <IconButton aria-label="show 12 new notifications" color="inherit">
                <Badge badgeContent={12} color="secondary" className="notificationHeaderIcon">
                  <img src={NotifyIcon} alt="notifications" />
                </Badge>
              </IconButton>
            </span>

            <span className='userBadge'>
              <Button id="btn" aria-controls="simple-menu" aria-haspopup="true" onClick={this.handleClick}>
                AD
              </Button>
            </span>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default Header;