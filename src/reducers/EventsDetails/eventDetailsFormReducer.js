import {
    FETCH_All_RULES_FIELDS,
    FETCH_All_RULES_FIELDS_SUCCESS,
    FETCH_All_RULES_FIELDS_FAILURE,
    FETCH_RULES_APPROVE_REJECT,
    FETCH_RULES_APPROVE_REJECT_SUCCESS,
    FETCH_RULES_APPROVE_REJECT_FAILURE,
    
} from "./../../actions/EventsDetails/eventDetailsFormActions";

const INITIAL_STATE = {
    rulesList: { rulesList: null , error: null, loading: false },
    rulesTypesList: null,
};

const dict = {
    [FETCH_All_RULES_FIELDS]: (state = INITIAL_STATE, action = {}) => ({ 
        ...state,
        rulesList: {
            rulesList: null,
            error: null,
            loading: false
        },
    }),
    [FETCH_All_RULES_FIELDS_SUCCESS]: (state = INITIAL_STATE, action = {}) => ({
        ...state,
        rulesList: {
            rulesList: action.payload.data,
            error: null,
            loading: false
        },
    }),
    [FETCH_All_RULES_FIELDS_FAILURE]: (state = INITIAL_STATE, action = {}) => ({
        ...state,
        rulesList: {
            rulesList: null,
            error: null,
            loading: false
        },
    }),
    [FETCH_RULES_APPROVE_REJECT]: (state = INITIAL_STATE, action = {}) => ({ 
        ...state,
        rulesTypesList : null
    }),
    [FETCH_RULES_APPROVE_REJECT_SUCCESS]: (state = INITIAL_STATE, action = {}) => ({
        ...state,
        rulesTypesList : action.payload.data,
    }),
    [FETCH_RULES_APPROVE_REJECT_FAILURE]: (state = INITIAL_STATE, action = {}) => ({
        ...state,
        rulesTypesList: null
    })
};

export default function (state = INITIAL_STATE, action = {}) {
    const delegate = dict[action.type];
    if (!!delegate) {
        return delegate(state, action);
    }
    return state;
}
