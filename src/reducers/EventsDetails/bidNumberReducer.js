import {
    FETCH_All_BIDS,
    FETCH_All_BIDS_SUCCESS,
    FETCH_All_BIDS_FAILURE,
} from "./../../actions/EventsDetails/bidNumberAction";

const INITIAL_STATE = {
    bidList: { bidList: null , error: null, loading: false },
};

const dict = {
    [FETCH_All_BIDS]: (state = INITIAL_STATE, action = {}) => ({ 
        ...state,
        bidList: {
            bidList: null,
            error: null,
            loading: false
        },
    }),
    [FETCH_All_BIDS_SUCCESS]: (state = INITIAL_STATE, action = {}) => ({
        ...state,
        bidList: {
            bidList: action.payload.data,
            error: null,
            loading: false
        },
    }),
    [FETCH_All_BIDS_FAILURE]: (state = INITIAL_STATE, action = {}) => ({
        ...state,
        bidList: {
            bidList: null,
            error: null,
            loading: false
        },
    })
};

export default function (state = INITIAL_STATE, action = {}) {
    const delegate = dict[action.type];
    if (!!delegate) {
        return delegate(state, action);
    }
    return state;
}
