import {
    FETCH_All_TASKS_LIST,
    FETCH_All_TASKS_LIST_SUCCESS,
    FETCH_All_TASKS_LIST_FAILURE,
} from "./../../actions/EventsDetails/taskActions";

const INITIAL_STATE = {
    taskList: { taskList: null , error: null, loading: false },
};

const dict = {
    [FETCH_All_TASKS_LIST]: (state = INITIAL_STATE, action = {}) => ({ 
        ...state,
        taskList: {
            taskList: null,
            error: null,
            loading: false
        },
    }),
    [FETCH_All_TASKS_LIST_SUCCESS]: (state = INITIAL_STATE, action = {}) => ({
        ...state,
        taskList: {
            taskList: action.payload.data,
            error: null,
            loading: false
        },
    }),
    [FETCH_All_TASKS_LIST_FAILURE]: (state = INITIAL_STATE, action = {}) => ({
        ...state,
        taskList: {
            taskList: null,
            error: null,
            loading: false
        },
    })
};

export default function (state = INITIAL_STATE, action = {}) {
    const delegate = dict[action.type];
    if (!!delegate) {
        return delegate(state, action);
    }
    return state;
}
