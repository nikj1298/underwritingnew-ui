import {
  SHOW_LOADER, UPDATED_URL
} from '../actions/appAction'

const INITIAL_STATE = {
  loaderStatus: false,
  lastVisitedUrl: undefined
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case SHOW_LOADER:
      return { ...state, loaderStatus: action.payload }
    case UPDATED_URL:
      return { ...state, lastVisitedUrl: action.payload }
    default:
      return { ...state }
  }
}