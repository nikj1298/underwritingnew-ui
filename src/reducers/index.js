import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import eventsListReducer from './Events/eventsListReducer';
import appReducer from "./appReducer"
import bidNumberReducer from './EventsDetails/bidNumberReducer';
import taskReducer from './EventsDetails/taskReducer'
import reviewListReducer from "./Review/reviewListReducer"
import eventDetailsFormReducer from './EventsDetails/eventDetailsFormReducer'

const rootReducer = combineReducers({
  routing: routerReducer,
  eventsListReducer,
  bidNumberReducer,
  taskReducer,
  appReducer,
  reviewListReducer,
  eventDetailsFormReducer
})

export default rootReducer