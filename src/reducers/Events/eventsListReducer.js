import {
    FETCH_ALL_STATES_LIST,
    FETCH_ALL_STATES_LIST_SUCCESS,
    FETCH_ALL_STATES_LIST_FAILURE,
    FETCH_EVENTS_TYPES,
    FETCH_EVENTS_TYPES_SUCCESS,
    FETCH_EVENTS_TYPES_FAILURE,
    FETCH_ALL_EVENTS_LIST,
    FETCH_ALL_EVENTS_LIST_SUCCESS,
    FETCH_ALL_EVENTS_LIST_FAILURE,
    UPDATE_EVENT_PAGE_NUMBER,
    UPDATE_SEARCH_EVENT_PARAM,
    UPDATE_REVIEW_REPORT_DATA,
    VALIDATE_REVIEW_REPORT_DATA,
    GENERATE_REVIEW_REPORT, GENERATE_REVIEW_REPORT_SUCCESS, GENERATE_REVIEW_REPORT_FAILURE,
    GET_REVIEW_REPORT_URL, GET_REVIEW_REPORT_URL_SUCCESS, GET_REVIEW_REPORT_URL_FAILURE
} from "../../actions/Events/eventsListAction";
import { searchEventsParam, generateReviewReport, validateReviewReportData } from "../../common/const"

const INITIAL_STATE = {
    allStatesList: { allStatesList: undefined, error: null, loading: false },
    eventTypesList: { eventTypesList: undefined, error: null, loading: false },
    eventList: { eventList: undefined, error: null, loading: false },
    eventsCount: 0,
    searchEventParam: searchEventsParam,
    eventPageNumber: 1,
    reviewReportData: generateReviewReport,
    validateReviewReport: validateReviewReportData,
    generateReviewReportData: undefined,
    reviewReportUrl: undefined
};

export default function (state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case FETCH_ALL_STATES_LIST:
            return {
                ...state, allStatesList: {
                    allStatesList: undefined,
                    error: null,
                    loading: false
                }
            }
        case FETCH_ALL_STATES_LIST_SUCCESS:
            return {
                ...state, allStatesList: {
                    allStatesList: action.payload.data.List,
                    error: null,
                    loading: false
                }
            }
        case FETCH_ALL_STATES_LIST_FAILURE:
            return {
                ...state, allStatesList: {
                    allStatesList: undefined,
                    error: null,
                    loading: false
                }
            }
        case FETCH_EVENTS_TYPES:
            return {
                ...state,
                eventTypesList: {
                    eventTypesList: undefined,
                    error: null,
                    loading: false
                }
            }
        case FETCH_EVENTS_TYPES_SUCCESS:
            return {
                ...state,
                eventTypesList: {
                    eventTypesList: action.payload.data.List,
                    error: null,
                    loading: false
                }
            }
        case FETCH_EVENTS_TYPES_FAILURE:
            return {
                ...state,
                eventTypesList: {
                    eventTypesList: undefined,
                    error: null,
                    loading: false
                }
            }
        case FETCH_ALL_EVENTS_LIST:
            return {
                ...state,
                eventList: {
                    eventList: state.eventList && state.eventList.eventList ? state.eventList.eventList : undefined,
                    eventsCount: 0,
                    error: null,
                    loading: false
                }
            }
        case FETCH_ALL_EVENTS_LIST_SUCCESS:
            return {
                ...state,
                eventList: {
                    eventList: action.payload.data.List,
                    eventsCount: action.payload.data.TotalCount,
                    error: null,
                    loading: false
                }
            }
        case FETCH_ALL_EVENTS_LIST_FAILURE:
            return {
                ...state,
                eventList: {
                    eventList: undefined,
                    eventsCount: 0,
                    error: null,
                    loading: false
                }
            }
        case UPDATE_EVENT_PAGE_NUMBER:
            return {
                ...state,
                eventPageNumber: action.payload.pageNumber
            }
        case UPDATE_SEARCH_EVENT_PARAM:
            return {
                ...state,
                searchEventParam: action.payload
            }
        case UPDATE_REVIEW_REPORT_DATA:
            return { ...state, reviewReportData: action.payload }
        case VALIDATE_REVIEW_REPORT_DATA:
            return { ...state, validateReviewReport: action.payload }
        case GENERATE_REVIEW_REPORT:
            return { ...state, generateReviewReportData: undefined }
        case GENERATE_REVIEW_REPORT_SUCCESS:
            return { ...state, generateReviewReportData: action.payload.data }
        case GENERATE_REVIEW_REPORT_FAILURE:
            return { ...state, generateReviewReportData: undefined }
        case GET_REVIEW_REPORT_URL:
            return { ...state, reviewReportUrl: undefined }
        case GET_REVIEW_REPORT_URL_SUCCESS:
            return { ...state, reviewReportUrl: action.payload.data }
        case GET_REVIEW_REPORT_URL_FAILURE:
            return { ...state, reviewReportUrl: undefined }
        default:
            return { ...state }
    }

}
