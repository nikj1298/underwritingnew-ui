import {
  GET_ALL_EVENTS, GET_ALL_EVENTS_SUCCESS,
  GET_ALL_EVENTS_FAILURE, UPDATE_ALL_EVENT_PARAM, UPDATE_GET_PROPERTIES_PARAM,
  GET_PROPERTIES_LIST, GET_PROPERTIES_LIST_SUCCESS, GET_PROPERTIES_LIST_FAILURE,
  UPDATE_EVENT_PROPERTY_PAGE_NUMBER, FETCH_ALL_LEVELS, FETCH_ALL_LEVELS_SUCCESS, FETCH_ALL_LEVELS_FAILURE,
  GET_ALL_GENERAL_LAND_USE_CODE, GET_ALL_GENERAL_LAND_USE_CODE_SUCCESS, GET_ALL_GENERAL_LAND_USE_CODE_FAILURE,
  GET_ALL_INTERNAL_LAND_USE_CODE, GET_ALL_INTERNAL_LAND_USE_CODE_SUCCESS, GET_ALL_INTERNAL_LAND_USE_CODE_FAILURE
} from "../../actions/Review/reviewListAction"
import { getEventsParam, filterPropertiesParam } from "../../common/const"

const INITIAL_STATE = {
  getAllEvents: undefined,
  allEventParam: getEventsParam,
  getPropertiesParam: filterPropertiesParam,
  getPropertiesList: undefined,
  eventPropertyPageNumber: 1,
  allLevelList: undefined,
  allGeneralLandUseCode: undefined,
  allInternalLandUseCode: undefined
}

export default function (state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case GET_ALL_EVENTS:
      return { ...state, getAllEvents: state.getAllEvents ? state.getAllEvents : { getAllEvents: undefined, error: null, loading: true } }
    case GET_ALL_EVENTS_SUCCESS:
      return {
        ...state, getAllEvents: {
          getAllEvents: action.payload.List, error: null, loading: false
        }
      }
    case GET_ALL_EVENTS_FAILURE:
      return { ...state, getAllEvents: { getAllEvents: undefined, error: action.payload, loading: false } }
    case UPDATE_ALL_EVENT_PARAM:
      return { ...state, allEventParam: action.payload }
    case UPDATE_GET_PROPERTIES_PARAM:
      return { ...state, getPropertiesParam: action.payload }
    case GET_PROPERTIES_LIST:
      return { ...state, getPropertiesList: state.getPropertiesList ? state.getPropertiesList : { getPropertiesList: undefined, eventPropertyCount: 0, error: null, loading: true } }
    case GET_PROPERTIES_LIST_SUCCESS:
      return { ...state, getPropertiesList: { getPropertiesList: action.payload.List, eventPropertyCount: action.payload.TotalCount, error: null, loading: false } }
    case GET_PROPERTIES_LIST_FAILURE:
      return { ...state, getPropertiesList: { getPropertiesList: undefined, eventPropertyCount: 0, error: action.payload, loading: false } }
    case UPDATE_EVENT_PROPERTY_PAGE_NUMBER:
      return { ...state, eventPropertyPageNumber: action.payload.pageNumber }
    case FETCH_ALL_LEVELS:
      return { ...state, allLevelList: undefined }
    case FETCH_ALL_LEVELS_SUCCESS:
      return { ...state, allLevelList: action.payload.List }
    case FETCH_ALL_LEVELS_FAILURE:
      return { ...state, allLevelList: undefined }
    case GET_ALL_GENERAL_LAND_USE_CODE:
      return { ...state, allGeneralLandUseCode: undefined }
    case GET_ALL_GENERAL_LAND_USE_CODE_SUCCESS:
      return { ...state, allGeneralLandUseCode: action.payload.List }
    case GET_ALL_GENERAL_LAND_USE_CODE_FAILURE:
      return { ...state, allGeneralLandUseCode: undefined }
    case GET_ALL_INTERNAL_LAND_USE_CODE:
      return { ...state, allInternalLandUseCode: undefined }
    case GET_ALL_INTERNAL_LAND_USE_CODE_SUCCESS:
      return { ...state, allInternalLandUseCode: action.payload.List }
    case GET_ALL_INTERNAL_LAND_USE_CODE_FAILURE:
      return { ...state, allInternalLandUseCode: undefined }
    default:
      return { ...state }
  }
}