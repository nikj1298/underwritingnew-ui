// import React, { Fragment } from "react"
// import Icon from '@material-ui/core/Icon';
// import Button from '@material-ui/core/Button'
// import Menu from '@material-ui/core/Menu';
// import MenuItem from '@material-ui/core/MenuItem';


// export const ProgressViewType = (props) => {
//     const [anchorEl, setAnchorEl] = React.useState(null);
//     function handleClick(event) {
//         setAnchorEl(event.currentTarget);
//     }
//     function handleClose() {
//         setAnchorEl(null);
//     }
//     /**
//      * handles change of progress type
//      * @param {*} value 
//      */
//     const handleChangeProgressType = (value) => {
//         const searchParam = Object.assign({}, props.searchEventParam)
//         searchParam.Offset = 0
//         searchParam.FullSearch = ""
//         searchParam.Filter.IsLockedStatus = value
//         props.updateProps(searchParam, 1)
//         handleClose();
//     }

//     return <span className="paginationStyle tHeaadDrop">
//         <span className="pageCount">
//             <Fragment>
//                 <Button id='expandMoreIcon' aria-controls="table-pagerlist" aria-haspopup="true" onClick={handleClick}>
//                     <Icon>expand_more</Icon>
//                 </Button>
//                 <Menu
//                     id="pageChanger"
//                     anchorEl={anchorEl}
//                     keepMounted
//                     className="minWidthDrop"
//                     open={Boolean(anchorEl)}
//                     onClose={handleClose}
//                     getContentAnchorEl={null}
//                     anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
//                     transformOrigin={{ vertical: "top", horizontal: "right" }}
//                 >
//                     <MenuItem onClick={() => { handleChangeProgressType(null) }}>View all</MenuItem>
//                     <MenuItem onClick={() => { handleChangeProgressType(true) }}>Lock</MenuItem>
//                     <MenuItem onClick={() => { handleChangeProgressType(false) }}>Unlock</MenuItem>
//                 </Menu>
//             </Fragment>
//         </span>
//     </span>
// }