import React, { Fragment, Component } from "react"
import Header from '../../pages/header'
import { EventsTopBarComponent } from './eventTopBar'
import { Scrollbars } from 'react-custom-scrollbars'
import { EventTableComponent } from "./eventTableComponent"
import LoaderComponent from '../../common/loaderComponent'
import { searchEventsParam, eventsPageSize } from '../../common/const'
import { ListPagination } from "../common/listPagination"

export default class EventListComponent extends Component {
	componentDidMount = () => {
		searchEventsParam.offset = 0
		searchEventsParam.fullSearch = ''
		this.props.getAllStatesList();
		this.props.getEventsTypesList();
		this.props.updateSearchEventParam(searchEventsParam)
		this.props.updateEventsPageNumber(1)
		this.props.getEventList(searchEventsParam)
	}

	/**
	 * common function to be called while handling page movement in pagination
	 */

	updateAllProps = (searchParam, pageNumber) => {
		this.props.updateEventsPageNumber(pageNumber)
		this.props.updateSearchEventParam(searchParam)
		this.props.getEventList(searchParam)
	}

	render() {
		return (
			<Fragment>
				<Header />
				<div id="loanlist">
					{this.props.eventList ? (
						<Fragment>
							<div className="container-fluid">
								<EventsTopBarComponent searchParam={this.props.searchEventParam} listTotalCount={this.props.eventsCount}
									updateProps={this.updateAllProps} {...this.props} />
								<Scrollbars
									autoHeight
									autoHeightMin={'100%'}
									autoHeightMax={'100%'}
									renderTrackHorizontal={(props) => <div {...props} className="track-horizontal" />}
									thumbSize={300}>
									<EventTableComponent searchParam={this.props.searchEventParam}
										updateProps={this.updateAllProps} {...this.props} />
								</Scrollbars>
								<div className="bottomPagination">
									<ListPagination listPageNumber={this.props.eventPageNumber} listTotalCount={this.props.eventsCount}
										searchParam={this.props.searchEventParam} listPageSize={eventsPageSize} updateProps={this.updateAllProps} />
								</div>
							</div>{' '}
						</Fragment>
					) : (
							<LoaderComponent />
						)}
				</div>
			</Fragment>
		)
	}
}