import React, { Component, Fragment } from 'react'
import '../common-components.css'
import 'date-fns';
import { Button, Dialog, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import ReviewReportIcon from '../../assets/review-report-icon.svg';
import { styles, DialogActions, DialogTitle, DialogContent } from "../common/dialogComponent"
import { GenerateReportDialog } from "./generateReportDialog"
import { checkReviewReportValidity } from "../common/utils"
import LoaderComponent from "../common/loaderComponent"
import { ArrowTooltip } from '../common/tooltipComponents'

class ReviewReportDialog extends Component {
	constructor(props) {
		super(props);
	}

	handleClickOpen = () => {
		this.props.setDialogName("Review")
		const reviewData = { ...this.props.reviewReportData }
		reviewData.IsPerUserReport = false
		this.props.updateReviewReportData(reviewData)
		this.props.setOpen(true)
	}

	/**
	 * action to generate report
	 */
	handleGenerateReport = () => {
		const { validateReviewReport, reviewReportData } = this.props
		const checkFieldValidity = checkReviewReportValidity(reviewReportData, validateReviewReport)
		this.props.validateReviewReportData(checkFieldValidity)
		if (Object.values(checkFieldValidity).indexOf(true) > -1) return
		else {
			this.props.generateDownloadReport(reviewReportData)
		}
	}

	/**
	 * renders dialog actions button
	 */
	renderDialogButtons = () => {
		return <DialogActions id="reportDialogAction">
			<Button id="reportGenButton" onClick={this.handleGenerateReport} variant="contained" color="primary" className="fab_btn">
				Generate
			</Button>
			<Button className="fab_btn" onClick={this.props.handleClose} >
				Cancel
			</Button>
		</DialogActions>
	}

	/**
	 * renders dialog
	 */
	renderDialog = () => {
		return <Dialog id="addReminderDialog" fullWidth={true} maxWidth={"md"} onClose={this.props.handleClose} aria-labelledby="customized-dialog-title" open={this.props.open} className="titleDialog">
			<div className="widthaddReminder">
				<DialogTitle id="addReminderTitle">
					<div className="aaddReminderTitleBack">
						<Typography variant="h6">Generate Review Report</Typography>
					</div>
				</DialogTitle>
				<DialogContent className="customScroll">
					<GenerateReportDialog {...this.props} />
				</DialogContent>
				{this.renderDialogButtons()}
			</div>
		</Dialog>
	}

	render() {
		return (
			<Fragment>
				<ArrowTooltip title={"Review Report"} placement="bottom">
					<Button id="openButton" aria-controls="table-actionlist" aria-haspopup="true" onClick={this.handleClickOpen}>
						<img src={ReviewReportIcon} alt="report" />
					</Button>
				</ArrowTooltip>
				{this.props.dialogName === "Review" ? this.renderDialog() : null}
				{this.props.showLoaderStatus ? <LoaderComponent /> : null}
			</Fragment>
		);
	}
}


export default withStyles(styles)(ReviewReportDialog);