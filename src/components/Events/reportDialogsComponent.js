import React, { Fragment } from "react"
import ActivityReportDialog from './eventActivityReportDialog'
import ReviewReportDialog from './eventReviewReportDialog'
import { formatDate } from "../common/utils"
import _ from "lodash"
import { generateReviewReport, validateReviewReportData } from "../../common/const"

export const ReportDialogComponent = (props) => {
  const { reviewReportData, validateReviewReport } = props
  const [open, setOpen] = React.useState(false)
  const [tabValue, setValue] = React.useState(0)
  const [dialogName, setDialogName] = React.useState("")

	/**
	 * handles close of dialog
	 */
  const handleClose = () => {
    setOpen(false)
    setValue(0)
    let validateFields = { ...validateReviewReportData }
    validateFields = _.mapValues(validateFields, () => false);
    let reviewData = { ...generateReviewReport }
    reviewData = _.mapValues(reviewData, () => null);
    props.validateReviewReportData(validateFields)
    props.updateReviewReportData(reviewData)
  }

  /**
   * handles change of every field
   */
  const handleChange = ({ target }) => {
    const { name, value } = target
    const finalReviewReportData = { ...reviewReportData, [name]: value }
    props.updateReviewReportData(finalReviewReportData)
    const validateData = { ...validateReviewReport, [name]: false }
    props.validateReviewReportData(validateData)
  }

  /**
   * handles change of every date type field in pop-up
   * @param {*} value 
   * @param {*} name 
   */
  const handleDateChange = (value, name) => {
    const target = { name, value: formatDate(value) }
    handleChange({ target })
  }

  /**
   * generate and download report
   */
  const generateDownloadReport = (finalReviewData) => {
    props.showLoader(true)
    props.generateReviewReport(finalReviewData).then((response) => {
      props.getReviewReportUrl(response).then((data) => {
        if (data && data.Url) {
          props.downloadGeneratedReport(data)
          props.showLoader(false)
          handleClose()
        }
      })
    }).catch((error) => {
      props.showLoader(false)
    })
  }

  return <Fragment>
    <ActivityReportDialog dialogName={dialogName} open={open} handleClose={handleClose} setDialogName={setDialogName}
      value={tabValue} setOpen={setOpen} setValue={setValue} generateDownloadReport={generateDownloadReport}
      userReport={true} handleChange={handleChange} handleDateChange={handleDateChange} {...props} />
    <ReviewReportDialog dialogName={dialogName} open={open} handleClose={handleClose} setDialogName={setDialogName}
      setOpen={setOpen} generateDownloadReport={generateDownloadReport} userReport={false}
      handleChange={handleChange} handleDateChange={handleDateChange}  {...props} />
  </Fragment>
}