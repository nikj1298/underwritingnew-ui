import React from "react"
import { DropDownMenu } from "../common/dropDownMenu"
import { EventsSearchBar } from "./eventSearchBar"
import { ListPagination } from "../common/listPagination"
import { eventsPageSize } from "../../common/const"
import Button from '@material-ui/core/Button';
import CalIcon from '../../assets/calendar-icon.svg';
// import NewEventDialog from './newEventDialog'
import FilterByDialogs from './filterByDialog'
import LoaderComponent from "../common/loaderComponent"
import { ReportDialogComponent } from "./reportDialogsComponent"
import { ArrowTooltip } from '../common/tooltipComponents'


export const EventsTopBarComponent = (props) => {
  const { eventPageNumber, eventsCount, searchEventParam, allStatesList, eventTypesList } = props

  return <div className="tableTopRow">
    <div>
      <DropDownMenu menuName="Event" {...props} />
    </div>
    <div className="paginationSearch">
      <EventsSearchBar {...props} />
      <span className="topBarListIcon">
        {allStatesList && allStatesList.allStatesList && eventTypesList && eventTypesList.eventTypesList ? <FilterByDialogs {...props} /> : <LoaderComponent />}
        {/* <NewEventDialog /> */}
        <ArrowTooltip title={"Calender"} placement="bottom">
          <Button aria-controls="table-actionlist" aria-haspopup="true">
            <img src={CalIcon} alt="Calender" />
          </Button>
        </ArrowTooltip>
        {allStatesList && allStatesList.allStatesList ? <ReportDialogComponent {...props} /> : <LoaderComponent />}
      </span>
      <ListPagination listPageNumber={eventPageNumber} listTotalCount={eventsCount}
        searchParam={searchEventParam} listPageSize={eventsPageSize} updateProps={props.updateProps} />
    </div>
  </div>
}