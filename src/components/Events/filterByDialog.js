import React, { Component } from 'react'
import '../common-components.css'
import {
  Button, Typography, Tabs, Tab, TextField, Icon,
  List, ListItem, ListItemText
} from '@material-ui/core';
import { Scrollbars } from 'react-custom-scrollbars';
import SearchIcon from '../../assets/icon.svg';
import { TabPanel } from "../common/tabPanelComponent"
import { a11Props } from "../common/utils"
import { getMaxRows } from '../../common/const'
import _ from "lodash"
import { isBoolean } from 'util';
import { FilterActionButtons, FilterPopupHeader } from "../common/filterComponent"
import { AdvanceFilterDialogPopup } from "../common/advanceFilterDialogPopup"

class FilterByDialogs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      searchFilter: {
        StateId: this.props.searchEventParam.Filter.StateId,
        IsLockedStatus: this.props.searchEventParam.Filter.IsLockedStatus,
        Type: this.props.searchEventParam.Filter.Type
      }
    };
  }

  handleChange = (event, newValue) => {
    this.setState({ value: newValue });
  };
  componentDidMount = () => {
    this.setState({ open: false });
  }

  handlesChangeState = (e, value, name) => {
    const filterState = { ...this.state.searchFilter }
    filterState[name] = value
    this.setState({
      searchFilter: filterState
    })
  }

  /**
   * handles filter selection
   */
  handleFilterSelection = (e, id, name) => {
    const searchEventParamNew = { ...this.props.searchEventParam }
    searchEventParamNew.Filter[name] = id
    this.props.updateSearchEventParam(searchEventParamNew)
  }
  /**
   * renders state tab list when state tab is active
   */
  renderStateTab = () => {
    const { allStatesList } = this.props
    return <TabPanel value={this.state.value} index={0}>
      <div>
        <span className="searchInput filterSearch">
          <span className="searchIcon">
            <img src={SearchIcon} alt="search" />
          </span>
          <TextField
            id="filled-dense-hidden-label"
            margin="dense"
            hiddenLabel
            variant="filled"
            inputRef={this.textInput}
            type="text"
            placeholder="SEARCH"
          />
          <span className="clearIcon">
            <Button>
              <Icon>close</Icon>
            </Button>
          </span>
        </span>
      </div>
      <Scrollbars
        style={{ height: 365 }}
        renderTrackHorizontal={props => <div {...props} className="track-horizontal" style={{ overflow: 'hidden' }} />}
        renderTrackVertical={props => <div {...props} className="track-vertical" />}
      >
        <div className="contentInner">
          <div className="filter-ul-list">
            <List name="StateId">
              {allStatesList.allStatesList.map((stateItem) => {
                return <ListItem key={stateItem.Id} button selected={this.state.searchFilter.StateId === stateItem.Id}
                  onClick={(e) => this.handlesChangeState(e, stateItem.Id, "StateId")}
                  className={this.state.searchFilter.StateId === stateItem.Id ? "activeList" : ""} >
                  <ListItemText value={stateItem.Id} primary={stateItem.Name} />
                </ListItem>
              })}
            </List>
          </div>
        </div>
      </Scrollbars>
    </TabPanel>
  }

  /**
   * renders event type tab list when event-type tab is active
   */
  renderEventTypeTab = () => {
    const { eventTypesList } = this.props
    return <TabPanel value={this.state.value} index={1}>
      <Scrollbars
        style={{ height: 400 }}
        renderTrackHorizontal={props => <div {...props} className="track-horizontal" style={{ overflow: 'hidden' }} />}
        renderTrackVertical={props => <div {...props} className="track-vertical" />}
      >
        <div className="f-listHeadingText">
          {"Event Type"}
        </div>
        <div className="contentInner">
          <div className="filter-ul-list">
            <List name="Type">
              {eventTypesList.eventTypesList.map((eventType) => {
                return <ListItem className={this.state.searchFilter.Type === eventType.Id ? "activeList" : ""}
                  key={eventType.Id} button
                  onClick={(e) => this.handlesChangeState(e, eventType.Id, "Type")}
                  selected={this.state.searchFilter.Type === eventType.Id} >
                  <ListItemText primary={eventType.Name} />
                </ListItem>
              })}
            </List>
          </div>
        </div>
      </Scrollbars>

    </TabPanel>
  }

  /**
   * renders progress tab list when progress tab is active
   */
  renderProgressTab = () => {
    return <TabPanel value={this.state.value} index={2}>
      <Scrollbars
        style={{ height: 400 }}
        renderTrackHorizontal={props => <div {...props} className="track-horizontal" style={{ overflow: 'hidden' }} />}
        renderTrackVertical={props => <div {...props} className="track-vertical" />}
      >
        <div className="f-listHeadingText">
          {"Progress"}
        </div>
        <div className="contentInner">
          <div className="filter-ul-list">
            <List name={"IsLockedStatus"}>
              <ListItem onClick={(e) => this.handlesChangeState(e, null, "IsLockedStatus")}
                selected={this.state.searchFilter.IsLockedStatus === null}
                button className={this.state.searchFilter.IsLockedStatus === null ? "activeList" : ""}>
                <ListItemText primary="View All" />
              </ListItem>
              <ListItem onClick={(e) => this.handlesChangeState(e, true, "IsLockedStatus")}
                selected={this.state.searchFilter.IsLockedStatus === true}
                button className={this.state.searchFilter.IsLockedStatus === true ? "activeList" : ""}>
                <ListItemText value={true} primary="Lock" />
              </ListItem>
              <ListItem onClick={(e) => this.handlesChangeState(e, false, "IsLockedStatus")}
                selected={this.state.searchFilter.IsLockedStatus === false}
                button className={this.state.searchFilter.IsLockedStatus === false ? "activeList" : ""}>
                <ListItemText value={true} primary="Unlock" />
              </ListItem>

            </List>
          </div>
        </div>
      </Scrollbars>
      {/* <div className="noSearchFound"></div> */}
    </TabPanel>
  }

  /**
   * re-assigns the filter state to last applied filter
   */
  resetFilterLastApplied = (e, popupState) => {
    popupState.close()
    const searchEventParamNew = { ...this.props.searchEventParam }
    const searchFilterState = { ...this.state.searchFilter }
    searchFilterState.StateId = searchEventParamNew.Filter.StateId
    searchFilterState.Type = searchEventParamNew.Filter.Type
    searchFilterState.IsLockedStatus = searchEventParamNew.Filter.IsLockedStatus
    this.setState({
      searchFilter: searchFilterState,
      value: 0
    })
  }

  /**
   * common things to be handled in case of apply and clear filter
   */
  applyOrClearFilter = (event, popupState, actionName) => {
    event.preventDefault()
    const searchEventParamNew = { ...this.props.searchEventParam }
    let searchFilterState = { ...this.state.searchFilter }
    if (actionName === "Clear") {
      searchFilterState = _.mapValues(searchFilterState, () => null)
      searchFilterState.IsLockedStatus = false
      this.setState({
        searchFilter: searchFilterState
      })
    }
    for (let key in searchFilterState) {
      searchEventParamNew.Filter[key] = searchFilterState[key]
    }
    searchEventParamNew.offset = 0
    searchEventParamNew.Limit = getMaxRows()
    this.props.updateEventsPageNumber(1)
    this.props.updateSearchEventParam(searchEventParamNew)
    this.props.getEventList(searchEventParamNew)
    popupState.close()
    this.setState({
      value: 0
    })
  }

  /**
   * render tabs name on filter pop-up
   */
  renderFilterTabs = () => {
    return <Scrollbars
      style={{ height: 305 }}
      renderTrackVertical={props => <div {...props} className="track-vertical track-v-r8" />}>
      <Typography component="div">
        <Tabs
          orientation="vertical"
          value={this.state.value}
          onChange={this.handleChange}
          aria-label="Vertical tabs view"
          className="verTabs"
        >
          <Tab
            className={this.state.searchFilter.StateId ? "checkedIcon" : null}
            label="State" {...a11Props(0)} />
          <Tab
            className={this.state.searchFilter.Type ? "checkedIcon" : null}
            label="Event Type" {...a11Props(1)} />
          <Tab
            className={isBoolean(this.state.searchFilter.IsLockedStatus) || (this.state.searchFilter.IsLockedStatus === null) ?
              "checkedIcon" : null}
            label="Progress" {...a11Props(2)} />

        </Tabs>
      </Typography>
    </Scrollbars>
  }

  /**
   * render the complete filter dialog
   */
  renderFilterDialog = (popupState) => {
    return <div className="filterDialogInner">
      <div className="verTabsRoot">
        <Typography component="div" className="verContentBox">
          {this.renderStateTab()}
          {this.renderEventTypeTab()}
          {this.renderProgressTab()}
        </Typography>
        <Typography component="div" className="verTabBox">
          <FilterPopupHeader popupState={popupState} resetFilterLastApplied={this.resetFilterLastApplied} />
          {this.renderFilterTabs()}
          <FilterActionButtons popupState={popupState} applyOrClearFilter={this.applyOrClearFilter} />
        </Typography>
      </div>
    </div>
  }

  render() {
    return (
      <AdvanceFilterDialogPopup renderDialog={this.renderFilterDialog} />
    );
  }
}

export default FilterByDialogs;
