import React from "react"
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import { Grid, FormControl, InputLabel, FormHelperText } from '@material-ui/core';
import { formatDate } from "../common/utils"
import { DropdownComponent } from "../common/dropdownComponent"


export const GenerateReportDialog = (props) => {
  const { reviewReportData, allStatesList, classes, validateReviewReport, handleChange, handleDateChange } = props

  /**
 * renders date type field in pop-up
 */
  const renderDateTypeField = (name) => {
    return <Grid item sm={4} >
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container justify="flex-start">
          <KeyboardDatePicker
            error={validateReviewReport[name]}
            name={name}
            disableFuture={name === "SaleDateFrom"}
            variant="inline"
            format="MM/dd/yyyy"
            id="datepicker"
            className="calendarField mandatoryField"
            label={name}
            value={reviewReportData[name] ? formatDate(reviewReportData[name]) : null}
            onChange={(value) => handleDateChange(value, name)}
            maxDateMessage="Invalid Date"
            invalidDateMessage="Invalid Date"
            minDateMessage="Invalid Date"
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
          {validateReviewReport[name] ?
            <FormHelperText id="component-error-text" error>
              {!reviewReportData[name] ? "Date is required." : "Invalid Sale Date Range"}
            </FormHelperText> : null}
        </Grid>
      </MuiPickersUtilsProvider>
    </Grid>
  }

	/**
	 * renders select state field in popup
	 */
  const renderSelectStateField = () => {
    return <Grid item sm={8}>
      <div className="mandatoryField">
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="record-helper">State</InputLabel>
          <DropdownComponent name={"StateId"} label={"State"} dropDownList={allStatesList.allStatesList}
            handleChange={handleChange} validateData={validateReviewReport.StateId}
            valueData={reviewReportData["StateId"]} />
          {validateReviewReport.StateId ?
            <FormHelperText id="component-error-text" error >State is required.</FormHelperText> : ""}
        </FormControl>
      </div>
    </Grid>
  }
  return <form>
    <Grid item sm={12} container className="GridStyle FormStyle">
      {renderSelectStateField()}
    </Grid>
    <Grid item sm={12} container className="GridStyle FormStyle">
      {renderDateTypeField("SaleDateFrom")}
      {renderDateTypeField("SaleDateTo")}
    </Grid>
  </form>
}