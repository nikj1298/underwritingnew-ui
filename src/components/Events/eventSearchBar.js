import React, { useState } from 'react'
import { SearchBarComponent } from "../common/searchBarComponent"

export const EventsSearchBar = (props) => {
	const { searchEventParam } = props
	const [showCross, setShowCross] = useState(false)
	const [inputValue, handleInputValue] = useState('')

	/**
	 * handles search filter on hit of Enter key
	 */
	const searchInputText = () => {
		searchEventParam.FullSearch = inputValue
		searchEventParam.offset = 0
		props.updateSearchEventParam(searchEventParam)
		props.updateEventsPageNumber(1)
		props.getEventList(searchEventParam)
	}
	/**
   * cleares all search query on click of cross icon on search bar
   */
	const clearGlobalSearch = () => {
		handleInputValue('')
		searchEventParam.FullSearch = ''
		searchEventParam.offset = 0
		props.getEventList(searchEventParam)
		props.updateEventsPageNumber(1)
		setShowCross(false)
	}

	return (
		<SearchBarComponent listTotalCount={props.listTotalCount} inputValue={inputValue} showCross={showCross} handleInputValue={handleInputValue}
			setShowCross={setShowCross} searchInputText={searchInputText} clearGlobalSearch={clearGlobalSearch} />
	)
}
