import React from "react"
import { Table, TableBody, TableCell, TableHead, TableRow, Paper, Link } from '@material-ui/core';
import { Scrollbars } from 'react-custom-scrollbars';
import UnlockIcon from '../../assets/unlock-icon.svg'
import LockIcon from "../../assets/lock.svg"
import LoaderComponent from "../common/loaderComponent"
import { sortEventsParams } from "../../common/const"
import SortIcon from "../../assets/sort.svg";
import SortAscIcon from "../../assets/sorting_up.svg";
import SortDescIcon from "../../assets/sorting_down.svg";
import { getFormattedDate, getAlteredString } from "../common/utils"
import { NoRecordFound } from '../common/noRecordFound'
import { ArrowTooltip } from '../common/tooltipComponents'

export const EventTableComponent = (props) => {
  const { eventList, searchParam } = props
  const sortParam = Object.assign({}, searchParam)
  /**
   * sorts the list on the basis of column selected
   */

  const sortList = (index) => {
    if (sortEventsParams[index].isSortable) {
      sortParam.offset = 0
      sortParam.SortField = sortEventsParams[index].value
      if (!sortEventsParams[index].isSorted) {
        sortEventsParams[index].isSorted = "asc"
        sortParam.SortOrder = "asc"
      } else {
        if (sortEventsParams[index].isSorted === "asc") {
          sortEventsParams[index].isSorted = "desc"
          sortParam.SortOrder = "desc"
        } else {
          sortEventsParams[index].isSorted = "asc"
          sortParam.SortOrder = "asc"
        }
      }
      sortEventsParams.map((item, indexData) => {
        if (indexData !== index) {
          item.isSorted = null
        }
        return item
      })
      props.updateProps(sortParam, 1)
    }
  }

  /**
   * renders complete table data
   */
  const renderEventTableCells = () => {
    return <TableBody>
      {eventList ?
        eventList.map((row, i) => (
          <TableRow key={row.Number + i}>
            <TableCell>
              <ArrowTooltip title={row.Number} placement="bottom">
                <Link onClick={() => {
                  props.history.push('/underwritingnew/events-details/' + row.Id)
                }}>{row.Number ? getAlteredString(row.Number, 20) : "-"}</Link>
              </ArrowTooltip>
            </TableCell>
            <TableCell>
              {row.State && row.State.Name ? row.State.Name : "-"}
            </TableCell>
            <TableCell>
              {row.County && row.County.Name ?
                <ArrowTooltip title={row.County.Name} placement="bottom">
                  <span>{getAlteredString(row.County.Name, 20)}</span>
                </ArrowTooltip> : "-"}
            </TableCell>
            <TableCell>
              {row.Type && row.Type.Name ? row.Type.Name : "-"}
            </TableCell>
            <TableCell>
              {row.AuctionType && row.AuctionType.Name ? row.AuctionType.Name : "-"}
            </TableCell>
            <TableCell>
              {row.SaleDate ? getFormattedDate(row.SaleDate) : "-"}
            </TableCell>
            <TableCell>
              {row.FundingDate ? getFormattedDate(row.FundingDate) : "-"}
            </TableCell>
            <TableCell>
              {
                !row.IsLocked ? <span className="progressViewIcon">
                  <img src={UnlockIcon} alt="unlock" />
                </span> : <span className="progressViewIcon">
                    <img src={LockIcon} alt="lock" />
                  </span>
              }
            </TableCell>
          </TableRow>
        )) : <LoaderComponent />}
    </TableBody >
  }

  /**
   * renders header of table
   */
  const renderEventTableHeader = () => {
    return <TableHead>
      <TableRow>
        {sortEventsParams.map((item, index) => {
          return <TableCell key={"col" + index} align="left">
            <div>
              <span>
                {item.name}
              </span>
              {item.isSortable ?
                item.isSorted === "asc" ? <img className="sort-icon"
                  src={SortAscIcon}
                  alt="sort"
                  onClick={() => sortList(index)}></img> :
                  item.isSorted === "desc" ? <img className="sort-icon"
                    src={SortDescIcon}
                    alt="sort"
                    onClick={() => sortList(index)}></img> : <img className="sort-icon"
                      src={SortIcon}
                      alt="sort"
                      onClick={() => sortList(index)}></img> : <span></span>}
            </div>
          </TableCell>
        })
        }
        {/* <TableCell>
          Progress
        <ProgressViewType {...props} />
        </TableCell> */}
      </TableRow>
    </TableHead>
  }

  return <Scrollbars
    autoHeight
    autoHeightMin={'100%'}
    autoHeightMax={'100%'}
    renderTrackHorizontal={prop => <div {...prop} className="track-horizontal" />}
    thumbSize={300}>
    <div className="fui-wrapper">
      <Paper className="alineaTable">{
        !props.showLoaderStatus && eventList && !eventList.length ?
          <NoRecordFound /> :
          <Table>
            {renderEventTableHeader()}
            {!props.showLoaderStatus && eventList && eventList.length ? renderEventTableCells() : <LoaderComponent />}
          </Table>
      }

      </Paper>
    </div>
  </Scrollbars>
}