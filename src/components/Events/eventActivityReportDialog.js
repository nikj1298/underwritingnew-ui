import React, { Component, Fragment } from 'react'
import '../common-components.css'
import 'date-fns';
import {
	Button, Grid, Typography, FormControl, InputLabel,
	AppBar, Tabs, Tab, Dialog, FormHelperText
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { styles, DialogActions, DialogTitle, DialogContent } from "../common/dialogComponent"
import ActivityReportIcon from '../../assets/activity-report-icon.svg';
import { GenerateReportDialog } from "./generateReportDialog"
import { TabPanel } from "../common/tabPanelComponent"
import { a11Props, checkReviewReportValidity } from "../common/utils"
import { DropdownComponent } from "../common/dropdownComponent"
import LoaderComponent from "../common/loaderComponent"
import { ArrowTooltip } from '../common/tooltipComponents'

class ActivityReportDialog extends Component {
	constructor(props) {
		super(props);
	}

	/**
	 * handle change of fields
	 */
	handleChange = (event, newValue) => {
		const reviewData = { ...this.props.reviewReportData }
		if (newValue) {
			reviewData.IsEventLocked = true
		} else { reviewData.IsEventLocked = false }
		this.props.updateReviewReportData(reviewData)
		this.props.setValue(newValue)
	}

	/**
	 * handle open of dialog
	 */
	handleClickOpen = () => {
		this.props.setDialogName("Activity")
		const reviewData = { ...this.props.reviewReportData }
		reviewData.IsEventLocked = false
		delete reviewData.SaleDateFrom
		delete reviewData.SaleDateTo
		if (this.props.userReport) { reviewData.IsPerUserReport = true }
		else { reviewData.IsPerUserReport = false }
		this.props.updateReviewReportData(reviewData)
		this.props.setOpen(true)
	}

	/**
	 * action to generate report
	 */
	handleGenerateReport = () => {
		const { validateReviewReport, reviewReportData } = this.props
		const finalReviewData = { ...reviewReportData }
		finalReviewData.IsPerUserReport = true
		if (!this.props.value) {
			delete finalReviewData.SaleDateFrom
			delete finalReviewData.SaleDateTo
		}
		const checkFieldValidity = checkReviewReportValidity(finalReviewData, validateReviewReport)
		this.props.validateReviewReportData(checkFieldValidity)
		if (Object.values(checkFieldValidity).indexOf(true) > -1) return
		else {
			this.props.generateDownloadReport(finalReviewData)
		}
	}

	/**
	 * render dialog action button
	 */
	renderDialogActions = () => {
		return <DialogActions id="reviewReportActions">
			<Button id="generateActivityReport" onClick={this.handleGenerateReport} variant="contained" color="primary" className="fab_btn">
				Generate
     </Button>
			<Button className="fab_btn" onClick={this.props.handleClose} >
				Cancel
     </Button>
		</DialogActions>
	}

	/**
	 * render Dialog Title
	 */
	renderDialogTitle = () => {
		return <DialogTitle id="addReminderTitle">
			<div className="aaddReminderTitleBack">
				<Typography variant="h6">Generate Activity Report</Typography>
			</div>
		</DialogTitle>
	}

	/**
	 * render dialog tabs
	 */
	renderDialogTabs = () => {
		return <div className="dragAppBar">
			<AppBar position="static">
				<Tabs value={this.props.value} onChange={this.handleChange} aria-label="simple tabs example">
					<Tab label="Active" {...a11Props(0)} />
					<Tab label="Locked" {...a11Props(1)} />
				</Tabs>
			</AppBar>
		</div>
	}

	/**
	 * render dialog content
	 */
	renderDialogContent = () => {
		const { reviewReportData, allStatesList, classes, validateReviewReport, handleChange } = this.props
		return <DialogContent className="customScroll" style={{ paddingTop: 0 }}>
			<TabPanel value={this.props.value} index={0}>
				<form>
					<Grid item sm={12} container className="GridStyle FormStyle">
						<Grid item sm={8}>
							<FormControl className={classes.formControl}>
								<InputLabel htmlFor="record-helper">State</InputLabel>
								<DropdownComponent name={"StateId"} label={"State"} dropDownList={allStatesList.allStatesList}
									handleChange={handleChange} validateData={validateReviewReport.StateId}
									valueData={reviewReportData["StateId"]} />
								{validateReviewReport.StateId ?
									<FormHelperText id="component-error-text" error >State is required.</FormHelperText> : ""}
							</FormControl>
						</Grid>
					</Grid>
				</form>
			</TabPanel>
			<TabPanel value={this.props.value} index={1}>
				<GenerateReportDialog {...this.props} />
			</TabPanel>
		</DialogContent>
	}

	/**
	 * render complete dialog of activity report
	 */
	renderDialog = () => {
		return <Dialog id="addReminderDialog" fullWidth={true} maxWidth={"md"} onClose={this.props.handleClose} aria-labelledby="customized-dialog-title" open={this.props.open} className="titleDialog">
			<div className="widthaddReminder">
				{this.renderDialogTitle()}
				{this.renderDialogTabs()}
				{this.renderDialogContent()}
				{this.renderDialogActions()}

			</div>
		</Dialog>
	}


	render() {
		return (
			<Fragment>
				<ArrowTooltip title={"Reviewer Activity"} placement="bottom">
					<Button id="activityReportButton" aria-controls="table-actionlist" aria-haspopup="true" onClick={this.handleClickOpen}>
						<img src={ActivityReportIcon} alt="report" />
					</Button>
				</ArrowTooltip>
				{this.props.dialogName === "Activity" ? this.renderDialog() : null}
				{this.props.showLoaderStatus ? <LoaderComponent /> : null}
			</Fragment>
		);
	}
}


export default withStyles(styles)(ActivityReportDialog);