// import React, { Component, Fragment } from 'react'
// import '../common-components.css'
// import 'date-fns';
// import DateFnsUtils from '@date-io/date-fns';
// import Button from '@material-ui/core/Button';
// import { withStyles } from '@material-ui/core/styles';
// import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
// import DialogContent from '@material-ui/core/DialogContent';
// import DialogTitle from '@material-ui/core/DialogTitle';
// import Grid from '@material-ui/core/Grid';
// import Typography from '@material-ui/core/Typography';
// import IconButton from '@material-ui/core/IconButton';
// import CloseIcon from '@material-ui/icons/Close';
// import MenuItem from '@material-ui/core/MenuItem';
// import TextField from '@material-ui/core/TextField';
// import FormControl from '@material-ui/core/FormControl';
// import InputLabel from '@material-ui/core/InputLabel';
// import Select from '@material-ui/core/Select';
// import Paper from '@material-ui/core/Paper';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Checkbox from '@material-ui/core/Checkbox';
// import InputAdornment from '@material-ui/core/InputAdornment';
// import {
//     MuiPickersUtilsProvider,
//     KeyboardDatePicker,
//     KeyboardTimePicker
// } from '@material-ui/pickers';
// import PlusIcon from '../../assets/add_opportunity.svg';
// import clock_icon from '../../assets/clock.svg'
// import UploadzoneArea from '../Upload/UploadzoneArea'

// const styles = theme => ({
//     formControl: {
//         margin: theme.spacing(0.1, 0),
//     },
// });

// class NewEventDialog extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             open: false,
//             record: '',
//             propertytype: '',
//             stagetype: '',
//             primarycontact: '',
//             loantype: '',
//             perpaytype: '',
//         };
//         this.handleRecord = this.handleRecord.bind(this);
//         this.handlePropertType = this.handlePropertType.bind(this);
//         this.handleStageType = this.handleStageType.bind(this);
//         this.handlePrimaryContact = this.handlePrimaryContact.bind(this);
//         this.handleLoanType = this.handleLoanType.bind(this);
//         this.handlePerpayType = this.handlePerpayType.bind(this);
//     }

//     handleClickOpen = () => {
//         this.setState({ open: true });
//     }
//     handleClose = () => {
//         this.setState({ open: false });
//     }
//     handleRecord(event) {
//         this.setState({ record: event.target.value });
//     }
//     handlePropertType(event) {
//         this.setState({ propertytype: event.target.value });
//     }
//     handleStageType(event) {
//         this.setState({ stagetype: event.target.value });
//     }
//     handlePrimaryContact(event) {
//         this.setState({ primarycontact: event.target.value });
//     }
//     handleLoanType(event) {
//         this.setState({ loantype: event.target.value });
//     }
//     handlePerpayType(event) {
//         this.setState({ perpaytype: event.target.value });
//     }


//     render() {
//         const { classes } = this.props;
//         return (
//             <Fragment>
//                 <Button aria-controls="table-actionlist" aria-haspopup="true" onClick={this.handleClickOpen}>
//                     <img src={PlusIcon} alt="plus" />
//                 </Button>
//                 <Dialog fullWidth={true} maxWidth={true} onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={this.state.open} className="titleDialog maxDialog">
//                     <DialogTitle id="customized-dialog-title" onClose={(event) => this.props.handleClose(event)}>
//                         <div className="popupBgPic">
//                             <Typography variant="h6">New Event</Typography>
//                             <IconButton aria-label="close" className="closeButton" onClick={this.handleClose}>
//                                 <CloseIcon onClick={this.handleClose} />
//                             </IconButton>
//                         </div>
//                     </DialogTitle>
//                     <DialogContent className="customScroll">
//                         <div className="dialogInnerContent">
//                             <form>
//                                 <Grid item sm={12} container className="GridStyle GridMargin FormStyle">
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">State</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="State"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="County/Jurisdiction"
//                                             className="mandatoryField"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                         />
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">Event Type</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="Event Type"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">Final Payment Type</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="Final Payment Type"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="Diposit Amount"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                             InputProps={{
//                                                 startAdornment: <InputAdornment position="start">$</InputAdornment>,
//                                             }}
//                                         />
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="Treasurer Fee"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                             InputProps={{
//                                                 startAdornment: <InputAdornment position="start">$</InputAdornment>,
//                                             }}
//                                         />
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="Interest Rate"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                             InputProps={{
//                                                 endAdornment: <InputAdornment position="end">%</InputAdornment>,
//                                             }}
//                                         />
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">Sale Date Status</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="Sale Date Status"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <MuiPickersUtilsProvider utils={DateFnsUtils}>
//                                             <Grid container justify="space-around">
//                                                 <KeyboardDatePicker
//                                                     disableFuture
//                                                     disablePresent
//                                                     disableToolbar
//                                                     variant="inline"
//                                                     format="MM/dd/yyyy"
//                                                     id="datepicker"
//                                                     className="calendarField mandatoryField"
//                                                     label="Sale Date"
//                                                     value={this.selectedDOB}
//                                                     onChange={this.handleDOB}
//                                                     KeyboardButtonProps={{
//                                                         'aria-label': 'change date',
//                                                     }}
//                                                 />
//                                             </Grid>
//                                         </MuiPickersUtilsProvider>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <MuiPickersUtilsProvider utils={DateFnsUtils}>
//                                             <Grid container justify="space-around">
//                                                 <KeyboardDatePicker
//                                                     disableFuture
//                                                     disablePresent
//                                                     disableToolbar
//                                                     variant="inline"
//                                                     format="MM/dd/yyyy"
//                                                     id="datepicker"
//                                                     className="calendarField"
//                                                     label="Funding Date"
//                                                     value={this.selectedDOB}
//                                                     onChange={this.handleDOB}
//                                                     KeyboardButtonProps={{
//                                                         'aria-label': 'change date',
//                                                     }}
//                                                 />
//                                             </Grid>
//                                         </MuiPickersUtilsProvider>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <MuiPickersUtilsProvider utils={DateFnsUtils}>
//                                             <Grid container justify="space-around">
//                                                 <KeyboardDatePicker
//                                                     disableFuture
//                                                     disablePresent
//                                                     disableToolbar
//                                                     variant="inline"
//                                                     format="MM/dd/yyyy"
//                                                     id="datepicker"
//                                                     className="calendarField"
//                                                     label="Registration Deadline"
//                                                     value={this.selectedDOB}
//                                                     onChange={this.handleDOB}
//                                                     KeyboardButtonProps={{
//                                                         'aria-label': 'change date',
//                                                     }}
//                                                 />
//                                             </Grid>
//                                         </MuiPickersUtilsProvider>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <MuiPickersUtilsProvider utils={DateFnsUtils}>
//                                             <Grid container justify="space-around">
//                                                 <KeyboardDatePicker
//                                                     disableFuture
//                                                     disablePresent
//                                                     disableToolbar
//                                                     variant="inline"
//                                                     format="MM/dd/yyyy"
//                                                     id="datepicker"
//                                                     className="calendarField"
//                                                     label="Diposit Deadline"
//                                                     value={this.selectedDOB}
//                                                     onChange={this.handleDOB}
//                                                     KeyboardButtonProps={{
//                                                         'aria-label': 'change date',
//                                                     }}
//                                                 />
//                                             </Grid>
//                                         </MuiPickersUtilsProvider>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">Auction Type</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="Auction Type"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="Auction Address"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                         />
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <MuiPickersUtilsProvider utils={DateFnsUtils}>
//                                             <Grid container justify="space-around">
//                                                 <KeyboardTimePicker
//                                                     clearable
//                                                     disablePast
//                                                     autoOk
//                                                     id="time"
//                                                     label="Time"
//                                                     name="SheduledTime"
//                                                     className="pickerIconsCustom"
//                                                     value={this.state.time}
//                                                     onChange={this.handleTimeChange}
//                                                     keyboardIcon={<img src={clock_icon} alt="Time Picker" />}
//                                                     KeyboardButtonProps={{
//                                                         'aria-label': 'change time'
//                                                     }}
//                                                 />
//                                             </Grid>
//                                         </MuiPickersUtilsProvider>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="Estimated Purchase Amount"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                             InputProps={{
//                                                 startAdornment: <InputAdornment position="start">$</InputAdornment>,
//                                             }}
//                                         />
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="Estimated Deposit Amount"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                             InputProps={{
//                                                 startAdornment: <InputAdornment position="start">$</InputAdornment>,
//                                             }}
//                                         />
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="Refund Amount"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                             InputProps={{
//                                                 startAdornment: <InputAdornment position="start">$</InputAdornment>,
//                                             }}
//                                         />
//                                     </Grid>
//                                 </Grid>
//                                 <Grid item sm={12} container alignItems="flex-end" className="GridStyle GridMargin FormStyle">
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="Treasurer Name"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                         />
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <TextField
//                                             id="minamount"
//                                             label="Treasurer Email"
//                                             value={this.state.minLoanAmt}
//                                             onChange={this.handleMinLoanAmt}
//                                         />
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <FormControlLabel
//                                             control={
//                                                 <Checkbox
//                                                     checked={this.state.checkedB}
//                                                     value="checkedB"
//                                                     color="primary"
//                                                 />
//                                             }
//                                             label="Reject Reason Required"
//                                         />
//                                     </Grid>

//                                 </Grid>


//                                 <Paper className="paperGray noFieldLine">
//                                     <Typography component="div" className="cardSubHeader">
//                                         <Typography id='primaryDetails' variant="h6">
//                                             Departments Assignment
//                                         </Typography>
//                                     </Typography>
//                                 </Paper>

//                                 <Grid item sm={12} container className="GridStyle GridMargin FormStyle">
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">Accounting</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="Accounting"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">Acquisition</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="Acquisition"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">Auction Type</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="Asset Management"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">Date</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="Date"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                     <Grid item sm={4}>
//                                         <div className="mandatoryField">
//                                             <FormControl className={classes.formControl}>
//                                                 <InputLabel htmlFor="record-helper">Management</InputLabel>
//                                                 <Select
//                                                     id="Type"
//                                                     label="Management"
//                                                     name="Type"
//                                                     value={this.state.record}
//                                                     onChange={this.handleRecord}
//                                                     MenuProps={{
//                                                         anchorOrigin: {
//                                                             vertical: "bottom",
//                                                             horizontal: "left"
//                                                         },
//                                                         transformOrigin: {
//                                                             vertical: "top",
//                                                             horizontal: "left"
//                                                         },
//                                                         getContentAnchorEl: null
//                                                     }}>
//                                                     <MenuItem value={10}>Ten</MenuItem>
//                                                     <MenuItem value={20}>Twenty</MenuItem>
//                                                     <MenuItem value={30}>Thirty</MenuItem>
//                                                 </Select>
//                                             </FormControl>
//                                         </div>
//                                     </Grid>
//                                 </Grid>

//                                 {/* <div>
//                                     <UploadzoneArea />
//                                 </div> */}

//                             </form>
//                         </div>
//                     </DialogContent>

//                     <DialogActions className="contentFlexStart">
//                         <Button onClick={this.handleClose} variant="contained" color="primary" className="fab_btn">
//                             Create
//               </Button>
//                         <Button className="fab_btn" onClick={this.handleClose} >
//                             Cancel
//               </Button>
//                     </DialogActions>
//                 </Dialog>
//             </Fragment>
//         );
//     }
// }


// export default withStyles(styles)(NewEventDialog);