import React, { Component, Fragment } from 'react'
import '../common-components.css'
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import MuiDialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Add from '../../assets/add_circle.svg'
import MenuItem from '@material-ui/core/MenuItem'
import LoaderComponent from '../../common/loaderComponent'
// import CustomSnackbar from '../common/CustomSnackbar'
import FormHelperText from '@material-ui/core/FormHelperText'
import InputLabel from '@material-ui/core/InputLabel'


const styles = theme => ({
    formControl: {
        margin: theme.spacing(0.1, 0),
        width: 250,
    },
});

const DialogContent = withStyles((theme) => ({
    root: {
        paddingBottom: 30,
    }
}))(MuiDialogContent)

const booleanLogic = {
    List: [{ value: true, Name: "Yes" },
    { value: false, Name: "No" }]
}

let rule = {
    RuleItems: [],
    RuleName: "" ,
    DataCutResultTypeId : "" 
}

let eventIdSelected = ''

class CampaignAddNewRule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            alertMessage: "",
            isInvalid: false,
            isInvalidName: false,
            isInvalidType: false,
            checkAllItems: false,
            isChanged: false,
            noRuleSelected: false ,
            rulesFieldList : []
        };
    }

    componentDidMount() {
        eventIdSelected = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);
    }

    handleClickOpen = () => {
        rule = {
            RuleItems: [],
            RuleName: "",
            DataCutResultTypeId : null
        }
        this.setState({ open: true });
        this.loadRulesFieldsList();
        // this.props.fetchGeneralLandusecodes()
    }

    loadRulesFieldsList = () =>{
        this.props.getRulesPossiblityListRules().then((res) => {
            this.setState({
                rulesFieldList : res.data.List
            })
        })
    }

    handleClose = () => {
        rule = {
            RuleItems: [],
            RuleName: "",
            DataCutResultTypeId : null
        }
        this.setState({
            open: false,
            isInvalid: false,
            isInvalidName: false,
            isInvalidType : false,
            checkAllItems: false,
            isChanged: false,
            noRuleSelected: false
        });
    }

    handleChange = (event, row, index) => {
        let newRuleFields = { ruleIndex: -1, Value: "", DataCutLogicTypeId: -1, DataCutRuleFieldId: -1, valid: true }
        let fieldName = event.target.name
        let newRule = {}

        if (fieldName === "checkAllRuleItems") {
            if (this.state.checkAllItems)
                rule.RuleItems = []
            else {
                this.state.rulesFieldList.map((listItem, i) => {
                    newRule = { ...newRuleFields, ["ruleIndex"]: i, ["DataCutLogicTypeId"]: listItem.LogicTypes[0].Id, ["DataCutRuleFieldId"]: listItem.Id }
                    rule.RuleItems.push(newRule)
                })
            }
            this.setState({ checkAllItems: !this.state.checkAllItems })
        } else
            if (fieldName === "RuleName") {
                rule.RuleName = event.target.value
            }else if (fieldName === "DataCutResultTypeId"){
                rule.DataCutResultTypeId = event.target.value
            } else if (rule.RuleItems.length === 0) {
                if (fieldName === "DataCutRuleFieldId")
                    newRule = { ...newRuleFields, ["ruleIndex"]: index, ["DataCutLogicTypeId"]: row.LogicTypes[0].Id, ["DataCutRuleFieldId"]: row.Id }
                else if (fieldName === "Value")
                    newRule = { ...newRuleFields, ["ruleIndex"]: index, ["DataCutLogicTypeId"]: row.LogicTypes[0].Id, ["Value"]: event.target.value }
                else
                    newRule = { ...newRuleFields, ["ruleIndex"]: index, [fieldName]: event.target.value }
                rule.RuleItems.push(newRule)
                this.setState({ isChanged: true })
            } else {
                let checkExist = rule.RuleItems.filter((ele) => {
                    return ele.ruleIndex === index
                })
                if (checkExist.length > 0) {
                    rule.RuleItems.forEach((element, idx) => {
                        if (element.ruleIndex === index) {
                            if (fieldName === "DataCutRuleFieldId") {
                                if (element.DataCutRuleFieldId === row.Id) {
                                    let eleIndex = rule.RuleItems.indexOf(element);
                                    rule.RuleItems.splice(eleIndex, 1);
                                }
                                else rule.RuleItems[idx] = { ...rule.RuleItems[idx], ["DataCutRuleFieldId"]: row.Id }
                                this.setState({ isChanged: true })
                            } else {
                                rule.RuleItems[idx] = { ...rule.RuleItems[idx], ["ruleIndex"]: index, [fieldName]: event.target.value }
                            }
                            this.setState({ isChanged: true })
                        }
                    })
                } else {
                    if (fieldName === "DataCutRuleFieldId")
                        newRule = { ...newRuleFields, ["ruleIndex"]: index, ["DataCutLogicTypeId"]: row.LogicTypes[0].Id, ["DataCutRuleFieldId"]: row.Id }
                    else if (fieldName === "Value")
                        newRule = { ...newRuleFields, ["ruleIndex"]: index, ["DataCutLogicTypeId"]: row.LogicTypes[0].Id, ["Value"]: event.target.value }
                    else
                        newRule = { ...newRuleFields, ["ruleIndex"]: index, [fieldName]: event.target.value }
                    rule.RuleItems.push(newRule)
                    this.setState({ isChanged: true })
                }
            }
    }

    checkChangedValue = (row, index) => {
        let ruleFieldId = rule.RuleItems.filter(item => {
            return item.DataCutRuleFieldId === row.Id || item.ruleIndex === index
        })
        if (ruleFieldId.length > 0)
            return ruleFieldId[0].DataCutLogicTypeId
    }

    callbackFunction = (childData) => {
        this.setState({ alertMessage: '' })
    }

    createNewRule = async () => {
        rule.RuleItems.map((item, index) => {
            if (item.DataCutRuleFieldId !== -1 && item.Value === "") {
                rule.RuleItems[index] = { ...rule.RuleItems[index], ["valid"]: false }
            } else if (item.DataCutRuleFieldId !== -1 && item.Value !== "") {
                rule.RuleItems[index] = { ...rule.RuleItems[index], ["valid"]: true }
            }
        })


        let finalInvalidRule = rule.RuleItems.filter(item => {
            return item.valid === false
        })
        if (finalInvalidRule.length > 0) {
            this.setState({ isInvalid: true })
        }


        if (rule.RuleName === "") {
            this.setState({ isInvalidName: true })
        } else this.setState({ isInvalidName: false })

        if (rule.DataCutResultTypeId === "") {
            this.setState({ isInvalidType: true })
        } else this.setState({ isInvalidType: false })
        

        let countUncheckedRuleItem = rule.RuleItems.filter(item => {
            return item.DataCutRuleFieldId === -1
        })
        if (countUncheckedRuleItem.length === rule.RuleItems.length) {
            this.setState({ noRuleSelected: true })
        } else this.setState({ noRuleSelected: false })


        let countCheckedRuleItem = rule.RuleItems.filter(item => {
            return item.DataCutRuleFieldId !== -1
        })
        let finalRule = rule.RuleItems.filter(item => {
            return item.DataCutRuleFieldId !== -1 && item.Value !== ""
        })
        if (finalRule.length !== 0 && finalRule.length === countCheckedRuleItem.length && rule.RuleName !== '') {
            rule = {
                RuleItems: finalRule,
                RuleName: rule.RuleName,
                DataCutResultTypeId : rule.DataCutResultTypeId
            }
            this.props.postsaveAllNewDataRules( eventIdSelected , rule).then(res =>{
                this.props.refreshList();
            })
            // this.props.createNewRulecreateNewRule(rule).then(response => {
            //     if (response) {
            //         const notification = JSON.parse(sessionStorage.getItem('notification'))
            //         if (notification.Code && (notification.Code.Id === 200 && notification.Code.Name === 'OK')) {
            //             this.props.fetchCampaignRules(this.props.campaignDetails.Id)
            //             this.setState({ showLoader: false, alertMessage: "New rule has been created successfully" })
            //         }
            //     }
            // })
            this.handleClose()
        }
    }

    checkValidRuleItem = (row, index) => {
        let ruleFieldId = rule.RuleItems.filter(item => {
            if (item.DataCutRuleFieldId === row.Id || item.ruleIndex === index)
                return item.valid === false
        })
        if (ruleFieldId.length > 0)
            return true
    }

    checkRow = (row, index) => {
        let checkStatus = rule.RuleItems.filter(item => {
            return item.DataCutRuleFieldId === row.Id && item.ruleIndex === index
        })
        if (checkStatus.length > 0 || (this.state.checkAllItems === true && rule.RuleItems.length === this.state.rulesFieldList.length))
            return true
        else return false
    }

    renderDropdownValue = (row, index) => {
        let checkStatus = rule.RuleItems.filter(item => {
            return item.DataCutRuleFieldId === row.Id || item.ruleIndex === index
        })
        if (checkStatus.length > 0)
            return checkStatus[0].Value
    }

    renderDialogTitleAndText = () => {
        return <Fragment>
            <DialogTitle id="customized-dialog-title">
                <div className="popupBgPic">
                    <Typography variant="h6">New Rule</Typography>
                    <IconButton aria-label="close" className="closeButton" onClick={() => this.handleClose()}>
                        <CloseIcon />
                    </IconButton>
                </div>
            </DialogTitle>

        </Fragment>
    }

    renderRuleNameField = () => {
        return <div className="dialogInnerContent">
            <form>
                <Grid item sm={12} container className="GridStyle GridMargin FormStyle">
                    <Grid item sm={5}>
                        <TextField
                            id="standard-basic"
                            name="RuleName"
                            label="Rule Name"
                            error={this.state.isInvalidName === true}
                            onChange={(event) => this.handleChange(event)}
                        />
                        {this.state.isInvalidName ? (
                            <FormHelperText id="component-error-text" error>
                                Enter rule name
								</FormHelperText>
                        ) : (
                                ''
                            )}
                    </Grid>
                    <Grid item sm={3} id="selectContain">
                        <InputLabel htmlFor="record-helper">Select Result</InputLabel>
                        <Select
                            id="resutType"
                            label="Select Result"
                            name="DataCutResultTypeId"
                            value={rule.DataCutResultTypeId}
                            onChange={(event) => this.handleChange(event)}
                            error={this.state.isInvalidName === true}
                        >
                            { this.props && this.props.rulesTypesList && this.props.rulesTypesList.List ?
                                this.props.rulesTypesList.List.map(item=>{
                                    return <MenuItem value={item.Id}>{item.Name}</MenuItem>
                                }) 
                                : "" }
                        </Select>
                        {this.state.isInvalidType ? (
                            <FormHelperText id="component-error-text" error>
                                Result is required
								</FormHelperText>
                        ) : (
                                ''
                            )}
                    </Grid>
                </Grid>
            </form>
        </div >
    }

    renderTableHead = () => {
        return <TableHead>
            <TableRow>
                <TableCell>
                    <Checkbox
                        style={{ padding: 2, marginRight: 15 }}
                        name="checkAllRuleItems"
                        checked={this.props.ruleFields && this.state.rulesFieldList &&
                            rule.RuleItems.length === this.state.rulesFieldList.length}
                        onChange={(event) => this.handleChange(event)}
                        color="primary"
                        inputProps={{
                            'aria-label': 'uncontrolled-checkbox',
                        }}
                    />
                    Parameter
      </TableCell>
                <TableCell>Logic</TableCell>
                <TableCell>Value</TableCell>
            </TableRow>
        </TableHead>
    }

    renderTableRows = () => {
        const { classes } = this.props;

        return <TableBody>
            { this.state.rulesFieldList  ? this.state.rulesFieldList.map((row, index) => (
                <TableRow key={row.Name}>
                    <TableCell>
                        <Checkbox
                            style={{ padding: 2, marginRight: 15 }}
                            checked={this.checkRow(row, index)}
                            color="primary"
                            name="DataCutRuleFieldId"
                            onChange={(event) => this.handleChange(event, row, index)}
                            inputProps={{
                                'aria-label': 'uncontrolled-checkbox',
                            }}
                        />
                        {row.Name}
                    </TableCell>


                    <TableCell style={{ width: 300 }}>
                        <Grid item sm={12} container alignItems="center" className="FormStyle">
                            <Grid item sm={4}>
                                <FormControl className={classes.formControl}>
                                    <Select
                                        id="Type"
                                        label="Less Than"
                                        name="DataCutLogicTypeId"
                                        onChange={(event) => this.handleChange(event, row, index)}
                                        value={this.checkChangedValue(row, index) ? this.checkChangedValue(row, index) : row.LogicTypes[0].Id}
                                        MenuProps={{
                                            anchorOrigin: {
                                                vertical: "bottom",
                                                horizontal: "left"
                                            },
                                            transformOrigin: {
                                                vertical: "top",
                                                horizontal: "left"
                                            },
                                            getContentAnchorEl: null
                                        }}>
                                        {row.LogicTypes.map((item, idx) => {
                                            return <MenuItem key={idx} value={row.LogicTypes[idx].Id}>{item.Name}</MenuItem>
                                        })}

                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </TableCell>


                    <TableCell style={{ width: 300 }}>
                        <Grid item sm={12} container alignItems="center" className="FormStyle">
                            <Grid item sm={4}>
                                <FormControl className={classes.formControl}>

                                    {row.Id > 200 ?
                                        <Select
                                            id="Type"
                                            label="Less Than"
                                            name="Value"
                                            error={this.state.isInvalid && this.checkValidRuleItem(row, index)}
                                            onChange={(event) => this.handleChange(event, row, index)}
                                            value={this.renderDropdownValue(row, index)}
                                            MenuProps={{
                                                anchorOrigin: {
                                                    vertical: "bottom",
                                                    horizontal: "left"
                                                },
                                                transformOrigin: {
                                                    vertical: "top",
                                                    horizontal: "left"
                                                },
                                                getContentAnchorEl: null
                                            }}>
                                            {/* {generalLandusecodes.map((item, idx) => {
                                                return <MenuItem key={idx} value={generalLandusecodes[idx].Name}>{item.Name}</MenuItem>
                                            })} */}
                                        </Select>
                                        :

                                        row.Id > 100 ?
                                            <Select
                                                id="Type"
                                                label="Less Than"
                                                name="Value"
                                                error={this.state.isInvalid && this.checkValidRuleItem(row, index)}
                                                onChange={(event) => this.handleChange(event, row, index)}
                                                value={this.renderDropdownValue(row, index)}
                                                MenuProps={{
                                                    anchorOrigin: {
                                                        vertical: "bottom",
                                                        horizontal: "left"
                                                    },
                                                    transformOrigin: {
                                                        vertical: "top",
                                                        horizontal: "left"
                                                    },
                                                    getContentAnchorEl: null
                                                }}>
                                                {/* {booleanLogic.List.map((item, idx) => {
                                                    return <MenuItem key={idx} value={booleanLogic.List[idx].value}>{item.Name}</MenuItem>

                                                })} */}
                                            </Select> :

                                            row.Id === 1 || row.Id === 2 ?
                                                <TextField
                                                    id="standard-basic"
                                                    name="Value"
                                                    error={this.state.isInvalid && this.checkValidRuleItem(row, index)}
                                                    onChange={(event) => this.handleChange(event, row, index)}
                                                    className="mandatoryField"
                                                />
                                                :

                                                <TextField
                                                    id="standard-basic"
                                                    name="Value"
                                                    type="number"
                                                    error={this.state.isInvalid && this.checkValidRuleItem(row, index)}
                                                    onChange={(event) => this.handleChange(event, row, index)}
                                                    className="mandatoryField"
                                                />
                                    }
                                </FormControl>
                                {this.state.isInvalid && this.checkValidRuleItem(row, index) ? (
                                    <FormHelperText id="component-error-text" error>
                                        Enter value
								</FormHelperText>
                                ) : (
                                        ''
                                    )}
                            </Grid>
                        </Grid>
                    </TableCell>
                </TableRow>
            )) : ""}
        </TableBody>
    }

    renderDialogActions = () => {
        return <DialogActions className="contentFlexStart">
            <Button onClick={() => this.createNewRule()} variant="contained" color="primary" className="fab_btn">
                Create
              </Button>
            <Button onClick={() => this.handleClose()} className="fab_btn">
                Cancel
              </Button>
        </DialogActions>
    }

    render() {
        return (
            <Fragment>
                <span className="tinyActionBtn" >
                    <Button onClick={() => this.handleClickOpen()}>
                        Add New Rule
            <img alt="Add Contact" src={Add} />
                    </Button>
                </span>
                {/* !this.state.showLoader && ruleFields && ruleFields.List && ruleFields.List.length > 0
                    && generalLandusecodes && generalLandusecodes.List && generalLandusecodes.List.length > 0 */}
                {!this.props.showLoaderStatus ?
                    <Dialog fullWidth={true} maxWidth={"lg"} open={this.state.open} aria-labelledby="customized-dialog-title" className="titleDialog">
                        {this.renderDialogTitleAndText()}
                        <DialogContent className="customScroll">
                            {this.renderRuleNameField()}
                            <Paper className="alineaTable cardTable alignTableDefault">
                                <Table aria-label="simple table">
                                    {this.renderTableHead()}
                                    {this.renderTableRows()}
                                </Table>
                            </Paper>
                            {this.state.noRuleSelected ? <span className="redAlertText">Select at least one item from the list</span> : ""}

                        </DialogContent>
                        {this.renderDialogActions()}
                    </Dialog>
                    : <LoaderComponent />
                }
                {/* {this.state.alertMessage !== '' ? <CustomSnackbar message={this.state.alertMessage} parentCallback={this.callbackFunction} /> : ''} */}

            </Fragment>
        );
    }
}


export default withStyles(styles)(CampaignAddNewRule);