import React, { Fragment, Component } from "react"
import { Button, Grid, TextField, Typography, makeStyles } from '@material-ui/core'
import SummaryIcon from '../../assets/summary.svg'
import Paper from '@material-ui/core/Paper'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Icon from '@material-ui/core/Icon';
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import InputAdornment from '@material-ui/core/InputAdornment'

const useStyles = makeStyles(theme => ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        color: '#815739',
    }
}));

export default class BatchAssignmentComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            open: false,
        };
    }

    handleClickOpen = event => {
        this.setState({ anchorEl: event.currentTarget })
    };

    handleClose = () => {
        this.setState({ anchorEl: null })
    };

    render() {
        return <Fragment>
            <Typography component="div" className="flexView-Heading">
                <span className="formHeading">
                    Batch Assignment -&nbsp;
                    <span className="batchMenuBtn" aria-controls="batch-menu" aria-haspopup="true" onClick={this.handleClickOpen}>
                        Final Level <Icon>expand_more</Icon>
                    </span>
                </span>
                <Menu
                    id="batch-menu"
                    className="batchDropMenu"
                    anchorEl={this.state.anchorEl}
                    keepMounted
                    open={Boolean(this.state.anchorEl)}
                    onClose={this.handleClose}
                    getContentAnchorEl={null}
                    anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
                    transformOrigin={{ vertical: "top", horizontal: "right" }}
                >
                    <MenuItem onClick={this.handleClose}>Final Level</MenuItem>
                    <MenuItem onClick={this.handleClose}>Level 1</MenuItem>
                    <MenuItem onClick={this.handleClose}>Level 2</MenuItem>
                </Menu>
                <span>
                    <Button variant="contained" color="primary" className="fab_btn">Assign</Button>
                    <span className="dropListViewIcon" style={{ marginLeft: 15 }}>
                        <Button>
                            <img src={SummaryIcon} alt="summary" />
                        </Button>
                    </span>
                </span>
            </Typography>
            <Paper className="paperCard paperCardHeading">
                <Paper className="paperGray">
                    <Typography component="div" className="cardSubHeader formHeadingFlex cardHeadingFlex">
                        <Typography id='primaryDetails' variant="h6">
                            Select Profiles
                        </Typography>
                        <span className="innerFlex">
                            <span className="totalLatestYear">
                                <p className="totalLabel">
                                    TOTAL NUMBER OF RECORDS <br />
                                    <span>36719</span>
                                </p>
                            </span>
                            <span className="totalLatestYear">
                                <p className="totalLabel">
                                    TOTAL PURCHASED RECORDS <br />
                                    <span>36719</span>
                                </p>
                            </span>
                        </span>
                    </Typography>
                </Paper>
                <div className="paperContent">
                    content area
                </div>
            </Paper>

        </Fragment>
    }
}