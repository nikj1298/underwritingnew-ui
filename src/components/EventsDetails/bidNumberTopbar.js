import React from "react"
import { DropDownMenu } from "../common/dropDownMenu"
import { SearchBarComponent } from "../common/searchBarComponent"
import { ListPagination } from "../common/listPagination"
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import CalIcon from '../../assets/more.svg';
import deleteIcon from '../../assets/deleteIcon.svg';
import '../common-components.css'
import AddBidNumberDialog from './addBidNumberDialog'


export const BidNumberTopbar = (props) => {
  return <div className="tableTopRow">
    <div>
      <span class="bidHeadingText">Bid Number</span>
    </div>
    <div className="paginationSearch">
      <SearchBarComponent  {...props} />
      <span className="topBarListIcon dropListViewIcon">
        <span className="loanExpandList">
          <Button className="" aria-controls="table-actionlist" aria-haspopup="true" onClick={props.handleOpen}>
            <img src={CalIcon} alt="More" />
          </Button>
        </span>
        <Menu
          className="tableActionListMenu"
          id="table-actionlist"
          anchorEl={props.anchorEl}
          keepMounted
          open={Boolean(props.anchorEl)}
          onClose={props.handleCloseMenu}
          getContentAnchorEl={null}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          transformOrigin={{ vertical: "top", horizontal: "right" }}
        >
          <MenuItem onClick={props.handleCloseMenu}>Export Data</MenuItem>
          <MenuItem onClick={props.handleCloseMenu}>Import Data</MenuItem>
        </Menu>
      </span>
      {props.itemsChecked ? <span className="topBarListIcon dropListViewIcon">
        <Button className="" aria-controls="table-actionlist" onClick={props.deleteCall}>
          <img src={deleteIcon} alt="Delete" />
        </Button>
      </span> : ""}
      <AddBidNumberDialog {...props} />
      <ListPagination {...props} />
    </div>
  </div>
}