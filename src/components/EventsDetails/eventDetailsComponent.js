import React, { Fragment, Component } from "react"
import Header from '../../pages/header';
import { SideMenu } from './sideMenu'
import { SubHeader } from '../common/subHeader'
import BidNumberContainer from '../../containers/Events/bidNumberContainer'
import EventDetailsFormContainer from './../../containers/Events/eventDetailsFormContainer'
import TaskContainer from './../../containers/Events/taskContainer'
import BatchAssignmentContainer from './../../containers/Events/batchAssignmentContainer'

export default class EventDetailsComponent extends Component {

  constructor(props) {
    super(props)
    this.state = {
      activePage: 0
    }
  }

  changePage = (data) => {
    this.setState({
      activePage: data
    })
  }

  render() {
    return <Fragment>
      <Header {...this.props} />
      <SubHeader {...this.props} />
      <div className="contentWrapper">
        <SideMenu {...this.props} changePage={this.changePage} selectedSection={this.state.activePage} />
        <div className="contentArea">
          {
            this.state.activePage == 0 ?
              <EventDetailsFormContainer {...this.props} /> : this.state.activePage == 1 ?
                <BidNumberContainer {...this.props} /> : this.state.activePage == 2 ?
                  <BatchAssignmentContainer {...this.props} /> : this.state.activePage == 3 ?
                    <TaskContainer {...this.props} /> : ""
          }
        </div>
      </div>
    </Fragment>
  }
}