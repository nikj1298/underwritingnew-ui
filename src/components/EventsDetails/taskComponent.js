import React, { Fragment, Component } from "react"
import { Typography, Paper, TableRow, Table, TableCell, TableHead, TableBody } from '@material-ui/core'
import { ListPagination } from "../common/listPagination"
import { tasksTableHead, filterParamsTasksList } from '../../common/const'
import RecordnotFound from '../../assets/not-found.png'
import '../common-components.css'
import LoaderComponent from '../../common/loaderComponent';

let eventIdSelected = ''

export default class TaskComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterParams: JSON.parse(JSON.stringify(filterParamsTasksList))
        }
    }

    componentDidMount() {
        this.setState({
            filterParams: JSON.parse(JSON.stringify(filterParamsTasksList))
        })
        eventIdSelected = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);
        this.props.getTasksList(eventIdSelected);
    }

    /**
    * renders tasks table head data
    */
    renderTableHeader = () => {
        return <TableHead id="headerId">
            <TableRow id="tableHeadRow">
                {tasksTableHead.map((item) => {
                    return <TableCell>{item}</TableCell>
                })}
            </TableRow>
        </TableHead>
    }

    /**
    * show status class based on current status
    */
    currentStatusClass = (status) => {
        if (status) {
            switch (status.toLowerCase()) {
                case 'completed':
                    return 'Approved'
                case 'inprogress':
                    return 'Submitted'
                case 'overdue':
                    return 'Returned'
                default:
                    return ''
            }
        } else
            return ''
    }

    /**
    *  change page
    */
    updateProps = (params, pageNumber) => {
        let filteringData = this.state.filterParams;
        filteringData.pageNumber = pageNumber;
        this.setState({
            filterParams: filteringData
        })
    }

    /**
    * renders tasks table body data
    */
    renderTableRows = () => {
        return <TableBody id="tableBody" >
            {this.props.taskList.taskList.List.map((item, indexing) => {
                if ((indexing) >= ((this.state.filterParams.pageNumber - 1) * this.state.filterParams.pageSize) && (indexing + 1) <= (this.state.filterParams.pageNumber * this.state.filterParams.pageSize)) {
                    return <TableRow id="tableHeadRow">
                        <TableCell>{item.Name}</TableCell>
                        <TableCell>{item.DueDate ? item.DueDate.split('T')[0] : "-"}</TableCell>
                        <TableCell>{item.Users && item.Users.length && item.Users.length > 0 ? item.Users.map((user, index) => {
                            let name = user.Name;
                            if (index != item.Users.length - 1) {
                                name += ", ";
                            }
                            return name
                        }) : "-"}</TableCell>
                        <TableCell>{item.Users && item.Users.length && item.Users.length > 0 ? item.Users.map((user, index) => {
                            let CompletedDate = user.CompletedDate ? user.CompletedDate.split('T')[0] : "";
                            if (index != item.Users.length - 1 && CompletedDate) {
                                CompletedDate += ", ";
                            }
                            return CompletedDate
                        }) : "-"}</TableCell>
                        <TableCell>{"-"}</TableCell>
                        <TableCell>{
                            <span className={'statusBtn color-' + this.currentStatusClass(item.Status.Name)}>
                                {item.Status.Name}
                            </span>
                        }</TableCell>
                    </TableRow>
                } else {
                    return ""
                }
            })}
        </TableBody>
    }
    render() {
        return <Fragment>
            {!this.props.showLoaderStatus ? <div>
                <div className="tableTopRow">
                    <div>
                        <span class="bidHeadingText">Tasks</span>
                    </div>
                    <div className="paginationSearch">
                        <ListPagination
                            {...this.props}
                            HidePageDropdown={true}
                            updateProps={this.updateProps}
                            listPageNumber={this.state.filterParams.pageNumber}
                            listTotalCount={this.props.taskList && this.props.taskList.taskList &&
                                this.props.taskList.taskList.TotalCount ? this.props.taskList.taskList.TotalCount : 0}
                            searchParam={this.state.filterParams}
                            listPageSize={this.state.filterParams.pageSize}
                            HidePageDropdown={true}
                        />
                    </div>
                </div>
                <Paper className="alineaTable tinyTableHeaderBg taskListPaper">
                    <div className="tableGridData">
                        <Table id="table" style={{ tableLayout: 'fixed' }}>
                            {this.renderTableHeader()}
                            {this.props.taskList && this.props.taskList.taskList && this.props.taskList.taskList.List ? this.renderTableRows() :
                                ""  // <TableBody id="tableBody" className="ceneteredNoRecordText">
                                //     <div className="noRecordFound">
                                //         <span className="noRecordText">No Record Found</span>
                                //         <img src={RecordnotFound} alt="No Record Found" />
                                //     </div>
                                // </TableBody>
                            }
                        </Table>
                    </div >
                </Paper>
            </div> : <LoaderComponent />}
        </Fragment>
    }
}