import React, { useState } from "react"
import '../common-components.css'
import { ArrowTooltip } from '../common/tooltipComponents'

export const SideMenu = (props) => {
  return <div className="fixedSidePanel">
    <div className="sidePanelBox">
      <ul>
        <ArrowTooltip title="Event Details" placement="right-end">
          <li className={ props.selectedSection === 0 ? 'eventDetailsSelected' : 'eventDetails'} onClick={() => { props.changePage(0) }}>
          </li>
        </ArrowTooltip>
        <ArrowTooltip title="Bid Number" placement="right-end">
          <li className={props.selectedSection && props.selectedSection === 1 ? 'bidNumberSelected' : 'bidNumber'} onClick={() => { props.changePage(1) }}>
          </li>
        </ArrowTooltip>
        <ArrowTooltip title="Batch Assignment" placement="right-end">
          <li className={props.selectedSection && props.selectedSection === 2 ? 'batchSelected' : 'batch'} onClick={() => { props.changePage(2) }}>
          </li>
        </ArrowTooltip>
        <ArrowTooltip title="Tasks" placement="right-end">
          <li className={props.selectedSection && props.selectedSection === 3 ? 'taskSelected' : 'task'} onClick={() => { props.changePage(3) }}>
          </li>
        </ArrowTooltip>
      </ul>
    </div>
  </div>
}