import React, { Component, Fragment } from 'react'
import '../common-components.css'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import MuiDialogContent from '@material-ui/core/DialogContent'
import MuiDialogActions from '@material-ui/core/DialogActions'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
    KeyboardTimePicker
} from '@material-ui/pickers';
import addIcon from '../../assets/add_opportunity.svg';
import { TextField } from '@material-ui/core'
import { addBidForm, validateAddBidForm } from './../../common/const'
import FormHelperText from '@material-ui/core/FormHelperText'

const styles = theme => ({
    formControl: {
        margin: theme.spacing(0.1, 0),
    },
});

const DialogContent = withStyles((theme) => ({ root: {
        padding: theme.spacing(3)
    }
}))(MuiDialogContent)


const DialogTitle = withStyles((theme) => ({ root: {
        padding: '16px 24px !important',
        textTransform: 'uppercase'
    }
}))(MuiDialogTitle)


const DialogActions = withStyles((theme) => ({ root: {
        margin: 0,
        padding: theme.spacing(0, 3, 3, 3),
        justifyContent: 'flex-start'
    }
}))(MuiDialogActions)



class AddBidNumberDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            addbidForm: JSON.parse(JSON.stringify(addBidForm)),
            validateAddBidForm: JSON.parse(JSON.stringify(validateAddBidForm))
        };
    }

    handleSave = () => {
        if( this.state.addbidForm.Number && this.state.addbidForm.Portfolio && this.state.addbidForm.Entity  ){
            this.props.saveNewBid(this.state.addbidForm)
            this.handleClose();
        }else{
            let validateData = this.state.validateAddBidForm;
            validateData.Entity = !this.state.addbidForm.Entity;
            validateData.Number = !this.state.addbidForm.Number;
            validateData.Portfolio = !this.state.addbidForm.Portfolio;
            this.setState({
                validateAddBidForm : validateData
            })
        }
    }

    handleClose = () => {
        this.setState({ 
            open: false,
            addbidForm: JSON.parse(JSON.stringify(addBidForm)),
            validateAddBidForm: JSON.parse(JSON.stringify(validateAddBidForm))
        });
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    }

    handleChange = (event) => {
        let validateData = this.state.validateAddBidForm;
        if(!event.target.value && !validateData[event.target.name]){
            validateData[event.target.name] = true;
        }else{
            validateData[event.target.name] = false;
        }
        let dataUpdate = this.state.addbidForm;
        dataUpdate[event.target.name] = event.target.value;
        this.setState({
            addbidForm: dataUpdate,
            validateAddBidForm : validateData
        })
    }

    render() {
        return (
            <Fragment>
                <span className="topBarListIcon dropListViewIcon">
                    <Button aria-controls="table-actionlist" aria-haspopup="true" onClick={this.handleClickOpen}>
                        <img src={addIcon} alt="report" />
                    </Button>
                </span>
                <Dialog id="addReminderDialog" fullWidth={true} maxWidth={"md"} onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={this.state.open} className="titleDialog">
                    <div className="widthaddReminder">
                        <DialogTitle id="addReminderTitle">
                            <div className="aaddReminderTitleBack">
                                <Typography variant="h6">Add New Bid</Typography>
                            </div>
                        </DialogTitle>
                        <DialogContent className="customScroll">
                            <form>
                                <Grid item sm={12} container className="GridStyle FormStyle">
                                    <Grid item sm={4}>
                                        <TextField
                                            error={this.state.validateAddBidForm.Number}
                                            value={this.state.addbidForm.Number}
                                            name="Number"
                                            type="text"
                                            label="Bidder Number*"
                                            onChange={this.handleChange}
                                        />
                                        {this.state.validateAddBidForm.Number ? (
                                            <FormHelperText id="component-error-text" error>
                                                Bidder Number is required
                                            </FormHelperText>
                                        ) : (
                                                ''
                                            )}
                                    </Grid>
                                    <Grid item sm={4}>
                                        <TextField
                                            error={this.state.validateAddBidForm.Entity}
                                            value={this.state.addbidForm.Entity}
                                            onChange={this.handleChange}
                                            type="text"
                                            label="Purchasing Entity*"
                                            name="Entity"
                                        />
                                        {this.state.validateAddBidForm.Entity ? (
                                            <FormHelperText id="component-error-text" error>
                                               Purchasing Entity is required
                                            </FormHelperText>
                                        ) : (
                                                ''
                                            )}
                                    </Grid>
                                    <Grid item sm={4}>
                                        <TextField
                                             error={this.state.validateAddBidForm.Portfolio}
                                            onChange={this.handleChange}
                                            value={this.state.addbidForm.Portfolio}
                                            type="text"
                                            label="Portfolio*"
                                            name="Portfolio"
                                        />
                                        {this.state.validateAddBidForm.Portfolio ? (
                                            <FormHelperText id="component-error-text" error>
                                               Portfolio is required
                                            </FormHelperText>
                                        ) : (
                                                ''
                                            )}
                                    </Grid>
                                </Grid>
                            </form>
                        </DialogContent>

                        <DialogActions>
                            <Button onClick={this.handleSave} variant="contained" color="primary" className="fab_btn">
                                Create
                         </Button>
                            <Button className="fab_btn" onClick={this.handleClose} >
                                Cancel
                         </Button>
                        </DialogActions>
                    </div>
                </Dialog>
            </Fragment>
        );
    }
}


export default withStyles(styles)(AddBidNumberDialog);