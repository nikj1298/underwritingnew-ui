import React, { Fragment, Component } from "react"
import { BidNumberTopbar } from './bidNumberTopbar'
import { filterParamsBidNumber } from "./../../common/const";
import BidTable from './bidTable'
import LoaderComponent from '../../common/loaderComponent'

let filterParams = JSON.parse(JSON.stringify(filterParamsBidNumber));
let eventIdSelected = ''

export default class BidList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isBidChecked: false,
            showCross: false,
            inputValue: "",
            anchorEl:null
        };
    }

    componentDidMount() {
        filterParams = JSON.parse(JSON.stringify(filterParamsBidNumber));
        this.setState({
            bidingList: [],
            isBidChecked: false ,
            showCross: false,
            inputValue: "",
            anchorEl:null
        }, () => {
            eventIdSelected = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);
            this.loadBidsList(filterParams, false);
        })
    }


    loadBidsList = (filterParamsData, flag) => {
        filterParamsData['EventId'] = eventIdSelected;
        this.props.getBidsListEventsList(filterParamsData).then(res => {
            this.setState({
                bidingList: res && res.data && res.data.List ? res.data.List : [],
                isBidChecked: false
            })
        });
    }

    saveNewBid = (data) => {
        data['EventId'] = eventIdSelected
        this.props.postSaveBidEventsList(data).then(res => {
            this.loadBidsList(filterParams, false);
        });
    }

    updateCall = (data) => {
        this.props.putUpdateBidEventsList(data).then(res => {
            this.loadBidsList(filterParams, false);
        });
    }

    deleteCall = () => {
        let data = []
        this.state.bidingList.map(item => {
            if (item['checked']) {
                data.push(item.Id);
            }
            return item
        })
        if (data.length > 0) {
            this.props.deleteBidEventsList(data).then(res => {
                this.loadBidsList(filterParams, false);
            });
        }
    }

    editBid = (index, flag) => {
        let data = this.state.bidingList;
        data.map((item, indexData) => {
            if (indexData == index) {
                if (flag) {
                    item['selected'] = true;
                } else {
                    item['selected'] = false;
                }
            } else {
                item['selected'] = false;
            }
            return item
        })
        this.setState({
            bidingList: data
        })
    }

    handleChange = (index, event) => {
        let data = this.state.bidingList;
        if (event.target.name == "checked")
            if (data[index][event.target.name])
                data[index][event.target.name] = false
            else
                data[index][event.target.name] = true
        else {
            data[index][event.target.name] = event.target.value;
        }
        let checkedFlag = false;
        data.map(item => {
            if (item['checked']) {
                checkedFlag = true;
            }
            return item
        })
        this.setState({
            bidingList: data,
            isBidChecked: checkedFlag
        })
    }

    handleCancelChange = () => {
        this.setState({
            bidingList: this.props.bidList
        })
    }

    setShowCross = () => {
        this.setState({
            showCross: true
        })
    }

    handleInputValue = (text) => {
        filterParams.FullSearch = text
    }

    clearGlobalSearch = () => {
        filterParams.FullSearch = ""
        this.setState({
            showCross: false,
        })
        this.loadBidsList(filterParams, false);
    }

    searchInputText = () => {
        filterParams.pageNumber = 1;
        this.loadBidsList(filterParams, false);
    }

    updateProps = (updatedSearchParam, pageNumber) => {
        filterParams.pageNumber = pageNumber;
        this.loadBidsList(filterParams, false);
    }

    handleOpen = (event) =>{
        this.setState({
            anchorEl : event.target
        })
    }

    handleCloseMenu = () =>{
        this.setState({
            anchorEl : null
        })
    }

    render() {
        return <Fragment>
            { !this.props.showLoaderStatus ? <div>
                <BidNumberTopbar {...this.props}
                    loadListData={this.loadBidsList}
                    filterParams={filterParams}
                    editBid={this.editBid}
                    list={this.state && this.state.bidingList ? this.state.bidingList : []}
                    itemsChecked={this.state.isBidChecked}
                    saveNewBid={this.saveNewBid}
                    handleChange={this.handleChange}
                    deleteCall={this.deleteCall}
                    showCross={this.state.showCross}
                    setShowCross={this.setShowCross}
                    handleInputValue={this.handleInputValue}
                    clearGlobalSearch={this.clearGlobalSearch}
                    inputValue={filterParams.FullSearch}
                    searchInputText={this.searchInputText}
                    updateProps={this.updateProps}
                    listPageNumber={filterParams.pageNumber}
                    listTotalCount={this.props.bidList && this.props.bidList.bidList &&
                        this.props.bidList.bidList.TotalCount ? this.props.bidList.bidList.TotalCount : 0}
                    searchParam={filterParams}
                    listPageSize={filterParams.pageSize}
                    HidePageDropdown={true}
                    handleOpen={this.handleOpen}
                    handleCloseMenu={this.handleCloseMenu}
                    anchorEl={this.state.anchorEl}
                />
                <BidTable  {...this.props}
                    loadListData={this.loadBidsList} filterParams={filterParams}
                    filterParams={filterParams}
                    list={this.state && this.state.bidingList ? this.state.bidingList : []}
                    editBid={this.editBid}
                    totalCount={this.state && this.state.bidingList ? this.state.bidingList.length : 0}
                    temsChecked={this.state.isBidChecked}
                    saveNewBid={this.saveNewBid}
                    handleChange={this.handleChange}
                    updateCall={this.updateCall}
                    handleCancelChange={this.handleCancelChange}
                    deleteCall={this.deleteCall}
                />
            </div> : <LoaderComponent />}
        </Fragment>
    }
}