import React, { Fragment, Component } from "react"
import { BidNumberTopbar } from './bidNumberTopbar'
import { Typography, TextField } from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import editIcon from './../../assets/editIcon.svg'
import comment_cancel from './../../assets/comment_cancel.svg'
import comment_check from './../../assets/comment_check.svg'
import Input from '@material-ui/core/Input';

export default class BidTable extends Component {

    render() {
        return <Fragment>
            <Paper className="alineaTable cardTable bidNumbersTable">
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell style={{ width: 300 }}>
                                <Checkbox
                                    style={{ padding: 2, marginRight: 15 }}
                                    value="checkedRule"
                                    color="primary"
                                    inputProps={{
                                        'aria-label': 'uncontrolled-checkbox',
                                    }} />Bid Number</TableCell>
                            <TableCell>PURCHASING ENTITY</TableCell>
                            <TableCell>PORTFOLIO</TableCell>
                            <TableCell>ACTIONS</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.list && this.props.list.length > 0 ? this.props.list.map((row, index) => {
                            return <TableRow>
                                <TableCell style={{ width: 300 }}>
                                    <Checkbox
                                        style={{ padding: 2, marginRight: 15 }}
                                        value="checkedRule"
                                        name="checked"
                                        checked={row && row['checked'] ? true : false}
                                        color="primary"
                                        onChange={(event) => { this.props.handleChange(index , event) }}
                                        inputProps={{
                                            'aria-label': 'uncontrolled-checkbox',
                                        }} />
                                     {row.selected ? <Input
                                            className="width100Input"
                                            value={row.Number}
                                            name="Number"
                                            onChange={(event) => { this.props.handleChange(index , event) }}
                                    /> : row.Number }
                                </TableCell>
                                <TableCell>
                                    {row.selected ? <Input
                                        className="width100Input"
                                        value={row.Entity}
                                        name="Entity"
                                        onChange={(event) => { this.props.handleChange(index , event) }}
                                    /> : row.Entity}
                                </TableCell>
                                <TableCell>
                                    {row.selected ? <Input
                                        className="width100Input"
                                        value={row.Portfolio}
                                        name="Portfolio"
                                        onChange={(event) => { this.props.handleChange(index , event) }}
                                    /> : row.Portfolio}
                                </TableCell>
                                <TableCell>
                                    {!row.selected ? <IconButton onClick={() => {
                                        this.props.editBid(index, true);
                                    }}>
                                        <img src={editIcon} />
                                    </IconButton> : ""}
                                    {row.selected ? <IconButton onClick={() => {
                                        this.props.updateCall(row); 
                                        this.props.editBid(index, true);
                                    }}>
                                        <img src={comment_check} />
                                    </IconButton> : ""}
                                    {row.selected ? <IconButton onClick={() => {
                                        this.props.editBid(index, false)
                                    }}>
                                        <img src={comment_cancel} />
                                    </IconButton> : ""}
                                </TableCell>
                            </TableRow>
                        }) : ""
                        }
                    </TableBody>
                </Table>
            </Paper>
        </Fragment>
    }
}