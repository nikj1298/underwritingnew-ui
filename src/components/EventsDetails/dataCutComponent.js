import React, { Component, Fragment } from 'react'
import '../common-components.css'
import { Typography } from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox';
import CancelRule from '../../assets/refresh_circle.svg'
import CampaignAddNewRule from './addNewRuleDialog'
// import CustomSnackbar from '../common/CustomSnackbar'
import { SearchBarComponent } from "../common/searchBarComponent"
import LoaderComponent from '../../common/loaderComponent'

let filteredIds = [], filteredList = [], campaignID = "";
let eventIdSelected = ''

class DataCutComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            alertMessage: "",
            showCross: false,
            inputValue: "",
            showLoader: false,
            updatedRuleList: false,
            checkAllRules : false
        }
    }

    componentDidMount = () => {
        this.setState({
            alertMessage: "",
            showCross: false,
            inputValue: "",
            showLoader: false,
            updatedRuleList: false ,
            rulesList : [] ,
            checkAllRules : false
        },()=>{
            eventIdSelected = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);
            this.props.getRulesAprroveRejectTypes();
            this.loadRulesList();
        })
    }

    loadRulesList = () =>{
        this.props.getAllRulesList(eventIdSelected).then(res=>{
            this.setState({
                rulesList: res.data.List,
            })
        })
    }

    callbackFunction = (childData) => {
        this.setState({ alertMessage: '' })
    }

    saveAppliedRules = (data) =>{
        this.props.postUpdateAppliedRuled(eventIdSelected , data).then( res=>{
            this.loadRulesList();
        })
    }

    showFullDescription = (event, showIndex) => {
        let updatedRuleList = this.state.rulesList;
        updatedRuleList.map((item, index) => {
            if (index === showIndex)
                return updatedRuleList[showIndex] = { ...updatedRuleList[showIndex], ["showFullDescription"]: !updatedRuleList[showIndex].showFullDescription }
        })
        this.setState({ rulesList : updatedRuleList })
    }

    renderRuleDescription = (row, index) => {
        let rulesArr = []
        if (row.DataCutRuleItems && row.DataCutRuleItems.length > 0) {
            row.DataCutRuleItems.forEach(ele => {
                rulesArr.push(ele.DataCutRuleField.Name + " " + ele.DataCutLogicType.Name + " " + ele.Value + " "
                )
            })
        } else rulesArr.push("-")

        return <Fragment>
            {rulesArr.length > 1 ?
                <Fragment>
                    <div className="visibleMoreText">
                        {rulesArr.map( (item , indexing) => {
                            return <span className="visibleTextRow">
                                {indexing == 0 &&  !this.state.rulesList[index].showFullDescription  ?  <span>{item}<Button className='moreEllipses' onClick={(event) => this.showFullDescription(event, index)}>...</Button></span> 
                                : this.state.rulesList[index].showFullDescription  ? <span>{item}{ indexing == rulesArr.length - 1 ? "" : ", " } </span>  : "" } 
                            </span>
                        })}
                    </div>
                </Fragment>
                : rulesArr[0]}
        </Fragment>
    }

    checkRules = (event, row) => {
        let updatedRuleList = this.state.rulesList;
        if (event.target && event.target.value && event.target.value === "checkAllRules") {
            updatedRuleList.map(item => {
                item["IsAttached"] = !this.state.checkAllRules
                return updatedRuleList
            })
            this.setState({
                checkAllRules : !this.state.checkAllRules
            })
        } else {
            updatedRuleList.map(item => {
                if (item.Id === row.Id) {
                    let index = updatedRuleList.indexOf(item)
                    return updatedRuleList[index] = { ...updatedRuleList[index], ["IsAttached"]: !row.IsAttached }
                }
            })
        }
        this.setState({ rulesList : updatedRuleList })
    }

    applyRules = (event, buttonType) => {
        if (buttonType === "CancelBtn") {
            let dataSend  = []
            this.saveAppliedRules(dataSend);
            this.setState({
                checkAllRules : false
            })
        } else {
            let dataSend  = this.state.rulesList.filter(item => item.IsAttached).map(data => data.Id)
            this.saveAppliedRules(dataSend);
        }
    }

    handleInputValue = (value) => {
        this.setState({ inputValue: value  , showCross : value ? true : false })
    }

    setShowCross = (crossState) => {
        this.setState({ showCross: crossState })
    }

    searchInputText = () => {
        // let filterList = [];

        // if (this.state.inputValue.trim() === '') {
        //     this.clearGlobalSearch()
        // } else {
        //     this.props.updatedRuleList.forEach(item => {
        //         if (item.Name !== null) {
        //             if (item.Name.toLowerCase().indexOf(this.state.inputValue.toLowerCase().trim(), 0) >= 0) {
        //                 filterList.push(item)
        //             }
        //         }
        //     })
        //     this.props.updateCampaignRules(filterList)
        // }
    }

    clearGlobalSearch = () => {
        // if (this.props.campaignRules.List && this.props.campaignRules.List.length > 0) {
        //     let ruleList = []
        //     this.props.campaignRules.List.forEach(rule => {
        //         let ruleObj = {
        //             DataCutRuleItems: rule.DataCutRuleItems,
        //             Id: rule.Id,
        //             IsAttached: rule.IsAttached,
        //             Name: rule.Name,
        //             showFullDescription: true
        //         }
        //         ruleList.push(ruleObj)
        //     })
        //     this.props.updateCampaignRules(ruleList)
        // }
        this.setState({
            inputValue: "",
            showCross: false,
        })
    }

    selectRecordActions = () => {
        let cancelList = this.state.rulesList && this.state.rulesList.length > 0 ? 
            this.state.rulesList.filter(rules => rules.IsAttached === true) : [];
        let cancelIds = cancelList.map(rule => rule.Id);

        return <div className="customTopBar w60">
            <div className="tinySearchBar">
                <SearchBarComponent inputValue={this.state.inputValue} showCross={this.state.showCross} handleInputValue={this.handleInputValue}
                    setShowCross={this.setShowCross} searchInputText={this.searchInputText} clearGlobalSearch={this.clearGlobalSearch} />
            </div>
            <div className="searchAction">
                <span className="tinyActionBtn">
                    <Button disabled={cancelIds.length === 0} onClick={event => this.applyRules(event, "CancelBtn")} >
                        Cancel Rule
                     <img alt="Add Contact" src={CancelRule} />
                    </Button>
                </span>
                <CampaignAddNewRule {...this.props} refreshList={this.loadRulesList} />
            </div>
        </div>
    }

    renderRuleList = () => {
        return <Paper className="alineaTable cardTable alignTableDefault maxHeightTable">
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell style={{ width: 300 }}>
                            <Checkbox
                                style={{ padding: 2, marginRight: 15 }}
                                value="checkAllRules"
                                color="primary"
                                inputProps={{
                                    'aria-label': 'uncontrolled-checkbox',
                                }}
                                checked={this.state.checkAllRules}
                                onClick={event => this.checkRules(event)}
                            />
                            Rule Name
                        </TableCell>
                        <TableCell>Decription</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {this.state.rulesList && this.state.rulesList.length > 0 ? this.state.rulesList.map((row, index) => (
                        <TableRow key={row.name} className={this.state.inputValue && row.Name && 
                        row.Name.toLowerCase().includes(this.state.inputValue.toLowerCase()) ? "" : this.state.inputValue ? 
                            "hiddenElement" : ""
                        }>
                            <TableCell style={{ width: 300 }}>
                                <Checkbox
                                    style={{ padding: 2, marginRight: 15 }}
                                    checked={row.IsAttached}
                                    value="checkedRule"
                                    color="primary"
                                    inputProps={{
                                        'aria-label': 'uncontrolled-checkbox',
                                    }}
                                    onClick={event => this.checkRules(event, row)}
                                />
                                {row.Name}
                            </TableCell>
                            <TableCell>
                                {this.renderRuleDescription(row, index)}
                            </TableCell>
                        </TableRow> 
                    )) : "" }
                </TableBody>
            </Table>
        </Paper>
    }

    render() {
        return (
            <Fragment>
                { !this.props.showLoaderStatus ?
                    <div style={{ paddingTop: 10 }} className="DataCutComponent">
                        {this.selectRecordActions()}
                        {this.renderRuleList()}
                        <div className="mt-2">
                            <Button variant="contained" color="primary" className="fab_btn" onClick={event => this.applyRules(event, "ApplyBtn")} disabled={this.props.filteredIdsList && this.props.filteredIdsList.length === 0}>Apply Rules</Button>
                        </div>
                    </div>
                    : < LoaderComponent />
                }
                {/* {this.state.alertMessage !== '' ? <CustomSnackbar message={this.state.alertMessage} parentCallback={this.callbackFunction} /> : ''} */}
            </Fragment>
        );
    }
}


export default DataCutComponent;