import React, { Fragment, Component } from "react"
import { Button, Grid, TextField, Typography, makeStyles } from '@material-ui/core'
import ListIcon from '../../assets/more.svg'
import LockScreen from '../../assets/lockBrown.svg'
import Paper from '@material-ui/core/Paper'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import InputAdornment from '@material-ui/core/InputAdornment'
import DataCutComponent from './dataCutComponent'

const useStyles = makeStyles(theme => ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        color: '#815739',
    }
}));

export default class EventDetailsFormComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            open: false,
        };
    }

    handleClickOpen = (event) => {
        this.setState({ open: true });
    }

    render() {
        return <Fragment>
            <Typography component="div" className="flexView-Heading">
                <Typography className="formHeading">Event Details</Typography>
                <span>
                    <Button variant="contained" color="primary" className="fab_btn">Edit</Button>
                    <span className="dropListViewIcon" style={{ marginLeft: 15 }}>
                        <Button aria-haspopup="true" onClick={this.handleClickOpen}>
                            <img src={LockScreen} alt="lock" />
                        </Button>
                        <Button id="settingList" aria-controls="settinglist" aria-haspopup="true" onClick={this.handleClickOpen}>
                            <img src={ListIcon} alt="listview" style={{ transform: ' scale(1.01)' }} />
                        </Button>
                    </span>
                </span>
            </Typography>

            <Paper className="paperCard">
                <div className="paperContent">
                    <div className="expansionPanel">
                        <ExpansionPanel>
                            <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="applydata-content"
                                id="applydata-header"
                            >
                                <Typography>Apply Data Cut</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                                <div className="expansionPanelContent">
                                    <DataCutComponent {...this.props}/>
                                </div>
                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                        <ExpansionPanel>
                            <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="details-content"
                                id="details-header"
                            >
                                <Typography>Details</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                                <div className="expansionPanelContent">
                                    <Paper className="paperCard panelLightGreen" style={{ marginTop: 15 }}>
                                        <div className="paperContent">
                                            <Grid item sm={12} container alignItems="center" className="GridStyle FormStyle gridRowCol">
                                                {/* <Grid item className="gridCol5">
                                                    <TextField
                                                        id="standard-read-only-input"
                                                        label="Original List Count"
                                                        value="0"
                                                        className={useStyles.textField}
                                                        margin="normal"
                                                        InputProps={{
                                                            readOnly: true,
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item className="gridCol5">
                                                    <TextField
                                                        id="standard-read-only-input"
                                                        label="Original List Amount"
                                                        value="0.00"
                                                        className={useStyles.textField}
                                                        margin="normal"
                                                        InputProps={{
                                                            readOnly: true,
                                                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item className="gridCol5">
                                                    <TextField
                                                        id="standard-read-only-input"
                                                        label="Pre Lim List Count"
                                                        value="0"
                                                        className={useStyles.textField}
                                                        margin="normal"
                                                        InputProps={{
                                                            readOnly: true,
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item className="gridCol5">
                                                    <TextField
                                                        id="standard-read-only-input"
                                                        label="Pre Lim List Amount"
                                                        value="0.00"
                                                        className={useStyles.textField}
                                                        margin="normal"
                                                        InputProps={{
                                                            readOnly: true,
                                                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item className="gridCol5">
                                                    <TextField
                                                        id="standard-read-only-input"
                                                        label="Approved Lien Count"
                                                        value="0"
                                                        className={useStyles.textField}
                                                        margin="normal"
                                                        InputProps={{
                                                            readOnly: true,
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item className="gridCol5">
                                                    <TextField
                                                        id="standard-read-only-input"
                                                        label="Approved Purchase Amount"
                                                        value="0.00"
                                                        className={useStyles.textField}
                                                        margin="normal"
                                                        InputProps={{
                                                            readOnly: true,
                                                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item className="gridCol5">
                                                    <TextField
                                                        id="standard-read-only-input"
                                                        label="Final Purchase Count"
                                                        value="0"
                                                        className={useStyles.textField}
                                                        margin="normal"
                                                        InputProps={{
                                                            readOnly: true,
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item className="gridCol5">
                                                    <TextField
                                                        id="standard-read-only-input"
                                                        label="Final Purchase Amount"
                                                        value="0.00"
                                                        className={useStyles.textField}
                                                        margin="normal"
                                                        InputProps={{
                                                            readOnly: true,
                                                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                                                        }}
                                                    />
                                                </Grid> */}
                                            </Grid>
                                        </div>
                                    </Paper>

                                </div>
                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                    </div>
                </div>
            </Paper>

        </Fragment>
    }
}