import React, { useState } from 'react'
import { SearchBarComponent } from "../common/searchBarComponent"

export const ReviewSearchBar = (props) => {
  const { getPropertiesParam } = props
  const [showCross, setShowCross] = useState(false)
  const [inputValue, handleInputValue] = useState('')

	/**
	 * handles search filter on hit of Enter key
	 */
  const searchInputText = () => {
    getPropertiesParam.FullSearch = inputValue
    getPropertiesParam.offset = 0
    props.updateGetPropertyListParam(getPropertiesParam)
    props.updateEventPropertyPageNumber(1)
    props.fetchEventProperties(getPropertiesParam)
  }
	/**
   * cleares all search query on click of cross icon on search bar
   */
  const clearGlobalSearch = () => {
    handleInputValue('')
    getPropertiesParam.FullSearch = ''
    getPropertiesParam.offset = 0
    props.fetchEventProperties(getPropertiesParam)
    props.updateEventPropertyPageNumber(1)
    setShowCross(false)
  }

  return (
    <SearchBarComponent listTotalCount={props.listTotalCount} inputValue={inputValue} showCross={showCross} handleInputValue={handleInputValue}
      setShowCross={setShowCross} searchInputText={searchInputText} clearGlobalSearch={clearGlobalSearch} />
  )
}
