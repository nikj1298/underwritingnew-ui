import React, { Fragment, Component } from "react"
import Header from '../../pages/header'
import { Scrollbars } from 'react-custom-scrollbars'
import { pageSize, filterPropertiesParam } from "../../common/const"
import { ReviewTopBar } from "./reviewTopBar"
import { ListPagination } from "../common/listPagination"
import { ReviewPropertyTableComponent } from "./reviewPropertyTableComponent"
let eventId = undefined
export default class ReviewListComponent extends Component {

  componentDidMount = () => {
    eventId = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1)
    const searchParam = { ...filterPropertiesParam }
    searchParam.Filter.EventId = eventId
    this.props.updateGetPropertyListParam(searchParam)
    this.props.updateEventPropertyPageNumber(1)
    this.props.fetchEventProperties(searchParam)
    this.props.getAllEventsList(this.props.allEventParam)
    this.props.fetchAllLevels(eventId)
    this.props.getAllGeneralLandUseCode()
    this.props.getAllInternalLandUseCode()
  }
  /**
	 * common function to be called while handling page movement in pagination
	 */

  updateAllProps = (searchParam, pageNumber) => {
    this.props.updateEventPropertyPageNumber(pageNumber)
    this.props.updateGetPropertyListParam(searchParam)
    this.props.fetchEventProperties(searchParam)
  }
  render() {
    return <Fragment>
      <Header />
      <div id="loanlist">
        <Fragment>
          <div className="container-fluid">
            <ReviewTopBar listTotalCount={this.props.eventPropertyCount} updateProps={this.updateAllProps} {...this.props} />
            <Scrollbars
              autoHeight
              autoHeightMin={'100%'}
              autoHeightMax={'100%'}
              renderTrackHorizontal={(props) => <div {...props} className="track-horizontal" />}
              thumbSize={300}>
              <ReviewPropertyTableComponent searchParam={this.props.getPropertiesParam}
                updateProps={this.updateAllProps} {...this.props} />
            </Scrollbars>
            <div className="bottomPagination">
              <ListPagination listPageNumber={this.props.eventPropertyPageNumber} listTotalCount={this.props.eventPropertyCount}
                searchParam={this.props.getPropertiesParam} listPageSize={pageSize} updateProps={this.updateAllProps} />
            </div>
          </div>
        </Fragment>
      </div>
    </Fragment>
  }
}