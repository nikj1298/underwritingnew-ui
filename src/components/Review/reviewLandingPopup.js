import React, { Component, Fragment } from "react"
import '../common-components.css'
import {
  Button, FormHelperText, Dialog, DialogActions, DialogContent,
  DialogTitle, Grid, FormControl, InputLabel
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { DropdownComponent } from "../common/dropdownComponent"
import { eventStatusMenu, assignmentDropMenu, reviewPopupUrl } from "../../common/const"
import LoaderComponent from "../common/loaderComponent"
import Header from '../../pages/header'
import { DropDownMenu } from "../common/dropDownMenu"
import { NoRecordFound } from "../common/noRecordFound"

const styles = theme => ({
  formControl: {
    margin: theme.spacing(0.1, 0),
  },
});

class ReviewLandingPopup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: true,
      requiredEventId: false
    };
  }

  componentDidMount = () => {
    const eventParamNew = { ...this.props.allEventParam }
    if (this.props.lastVisitedUrl && !this.props.lastVisitedUrl.includes(`${reviewPopupUrl}/event/`)) {
      eventParamNew.Filter.IsLocked = 0
      this.props.getAllEventsList(eventParamNew)
      const propertyParamNew = { ...this.props.getPropertiesParam }
      propertyParamNew.Filter["EventId"] = null
      propertyParamNew.Filter["AssignmentByUser"] = true
      this.props.updateGetPropertyListParam(propertyParamNew)
    } else {
      this.props.getAllEventsList(this.props.allEventParam)
    }
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  }
  handleClose = () => {
    if (this.props.lastVisitedUrl) {
      this.props.history.push(this.props.lastVisitedUrl)
    }
    this.setState({ open: false });
  }


  /**
   * handles change of every drop-down
   */
  handleChange = ({ target }) => {
    const { name, value } = target
    const eventParamNew = { ...this.props.allEventParam }
    const propertyParamNew = { ...this.props.getPropertiesParam }
    if (name === "IsLocked") {
      eventParamNew.Filter[name] = value
      propertyParamNew.Filter["EventId"] = null
      this.props.updateGetPropertyListParam(propertyParamNew)
      this.props.updateGetAllEventsParam(eventParamNew)
      this.props.getAllEventsList(eventParamNew)
      this.setState({
        requiredEventId: true
      })
    } else {
      propertyParamNew.Filter[name] = value
      this.setState({
        requiredEventId: false
      })
      this.props.updateGetPropertyListParam(propertyParamNew)
    }
  }

  /**
   * shows the properties based on the filter selected in pop-up
   */
  handleViewProperties = () => {
    if (!this.props.getPropertiesParam.Filter.EventId) {
      this.setState({
        requiredEventId: true
      })
      return
    }
    else {
      this.props.fetchEventProperties(this.props.getPropertiesParam)
      this.setState({
        requiredEventId: false,
        open: false
      })
      if (this.props.getPropertiesParam.Filter.EventId)
        this.props.history.push(`${reviewPopupUrl}/event/${this.props.getPropertiesParam.Filter.EventId}`)
    }
  }

  /**
   * renders the dialog action buttons
   */
  renderDialogActions = () => {
    return <DialogActions className="contentFlexStart">
      <Button onClick={this.handleViewProperties} variant="contained" color="primary" className="fab_btn">
        {"View Event"}
      </Button>
      <Button className="fab_btn" onClick={this.handleClose}
      >
        {"Cancel"}
      </Button>
    </DialogActions>
  }

  /**
   * render drop-downs in dialog
   */
  renderDropDown = () => {
    const { classes, getAllEvents, getPropertiesParam, allEventParam } = this.props;
    return <Grid item sm={12} container className="GridStyle GridMargin FormStyle">
      <Grid item sm={12}>
        <div className="mandatoryField">
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="record-helper">Event</InputLabel>
            <DropdownComponent name={"EventId"} valueData={getPropertiesParam.Filter.EventId}
              label={"Event"} dropDownList={getAllEvents.getAllEvents}
              handleChange={this.handleChange} validateData={this.state.requiredEventId} />
            {this.state.requiredEventId ?
              <FormHelperText id="component-error-text" error>
                {"Please select any event."}
              </FormHelperText> : null}
          </FormControl>
        </div>
      </Grid>
      <Grid item sm={6}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="record-helper">Event Status</InputLabel>
          <DropdownComponent name={"IsLocked"} valueData={allEventParam.Filter.IsLocked}
            label={"Event Status"} dropDownList={eventStatusMenu}
            handleChange={this.handleChange} />
        </FormControl>
      </Grid>
      <Grid item sm={6}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="record-helper">Assignments</InputLabel>
          <DropdownComponent name={"AssignmentByUser"} valueData={getPropertiesParam.Filter.AssignmentByUser}
            label={"Assignments"} dropDownList={assignmentDropMenu} disabled={!getPropertiesParam.Filter.EventId}
            handleChange={this.handleChange} />
        </FormControl>
      </Grid>
    </Grid>
  }

  /**
   * render dialog on landing over review from drop-menu
   */
  renderDialog = () => {
    return <Dialog
      id="deleteCommentsDialog"
      onClose={this.handleClose}
      open={this.state.open}
      fullWidth={true}
      maxWidth={'sm'}>
      <div className="widthaddReminder rv-textContentLeft">
        <DialogTitle id="addReminderTitle" className="reviewTitleBg">
          <div className="aaddReminderTitleBack" />
        </DialogTitle>
        <DialogContent>
          {this.renderDropDown()}
        </DialogContent>
        {this.renderDialogActions()}
      </div>
    </Dialog>
  }

  render() {
    const { getAllEvents } = this.props
    return (
      <Fragment>
        <Header />
        <div id="loanlist">
          <div className="container-fluid">
            <div className="tableTopRow">
              <div>
                <DropDownMenu menuName="Review" {...this.props} />
              </div>
            </div>
          </div>
        </div>
        {getAllEvents && getAllEvents.getAllEvents ? this.renderDialog() : <LoaderComponent />}
        {!this.props.getPropertiesParam.Filter.EventId ? <NoRecordFound /> : null}
      </Fragment>
    );
  }
}

export default withStyles(styles)(ReviewLandingPopup);