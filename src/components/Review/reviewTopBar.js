import React from "react"
import { DropDownMenu } from "../common/dropDownMenu"
import { ReviewSearchBar } from "./reviewSearchBar"
import { ListPagination } from "../common/listPagination"
import { pageSize } from "../../common/const"
import ReviewAdvanceFilter from "./reviewAdvanceFilter"

export const ReviewTopBar = (props) => {
  const { eventPropertyPageNumber, eventPropertyCount, getPropertiesParam } = props
  return <div className="tableTopRow">
    <div>
      <DropDownMenu menuName="Review" {...props} />
    </div>
    <div className="paginationSearch">
      <ReviewSearchBar {...props} />
      <ReviewAdvanceFilter {...props} />
      <ListPagination listPageNumber={eventPropertyPageNumber} listTotalCount={eventPropertyCount}
        searchParam={getPropertiesParam} listPageSize={pageSize} updateProps={props.updateProps} />
    </div>
  </div>
}