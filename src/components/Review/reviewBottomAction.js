import React, { Fragment, Component } from "react"
import {
    Button, Typography, Icon,
    IconButton, Popover
} from '@material-ui/core'
import EscapeOutside from "react-escape-outside"
import AncientIcon from '../../assets/ancient-scroll.svg'
import BrainIcon from '../../assets/brain.svg'
import ChatIcon from '../../assets/chat.svg'
import ManagerIcon from '../../assets/manager.svg'
import StarIcon from '@material-ui/icons/Star';

class ReviewerActionPopover extends Component {
    constructor() {
        super()
        this.state = {
            isOpen: true,
            isOpenSecond: false
        }
    }

    handleEscapeOutside = () => {
        this.setState({ isOpen: false })
        this.setState({ isOpenSecond: false })
    }
    handleFisrtOnClick = () => {
        this.setState({ isOpen: true })
    }
    handleSecondOnClick = () => {
        this.setState({ isOpenSecond: true })
    }

    render() {
        return (
            <Fragment>
                <div>
                    {this.state.isOpen && (
                        <Fragment>
                            <div className="escapePopoverOverlay"></div>
                            <EscapeOutside className="escapePopover"
                                onEscapeOutside={this.handleEscapeOutside}
                            >
                                <IconButton className="filterClose" aria-label="close" size="small" onClick={this.handleEscapeOutside}>
                                    <Icon>close</Icon>
                                </IconButton>
                                <div className="escapePopoverContent">
                                    Hide me by clicking outside me or by pressing the Escape key.
                            </div>
                            </EscapeOutside>
                        </Fragment>
                    )}

                    {this.state.isOpenSecond && (
                        <Fragment>
                            <div className="escapePopoverOverlay"></div>
                            <EscapeOutside className="escapePopover"
                                onEscapeOutside={this.handleEscapeOutside}
                            >
                                <IconButton className="filterClose" aria-label="close" size="small" onClick={this.handleEscapeOutside}>
                                    <Icon>close</Icon>
                                </IconButton>
                                <div className="escapePopoverContent">
                                    Hide me by clicking outside me <br></br>
                                </div>
                            </EscapeOutside>
                        </Fragment>
                    )}
                </div>
                <div className="rv-bottomReview">
                    <div className="rv-popoverActionRow">
                        <span className="rv-popoverOpenBtn">
                            <Button color="primary" onClick={this.handleFisrtOnClick}>
                                <img src={ManagerIcon} />
                            </Button>
                            <Button color="primary" onClick={this.handleSecondOnClick}>
                                <img src={ChatIcon} />
                            </Button>
                            <Button color="primary" onClick={this.handleThirdOnClick}>
                                <img src={AncientIcon} />
                            </Button>
                            <Button color="primary" onClick={this.handleForthOnClick}>
                                <img src={BrainIcon} />
                            </Button>
                        </span>
                        <span className="reviewStar">
                            <span className="activeStar"><StarIcon /></span>
                            <span className="activeStar"><StarIcon /></span>
                            <span className="activeStar"><StarIcon /></span>
                            <span><StarIcon /></span>
                            <span><StarIcon /></span>
                        </span>
                        <span className="reviewDateText">
                            <Typography variant="span">Batch 07/59</Typography>
                        </span>
                    </div>
                    <div className="rv-popoverActionRow">
                        <div className="rv-actionBtn">
                            <Button variant="contained" color="primary" className="fab_btn">Approve</Button>
                            <Button variant="contained" color="primary" className="fab_btn">Reject</Button>
                            <Button variant="contained" color="primary" className="fab_btn">Research</Button>
                        </div>
                    </div>
                </div>

            </Fragment>
        )
    }
}

export default ReviewerActionPopover;