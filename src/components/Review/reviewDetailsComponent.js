import React, { Fragment, Component } from "react"
import ReviewHeader from '../../pages/reviewHeader'
import '../common-components.css'
import Grid from '@material-ui/core/Grid';
import { Link, Button } from "@material-ui/core";
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MoneyBagIcon from '../../assets/money-bag.svg'
import BanknoteIcon from '../../assets/banknote.svg'
import CalenderIcon from '../../assets/small-calendar.svg'
import CheckedIcon from '../../assets/small-calendar-2.svg'
import HistoryIcon from '../../assets/history.svg'
import MapScreen from '../../assets/ScreenShot.png'
import ReviewerActionPopover from './reviewBottomAction'


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            <Box p={0}>{children}</Box>
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

class ReviewDetailsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            value: 0,
        };
    }

    handleChange = (event, newValue) => {
        this.setState({ value: newValue });
    };
    // handleClickOpen = () => {
    //     this.setState({ open: true });
    // }
    // handleClose = () => {
    //     this.setState({ open: false });
    // }

    handleClickOpen = event => {
        this.setState({ anchorEl: event.currentTarget })
    };

    render() {
        return (
            <Fragment>
                {/* <ReviewHeader /> */}

                <div className="container-fluid">

                    <div className="reviewContainer">

                        <div className="reviewPanelLeft">
                            <div className="googleLinkTab">
                                <Typography variant="span" className="g-hdgText">GOOGLE (Lat/Lon)</Typography>
                                <Link href="#">Google</Link>
                                <Link href="#">Google Owner</Link>
                                <Link href="#">Zillow</Link>
                                <Link href="#">Treasurer</Link>
                                <Link href="#">Assessor</Link>
                            </div>
                            <div className="googleMapPanel">
                                <img src={MapScreen} alt="map" />
                            </div>
                        </div>

                        <div className="reviewPanelRight">
                            <div className="topDetailsPanel">
                                <div className="rv-topGridRow">
                                    <Grid item sm={12} container className="GridStyle rv-topGridCol">
                                        <Grid item>
                                            <div className="rv-subHeadingtext">
                                                <span className="rv-iconCol">
                                                    <img src={MoneyBagIcon} alt="icon" />
                                                </span>
                                                <div className="rv-textCol">
                                                    <div>Tax Amount</div>
                                                    <div className="rv-subHeadingDatatext">
                                                        $ 1,23,009
                                             </div>
                                                </div>
                                            </div>
                                        </Grid>
                                        <Grid item>
                                            <div className="rv-subHeadingtext">
                                                <span className="rv-iconCol">
                                                    <img src={BanknoteIcon} alt="icon" />
                                                </span>
                                                <div className="rv-textCol">
                                                    <div>Land Value</div>
                                                    <div className="rv-subHeadingDatatext">
                                                        $ 1,23,009
                                                 </div>
                                                </div>
                                            </div>
                                        </Grid>
                                        <Grid item>
                                            <div className="rv-subHeadingtext">
                                                <span className="rv-iconCol">
                                                    <img src={CalenderIcon} alt="icon" />
                                                </span>
                                                <div className="rv-textCol">
                                                    <div>Improvement Value</div>
                                                    <div className="rv-subHeadingDatatext">
                                                        $ 1,23,009
                                                </div>
                                                </div>
                                            </div>
                                        </Grid>
                                        <Grid item>
                                            <div className="rv-subHeadingtext">
                                                <span className="rv-iconCol">
                                                    <img src={CheckedIcon} alt="icon" />
                                                </span>
                                                <div className="rv-textCol">
                                                    <div>Market Value</div>
                                                    <div className="rv-subHeadingDatatext">
                                                        $ 1,23,009
                                                 </div>
                                                </div>
                                            </div>
                                        </Grid>
                                    </Grid>
                                </div>
                                <div className="reviewTopTab">
                                    <Tabs value={this.state.value} onChange={this.handleChange} aria-label="simple tabs example">
                                        <Tab label="Details" {...a11yProps(0)} />
                                        <Tab label="More Information" {...a11yProps(1)} />
                                        <Tab label="Attachment" {...a11yProps(2)} />
                                    </Tabs>
                                </div>
                                <span className="reviewHistoryIcon">
                                    <Button>
                                        <img src={HistoryIcon} alt="history" />
                                    </Button>
                                </span>
                            </div>
                            <div className="rv-tabContent">
                                <TabPanel value={this.state.value} index={0}>
                                    Details
                            </TabPanel>
                                <TabPanel value={this.state.value} index={1}>
                                    More Information
                            </TabPanel>
                                <TabPanel value={this.state.value} index={2}>
                                    Attachment
                            </TabPanel>
                            </div>

                            <ReviewerActionPopover />


                        </div>
                    </div>

                </div>
            </Fragment>
        );
    }
}
export default ReviewDetailsComponent;