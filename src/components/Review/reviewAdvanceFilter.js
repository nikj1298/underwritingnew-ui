import React, { Component } from 'react'
import '../common-components.css'
import { Typography } from '@material-ui/core';
import { ReviewFilterTab } from "./reviewFilterTabs"
import { FilterActionButtons, FilterPopupHeader } from "../common/filterComponent"
import { reviewPropertyAdvanceFilter } from "../../common/const"
import { ReviewFilterDataTabs } from "./reviewFilterDataTabs"
import { AdvanceFilterDialogPopup } from "../common/advanceFilterDialogPopup"

export default class ReviewAdvanceFilter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: 0,
      searchFilter: { ...reviewPropertyAdvanceFilter }
    }
  }

  handleChange = (event, newValue) => {
    this.setState({ value: newValue });
  }

  /**
   * re-assigns the filter state to last applied filter
   */
  resetFilterLastApplied = (e, popupState) => {
    popupState.close()
    this.setState({
      value: 0
    })
  }
  /**
   * common things to be handled in case of apply and clear filter
   */
  applyOrClearFilter = (event, popupState, actionName) => {
    popupState.close()
    this.setState({
      value: 0
    })
  }
  /**
   * maintains the local state of applied filters
   */
  handlesChangeState = (e, value, name) => {
    // const filterState = { ...this.state.searchFilter }
    // filterState[name] = value
    // this.setState({
    //   searchFilter: filterState
    // })
  }
  /**
   * render the complete filter dialog
   */
  renderFilterDialog = (popupState) => {
    return <div className="filterDialogInner">
      <div className="verTabsRoot">
        <Typography component="div" className="verContentBox">
          <ReviewFilterDataTabs searchFilter={this.state.searchFilter} value={this.state.value} {...this.props} />
        </Typography>
        <Typography component="div" className="verTabBox">
          <FilterPopupHeader popupState={popupState} resetFilterLastApplied={this.resetFilterLastApplied} />
          <ReviewFilterTab handleChange={this.handleChange} value={this.state.value} {...this.props} />
          <FilterActionButtons popupState={popupState} applyOrClearFilter={this.applyOrClearFilter} />
        </Typography>
      </div>
    </div>
  }
  render() {
    return (
      <AdvanceFilterDialogPopup renderDialog={this.renderFilterDialog} />
    );
  }
}