import React, { Fragment } from "react"
import { Grid, TextField, List, ListItem, ListItemText } from "@material-ui/core"
import { TabPanel } from "../common/tabPanelComponent"
import { decisionDropDownMenu, levelReviewStatus } from "../../common/const"
import { Scrollbars } from 'react-custom-scrollbars';
import InputAdornment from '@material-ui/core/InputAdornment';

export const ReviewFilterDataTabs = (props) => {

  /**
   * render level review list in level tab
   */
  const renderReviewLevelList = () => {
    return <TabPanel value={props.value} index={0}>
      <Scrollbars
        style={{ height: 400 }}
        renderTrackHorizontal={propsTab => <div {...propsTab} className="track-horizontal" style={{ overflow: 'hidden' }} />}
        renderTrackVertical={propsTab => <div {...propsTab} className="track-vertical" />}
      >
        <div className="f-listHeadingText">
          {"Review Status"}
        </div>
        <div className="contentInner">
          <div className="filter-ul-list">
            <List name="Type">
              {levelReviewStatus.map((status) => {
                return <ListItem
                  className={props.searchFilter.LevelFilters[0].ReviewId === status.Id ? "activeList" : ""}
                  key={status.Id} button
                  // onClick={(e) => this.handlesChangeState(e, status.Id, "Type")}
                  selected={props.searchFilter.LevelFilters[0].ReviewId === status.Id} >
                  <ListItemText primary={status.Name} />
                </ListItem>
              })}
            </List>
          </div>
        </div>
      </Scrollbars>
    </TabPanel>
  }

  /**
 * renders decision type tab list
 */
  const renderDecisionsTab = () => {
    return <TabPanel value={props.value} index={1}>
      <Scrollbars
        style={{ height: 400 }}
        renderTrackHorizontal={propsTab => <div {...propsTab} className="track-horizontal" style={{ overflow: 'hidden' }} />}
        renderTrackVertical={propsTab => <div {...propsTab} className="track-vertical" />}
      >
        <div className="f-listHeadingText">
          {"Decisions"}
        </div>
        <div className="contentInner">
          <div className="filter-ul-list">
            <List name="Type">
              {decisionDropDownMenu.map((decision) => {
                return <ListItem
                  className={props.searchFilter.ReviewDecision === decision.Id ? "activeList" : ""}
                  key={decision.Id} button
                  // onClick={(e) => this.handlesChangeState(e, decision.Id, "Type")}
                  selected={props.searchFilter.ReviewDecision === decision.Id} >
                  <ListItemText primary={decision.Name} />
                </ListItem>
              })}
            </List>
          </div>
        </div>
      </Scrollbars>
    </TabPanel>
  }
  /**
   * render Parcel Id Text Field
   */
  const renderParcelIdTab = () => {
    return <TabPanel value={props.value} index={2}>
      <Grid item sm={12} container className="GridStyle FormStyle">
        <Grid item sm={12}>
          <TextField
            id="parcelId"
            value=""
            label="Parcel ID"
            name="ParcelId"
          />
        </Grid>
      </Grid>
    </TabPanel>
  }
  /**
     * render Owner Text Field
     */
  const renderOwnerTab = () => {
    return <TabPanel value={props.value} index={3}>
      <Grid item sm={12} container className="GridStyle FormStyle">
        <Grid item sm={12}>
          <TextField
            id="owner"
            value=""
            label="Owner"
            name="Owner"
          />
        </Grid>
      </Grid>
    </TabPanel>
  }

  /**
   * render Property Address Text Field
   */
  const renderPropertyAddressTab = () => {
    return <TabPanel value={props.value} index={4}>
      <Grid item sm={12} container className="GridStyle FormStyle">
        <Grid item sm={12}>
          <TextField
            id="propertyAddress"
            value=""
            label="Property Address"
            name="PropertyAddress"
          />
        </Grid>
      </Grid>
    </TabPanel>
  }
  /**
  * render Property City Text Field
  */
  const renderPropertyCityTab = () => {
    return <TabPanel value={props.value} index={5}>
      <Grid item sm={12} container className="GridStyle FormStyle">
        <Grid item sm={12}>
          <TextField
            id="propertyCity"
            value=""
            label="Property City"
            name="PropertyCity"
          />
        </Grid>
      </Grid>
    </TabPanel>
  }
  /**
  * render Property Zip Code Text Field
  */
  const renderPropertyZipCodeTab = () => {
    return <TabPanel value={props.value} index={6}>
      <Grid item sm={12} container className="GridStyle FormStyle">
        <Grid item sm={12}>
          <TextField
            id="propertyZipCode"
            value=""
            label="Property Zip Code"
            name="PropertyZipCode"
          />
        </Grid>
      </Grid>
    </TabPanel>
  }
  /**
  * render Land Use Code Text Field
  */
  const renderLandUseCodeTab = () => {
    return <TabPanel value={props.value} index={7}>
      <Grid item sm={12} container className="GridStyle FormStyle">
        <Grid item sm={12}>
          <TextField
            id="landUseCode"
            value=""
            label="Land Use Code"
            name="LandUseCode"
          />
        </Grid>
      </Grid>
    </TabPanel>
  }

  /**
   * render general land use code
   */
  const renderGeneralLandUseCode = () => {
    return <TabPanel value={props.value} index={9}>
      <Scrollbars
        style={{ height: 400 }}
        renderTrackHorizontal={propsTab => <div {...propsTab} className="track-horizontal" style={{ overflow: 'hidden' }} />}
        renderTrackVertical={propsTab => <div {...propsTab} className="track-vertical" />}
      >
        <div className="f-listHeadingText">
          {"Decisions"}
        </div>
        <div className="contentInner">
          <div className="filter-ul-list">
            <List name="Type">
              {props.allGeneralLandUseCode.map((decision) => {
                return <ListItem
                  // className={props.searchFilter.ReviewDecision === decision.Id ? "activeList" : ""}
                  key={decision.Id} button
                // onClick={(e) => this.handlesChangeState(e, decision.Id, "Type")}
                // selected={props.searchFilter.ReviewDecision === decision.Id}
                >
                  <ListItemText primary={decision.Name} />
                </ListItem>
              })}
            </List>
          </div>
        </div>
      </Scrollbars>
    </TabPanel>
  }
  /**
  * render internal land use code
  */
  const renderInternalLandUseCode = () => {
    return <TabPanel value={props.value} index={8}>
      <Scrollbars
        style={{ height: 400 }}
        renderTrackHorizontal={propsTab => <div {...propsTab} className="track-horizontal" style={{ overflow: 'hidden' }} />}
        renderTrackVertical={propsTab => <div {...propsTab} className="track-vertical" />}
      >
        <div className="f-listHeadingText">
          {"Decisions"}
        </div>
        <div className="contentInner">
          <div className="filter-ul-list">
            <List name="Type">
              {props.allInternalLandUseCode.map((internalCode) => {
                return <ListItem
                  // className={props.searchFilter.ReviewDecision === decision.Id ? "activeList" : ""}
                  key={internalCode.Id} button
                // onClick={(e) => this.handlesChangeState(e, decision.Id, "Type")}
                // selected={props.searchFilter.ReviewDecision === decision.Id}
                >
                  <ListItemText primary={internalCode.Name} />
                </ListItem>
              })}
            </List>
          </div>
        </div>
      </Scrollbars>
    </TabPanel>
  }

  /**
   * render currency type text field
   */
  const renderCurrencyTypeField = (fieldIdentifier) => {
    const { id, value, label, name } = fieldIdentifier
    return <Grid item sm={6}>
      <TextField
        id={id}
        value={value}
        label={label}
        name={name}
        InputProps={{
          startAdornment: <InputAdornment position="start">$</InputAdornment>,
        }}
      />
    </Grid>
  }
  /**
   * render Assessed value text field
   */
  const renderAssessedValue = () => {
    return <TabPanel value={props.value} index={10}>
      <Grid item sm={12} container className="GridStyle FormStyle">
        {renderCurrencyTypeField({ id: "minAssessedValue", value: "", label: "Min", name: "MinAssessedValue" })}
        {renderCurrencyTypeField({ id: "maxAssessedValue", value: "", label: "Max", name: "MaxAssessedValue" })}
      </Grid>
    </TabPanel>
  }

  /**
  * render Amoutn Due value text field
  */
  const renderAmountDueValue = () => {
    return <TabPanel value={props.value} index={11}>
      <Grid item sm={12} container className="GridStyle FormStyle">
        {renderCurrencyTypeField({ id: "minAmountDue", value: "", label: "Min", name: "MinAmountDue" })}
        {renderCurrencyTypeField({ id: "maxAmountDue", value: "", label: "Max", name: "MaxAmountDue" })}
      </Grid>
    </TabPanel>
  }

  return (
    <Fragment>
      {renderReviewLevelList()}
      {renderDecisionsTab()}
      {renderParcelIdTab()}
      {renderOwnerTab()}
      {renderPropertyAddressTab()}
      {renderPropertyCityTab()}
      {renderPropertyZipCodeTab()}
      {renderLandUseCodeTab()}
      {renderInternalLandUseCode()}
      {renderGeneralLandUseCode()}
      {renderAssessedValue()}
      {renderAmountDueValue()}
    </Fragment>
  )

}