import React from "react"
import { a11Props } from "../common/utils"
import { Scrollbars } from 'react-custom-scrollbars';
import {
  Typography, Tabs, Tab
} from '@material-ui/core';

export const ReviewFilterTab = (props) => {
  return <Scrollbars
    style={{ height: 305 }}
    renderTrackVertical={propsTab => <div {...propsTab} className="track-vertical track-v-r8" />}>
    <Typography component="div">
      <Tabs
        orientation="vertical"
        value={props.value}
        onChange={props.handleChange}
        aria-label="Vertical tabs view"
        className="verTabs"
      >
        <Tab label="Levels" {...a11Props(0)} />
        <Tab label="Decisions" {...a11Props(1)} />
        <Tab label="Parcel ID" {...a11Props(2)} />
        <Tab label="Owner" {...a11Props(3)} />
        <Tab label="Property Address" {...a11Props(4)} />
        <Tab label="Property City" {...a11Props(5)} />
        <Tab label="Property Zip Code" {...a11Props(6)} />
        <Tab label="Land Use Code" {...a11Props(7)} />
        <Tab label="Internal Land Use Code" {...a11Props(8)} />
        <Tab label="General Land Use Code" {...a11Props(9)} />
        <Tab label="Assessed Value" {...a11Props(10)} />
        <Tab label="Amount Due" {...a11Props(11)} />
      </Tabs>
    </Typography>
  </Scrollbars>
}