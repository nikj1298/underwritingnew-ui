import React from "react"
import { Table, TableBody, TableCell, TableHead, TableRow, Paper, Link } from '@material-ui/core';
import { Scrollbars } from 'react-custom-scrollbars';
import LoaderComponent from "../common/loaderComponent"
import { eventPropertiesHeader } from "../../common/const"
import { NoRecordFound } from '../common/noRecordFound'
import { formatter, getAlteredString } from "../common/utils"
import { ArrowTooltip } from '../common/tooltipComponents'

export const ReviewPropertyTableComponent = (props) => {
  const { getPropertiesList } = props

  /**
   * return className for current decision of property
   */
  const currentDecision = (currentDecisionName) => {
    if (currentDecisionName) {
      switch (currentDecisionName.toLowerCase()) {
        case 'reject':
          return "Reject"
        case 'approve':
          return 'Approve'
        case 'research':
          return "Research"
        default:
          return "Approve"
      }
    }
  }



  /**
 * renders complete table data
 */
  const renderTableCells = () => {
    return <TableBody>
      {getPropertiesList.getPropertiesList ?
        getPropertiesList.getPropertiesList.map((row, i) => (
          <TableRow key={row.Id}>
            <TableCell >
              <ArrowTooltip title={row.Lead.Name} placement="bottom">
                <Link>{row.Lead ? getAlteredString(row.Lead.Name, 25) : "-"}</Link>
              </ArrowTooltip>
            </TableCell>
            <TableCell>
              {row.ParcelId}
            </TableCell>
            <TableCell>
              {row.Address && row.Address.Address1 ?
                <ArrowTooltip title={row.Address.Address1} placement="bottom">
                  <span>{getAlteredString(row.Address.Address1, 20)}</span></ArrowTooltip> : "-"}
            </TableCell>
            <TableCell>
              {row.AppraisedValue ? formatter.format(row.AppraisedValue) : "$ 0.00"}
            </TableCell>
            <TableCell>
              {row.AmountDue ? formatter.format(row.AmountDue) : "$ 0.00"}
            </TableCell>
            <TableCell>
              {row.Ltv ? parseFloat((row.Ltv) * 100).toFixed(2) + " %" : "0.00 %"}
            </TableCell>
            <TableCell>
              {row.RuLtv ? parseFloat((row.RuLtv) * 100).toFixed(2) + " %" : "0.00 %"}
            </TableCell>
            <TableCell>
              {row.LandUseCode ?
                <ArrowTooltip title={row.LandUseCode} placement="bottom">
                  <span>
                    {getAlteredString(row.LandUseCode, 25)}
                  </span>
                </ArrowTooltip> : "-"}
            </TableCell>
            <TableCell>
              {row.CurrentDecision ?
                (<span className={'statusBtn color-' + currentDecision(row.CurrentDecision.Name)}>
                  {row.CurrentDecision.Name}
                </span>) :
                (<div align="center" style={{ width: 150 }}>{'-'}</div>)
              }
            </TableCell>
          </TableRow>
        )) : <LoaderComponent />}
    </TableBody>
  }

  /**
   * renders header of table
   */
  const renderTableHeader = () => {
    return <TableHead>
      <TableRow>
        {eventPropertiesHeader.map((item, index) => {
          return <TableCell key={"col" + index} align="left">
            <div>
              <span>
                {item.name}
              </span>
            </div>
          </TableCell>
        })
        }
      </TableRow>
    </TableHead>
  }
  return <Scrollbars
    autoHeight
    autoHeightMin={'100%'}
    autoHeightMax={'100%'}
    renderTrackHorizontal={prop => <div {...prop} className="track-horizontal" />}
    thumbSize={300}>
    <div className="fui-wrapper">
      <Paper className="alineaTable">{
        !props.showLoaderStatus && getPropertiesList && getPropertiesList.getPropertiesList && !getPropertiesList.getPropertiesList.length ?
          <NoRecordFound /> :
          <Table>
            {renderTableHeader()}
            {!props.showLoaderStatus && getPropertiesList && getPropertiesList.getPropertiesList && getPropertiesList.getPropertiesList.length ?
              renderTableCells() : <LoaderComponent />}
          </Table>
      }

      </Paper>
    </div>
  </Scrollbars>
}