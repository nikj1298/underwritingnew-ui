import React from 'react'
import '../common-components.css'
// import ActivityReportDialog from './../Events/eventActivityReportDialog'
// import ReviewReportDialog from './../Events/eventReviewReportDialog'
import Button from '@material-ui/core/Button';
import CalIcon from '../../assets/calendar-icon.svg';

export const SubHeader = (props) => {
	const { recordDetailsById } = props

	const renderSubHeader = (recordDetailById) => {
		return (
			<div className="subHeader">
				<div className="leftContent">
					<div className="subHeadingtext">
						<div className="subheadersubHeadingText">Event</div>
						<div className="subHeadingDatatext">
							{
								recordDetailById && recordDetailById.AccountName ?
									recordDetailsById.AccountName : 'Test'}

						</div>
					</div>

				</div>
				<div className="rightContent">
					<div className="actionSection">
						{/* <span className="subHeaderIcon">
							<ActivityReportDialog />
						</span>
						<span className="subHeaderIcon">
							<ReviewReportDialog />
						</span> */}
						<span className="subHeaderIcon">
							<Button aria-controls="table-actionlist" aria-haspopup="true">
								<img src={CalIcon} alt="Calender" />
							</Button>
						</span>
					</div>
				</div>
			</div>
		)
	}
	return <div className="subHeaderWrap">{
		renderSubHeader(recordDetailsById)}
	</div>
}
