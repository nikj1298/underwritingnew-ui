import React, { Fragment } from "react"
import { Popover } from '@material-ui/core';
import PopupState, { bindPopover } from 'material-ui-popup-state';
import { FilterDialogIcon } from "./filterComponent"

export const AdvanceFilterDialogPopup = (props) => {
  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {popupState => (
        <Fragment>
          <FilterDialogIcon popupState={popupState} />
          <Popover
            {...bindPopover(popupState)}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
          >
            {props.renderDialog(popupState)}
          </Popover>

        </Fragment>
      )}
    </PopupState>
  );
}