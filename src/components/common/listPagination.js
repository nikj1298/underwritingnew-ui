import React, { Fragment } from "react"
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

export const ListPagination = (props) => {
  const { listPageNumber, listTotalCount, searchParam, listPageSize, updateProps } = props
  let updatedSearchParam = Object.assign({}, searchParam)
  const [anchorEl, setAnchorEl] = React.useState(null);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }
  function handleClose() {
    setAnchorEl(null);
  }

  /**
   * handles navigation to first page of the list
   */
  const handleFirstPage = () => {
    updatedSearchParam.offset = 0
    updateProps(updatedSearchParam, 1)
    handleClose();
  }
  /**
   * handles navigation to last page of the list
   */
  const handleLastPage = () => {
    const pageNumber = Math.ceil(listTotalCount / listPageSize);
    updatedSearchParam.offset = (pageNumber - 1) * listPageSize
    updateProps(updatedSearchParam, pageNumber)
    handleClose();
  }

  /**
  * handles click funtcion for going on prev page in pagination
  */
  const previousPage = () => {
    if (listPageNumber !== 1) {
      updatedSearchParam.offset = (listPageNumber - 2) * listPageSize
      updateProps(updatedSearchParam, listPageNumber - 1)
    }
  }
  /**
  * handles click funtcion for going on next page in pagination
  */
  const nextPage = () => {
    if (listPageSize < listTotalCount && ((listTotalCount / listPageSize) > listPageNumber)) {
      updatedSearchParam.offset = listPageNumber * listPageSize
      updateProps(updatedSearchParam, listPageNumber + 1)
    }
  }

  return <span className="paginationStyle">
    <span className="pageCount">
      <b>{(listPageSize * (listPageNumber - 1)) + 1} - {((listPageNumber * listPageSize) > listTotalCount) ? (
        listTotalCount
      ) : (
          listPageSize * listPageNumber
        )
      }
      </b> of {listTotalCount}
      <Fragment>
        {props.HidePageDropdown ? "" : <Button id='expandMoreIcon' aria-controls="table-pagerlist" aria-haspopup="true" onClick={handleClick}>
          <Icon>expand_more</Icon>
        </Button>}
        <Menu
          id="pageChanger"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          getContentAnchorEl={null}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          transformOrigin={{ vertical: "top", horizontal: "right" }}
        >
          <MenuItem onClick={handleFirstPage}>First Page</MenuItem>
          <MenuItem onClick={handleLastPage}>Last Page</MenuItem>
        </Menu>
      </Fragment>
    </span>
    <IconButton aria-label="previous page" onClick={previousPage}>
      <Icon>chevron_left</Icon>
    </IconButton>
    <IconButton aria-label="next page" onClick={nextPage}>
      <Icon>chevron_right</Icon>
    </IconButton>
  </span>
}