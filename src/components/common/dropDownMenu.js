import React from 'react'
import { eventsDropdownMenu, reviewPopupUrl } from '../../common/const'
import DropDownIcon from '../../assets/module_drop_down.svg'
import '../common-components.css'
import { getAlteredString } from "./utils"
import { ArrowTooltip } from '../common/tooltipComponents'

export const DropDownMenu = (props) => {
	const { getAllEvents } = props
	let eventName = "", alteredEventName = ""
	const hideOverlay = () => {
		document.getElementById('overLayFixed').style.display = 'none'
		document.getElementById('dropDownRecordMenu').classList.remove('show')
	}
	/**
	 * navigate to selected page from menu
	 * @param {*} event 
	 * @param {*} item 
	 */
	const onNavigate = (event, item) => {
		hideOverlay()
		const underwritingNewIndex = window.location.href.search("/underwritingnew/")
		const requiredSubUrl = window.location.href.slice(underwritingNewIndex)
		props.updateUrl(requiredSubUrl)
		if (requiredSubUrl === reviewPopupUrl && item.navigate === reviewPopupUrl) {
			window.location.reload()
		}
		else {
			props.history.push(item.navigate)
		}
	}
	/**
	 * shows the drop menu on click of down arrow icon
	 */
	const handleShowList = () => {
		document.getElementById('overLayFixed').style.display = 'block'
		document.getElementById('dropDownRecordMenu').classList.add('show')
	}
	const eventIdSelected = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1);
	if (eventIdSelected && getAllEvents && getAllEvents.getAllEvents) {
		const eventData = getAllEvents.getAllEvents.filter((event) => event.Id === eventIdSelected)
		if (eventData && eventData.length) {
			eventName = eventData[0]["Name"]
		}
	}
	if (eventName && eventName.length) {
		alteredEventName = getAlteredString(eventName, 30)
	}
	return (
		<div id="loanListDropMenuId" className="loanListDropMenu">
			<div
				id="loanListButtonId"
				className="loanListButton cursorClass"
				onClick={handleShowList}>
				{props.getPropertiesParam && props.getPropertiesParam.Filter.EventId ?
					<ArrowTooltip title={eventName} placement="bottom">
						<span>
							{(props.menuName + "/" + alteredEventName)}
						</span>
					</ArrowTooltip>
					: props.menuName}
				&nbsp;&nbsp;
				<img id="dropDownMenuId" src={DropDownIcon} alt="Events" />
			</div>
			<ul id="dropDownRecordMenu">
				{eventsDropdownMenu.map((item) => {
					return (
						<li key={item.key} onClick={(event) => onNavigate(event, item)}>
							<span
								className={
									(props.menuName && props.menuName.toLowerCase() === item.icon.toLowerCase()) ||
										(props.menuName &&
											props.menuName.toLowerCase().includes(item.icon.toLowerCase())) ? (
											item.icon + 'Selected icons ' + item.icon
										) : (
											'icons ' + item.icon
										)
								}
							/>
							<span className="textName">{item.name}</span>
						</li>
					)
				})}
			</ul>
			<div id="overLayFixed" onClick={hideOverlay} style={{ display: 'none' }} />
		</div>
	)
}
