import moment from "moment"
export const getInitialsOfName = (name) => {
  let fullName = name.split(' ')
  return fullName[0].charAt(0).toUpperCase() + fullName[fullName.length - 1].charAt(0).toUpperCase()
}
/**
 * returns formatted Date
 */
export const getFormattedDate = (fileStartDate) => {
  const newDate = new Date(fileStartDate)
  return (
    String(newDate.getMonth() + 1).padStart(2, '0') +
    '-' +
    String(newDate.getDate()).padStart(2, '0') +
    '-' +
    newDate.getFullYear()
  )
}

export const a11Props = (index) => {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

/**
 * format date before saving
 */
export const formatDate = (value) => {
  if (value) return moment(value).format(moment.HTML5_FMT.DATE + 'T00:00:00')
  else return null
}

/**
 * check fields validity in review report
 */
export const checkReviewReportValidity = (reviewReportData, validateReviewReportData) => {
  const validateFields = { ...validateReviewReportData }
  if (!reviewReportData.StateId) validateFields.StateId = true
  if (reviewReportData.IsEventLocked !== false) {
    if (!reviewReportData.SaleDateTo) validateFields.SaleDateTo = true
    if (!reviewReportData.SaleDateFrom) validateFields.SaleDateFrom = true
    if (reviewReportData.SaleDateTo && reviewReportData.SaleDateFrom) {
      if (reviewReportData.SaleDateFrom > reviewReportData.SaleDateTo) validateFields.SaleDateFrom = true
      else validateFields.SaleDateFrom = false
    }
  }
  return validateFields
}

/**
 * returns currency in US format
 */
export const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
})

/**
 * get adjustable string to be visible on screen in table
 */
export const getAlteredString = (str, maxLength) => {
  let finalString = str
  if (str.length > maxLength) {
    finalString = str.substr(0, maxLength - 3) + "...";
  }
  return finalString
}