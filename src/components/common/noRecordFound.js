import React from 'react'
import RecordnotFound from '../../assets/not-found.png'

export const NoRecordFound = () => {
  return (
    <div className="noRecordFound">
      <span className="noRecordText">No Record Found</span>
      <img src={RecordnotFound} alt="No Record Found" />
    </div>
  )
}
