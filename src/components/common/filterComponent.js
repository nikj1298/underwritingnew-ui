import React from "react"
import { Button, Typography, Icon, IconButton } from '@material-ui/core';
import FilterIcon from '../../assets/filter_icon.svg';
import { bindTrigger } from 'material-ui-popup-state';
import { ArrowTooltip } from '../common/tooltipComponents'

export const FilterActionButtons = (props) => {
  const { popupState } = props
  return <div className="filterDialogAction">
    <Typography component="div" className="filterBottomButton">
      <Button variant="contained" color="primary"
        className="fab_btn" onClick={(event) => props.applyOrClearFilter(event, popupState, "Apply")} >
        {"Apply"}
      </Button>
      <Button className="fab_btn"
        onClick={(event) => props.applyOrClearFilter(event, popupState, "Clear")}>
        {"Clear Filters"}
      </Button>
    </Typography>
  </div>
}

export const FilterPopupHeader = (props) => {
  return <Typography component="div" className="filterByDiv">
    <Typography component="div" className="filterByFixed">
      {" Filter By"}
      <Typography className="smallText">
        {"Filters will be applied to your grid"}
      </Typography>
      <IconButton className="filterClose" aria-label="close" size="small" onClick={(e) => props.resetFilterLastApplied(e, props.popupState)}>
        <Icon>close</Icon>
      </IconButton>
    </Typography>
  </Typography>
}

export const FilterDialogIcon = (props) => {
  const { popupState } = props
  return <ArrowTooltip title={"Filter"} placement="bottom">
    <Button {...bindTrigger(popupState)}>
      <img src={FilterIcon} alt="filter" />
    </Button>
  </ArrowTooltip>
}