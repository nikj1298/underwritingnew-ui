import MuiDialogTitle from '@material-ui/core/DialogTitle'
import MuiDialogContent from '@material-ui/core/DialogContent'
import MuiDialogActions from '@material-ui/core/DialogActions'
import { withStyles } from '@material-ui/core/styles';

export const styles = theme => ({
  formControl: {
    margin: theme.spacing(0.1, 0),
  },
});
export const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(3)
  }
}))(MuiDialogContent)

export const DialogTitle = withStyles((theme) => ({
  root: {
    padding: '16px 24px !important',
    textTransform: 'uppercase'
  }
}))(MuiDialogTitle)

export const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(0, 3, 3, 3),
    justifyContent: 'flex-start'
  }
}))(MuiDialogActions)