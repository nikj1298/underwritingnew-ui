import React from "react"
import { MenuItem, Select } from '@material-ui/core';

export const DropdownComponent = (props) => {
  const { dropDownList, handleChange, validateData, valueData, name, label } = props
  return <Select
    disabled={props.disabled}
    key={valueData}
    error={validateData}
    onChange={handleChange}
    id="Type"
    label={label}
    MenuProps={{
      anchorOrigin: {
        vertical: "bottom",
        horizontal: "left"
      },
      transformOrigin: {
        vertical: "top",
        horizontal: "left"
      },
      getContentAnchorEl: null
    }}
    name={name}
    value={valueData}
  >
    {dropDownList.map((dropdownItem) => {
      return <MenuItem key={dropdownItem.Id} value={dropdownItem.Id}>{dropdownItem.Name}</MenuItem>
    })}
  </Select>
}