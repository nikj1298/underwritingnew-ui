import React, { Fragment } from 'react'
import TextField from '@material-ui/core/TextField'
import SearchIcon from '../../assets/search.svg'
import Icon from '@material-ui/core/Icon'
import Button from '@material-ui/core/Button'

export const SearchBarComponent = (props) => {

	/**
   * handles change in search input text field
   */
  function handleChange(event) {
    props.setShowCross(true)
    props.handleInputValue(event.target.value)
    if (event.target.value.trim().length === 0 && props.listTotalCount === 0) {
      props.clearGlobalSearch()
    }
  }
  return (
    <Fragment>
      <span className="searchInput">
        <span className="searchIcon">
          <img id="SearchImg" src={SearchIcon} alt="search" onClick={props.searchInputText} />
        </span>
        <TextField
          id="filled-dense-hidden-label"
          margin="dense"
          hiddenLabel
          variant="filled"
          value={props.inputValue}
          type="text"
          placeholder="SEARCH"
          onChange={handleChange}
          inputProps={{ maxLength: 100 }}
          onKeyPress={(ev) => {
            if (ev.key === 'Enter') {
              props.searchInputText()
            }
          }}
        />
        {props.showCross === true && (
          <span id="clearSearch" className="clearIcon">
            <Button id="clearButton" onClick={props.clearGlobalSearch}>
              <Icon id="clearSearchIcon" alt="Clear" >
                close
							</Icon>
            </Button>
          </span>
        )}
      </span>
    </Fragment>
  )
}