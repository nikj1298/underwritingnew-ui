export const ROOT_URL = localStorage.getItem('underwritingApi')

export const AUTH_URL = localStorage.getItem('authApi')

export const LANDING_URL = localStorage.getItem('landingApi')

export const ADMIN_URL = localStorage.getItem('adminApi')

export const WORKFLOW_URL = localStorage.getItem('workflowsApi')

export const NOTIFICATION_URL = localStorage.getItem('notificationApi')