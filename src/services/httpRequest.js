import axios from 'axios'
import { ROOT_URL, AUTH_URL, LANDING_URL, ADMIN_URL, WORKFLOW_URL, NOTIFICATION_URL } from './appConfig'
import { getSessionToken, getRefreshToken, logout } from '../common/requireAuth'
import { getNotificationAction } from '../common/notification'
import FileSaver from 'file-saver';

let refreshTokenApiCalled = false

// Add a response interceptor
axios.interceptors.response.use(
	async function (response) {
		// Any status code that lie within the range of 2xx cause this function to trigger
		// Do something with response data
		refreshTokenApiCalled = false
		if (response.status === 202) {
			const id = response.headers['x-correlation-id']
			const result = await getNotificationAction(id)
			if (result) {
				sessionStorage.setItem('notification', JSON.stringify(result.data))
			}
		}
		return response
	},
	function (error) {
		const originalRequest = error.config
		// Any status codes that falls outside the range of 2xx cause this function to trigger
		// Do something with response error
		if (error.message.indexOf(401) > 0 || error.message.indexOf(404) > 0) {
			if (error.message.indexOf(404) > 0 && originalRequest.url.includes('api/token')) {
				// alert("session expired. You will be redirected to pingone .. 1")
				console.log('in error')
				logout()
				return
			} else if (!refreshTokenApiCalled) {
				refreshTokenApiCalled = true
				getRefreshToken().then((res) => {
					error.config.headers['Authorization'] = 'Bearer ' + res.token
					axios.request(error.config)
					window.location.reload()
				})
			}
		}
	}
)

/**
 * action for authentication http request for notification
 */
export function doActionNotification(config) {
	const token = sessionStorage.currentUser ? getSessionToken(sessionStorage.currentUser) : null

	if (config) {
		return axios({
			url: `${NOTIFICATION_URL}/` + config.url,
			method: 'GET',
			headers: {
				Authorization: token,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
 * action for authentication http request for get
 */
export function doAuthActionGet(config) {
	if (config) {
		return axios({
			url: `${AUTH_URL}/` + config.url,
			method: 'GET'
		})
	}
}

/**
* action for http request for post
* @param {*} config
*/
export function doActionPost(config) {
	const token = sessionStorage.currentUser ? getSessionToken(sessionStorage.currentUser) : null
	if (config) {
		return axios({
			url: `${ROOT_URL}/` + config.url,
			method: 'POST',
			data: config.data,
			headers: {
				Authorization: token,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
* action for http request for post
* @param {*} config
*/
export function doActionPatch(config) {
	if (config) {
		return axios({
			url: `${ROOT_URL}/` + config.url,
			method: 'PATCH',
			data: config.data
		})
	}
}

/**
* action for http request for post
* @param {*} config
*/
export function doActionPut(config) {
	const token = sessionStorage.currentUser ? getSessionToken(sessionStorage.currentUser) : null
	if (config) {
		return axios({
			url: `${ROOT_URL}/` + config.url,
			method: 'PUT',
			data: config.data,
			headers: {
				Authorization: token,
				'Content-Type': 'application/json'
			}
		})
	}
}

/**
* action for http request for get
* @param {*} config
*/
export function doActionGet(config) {
	const token = sessionStorage.currentUser ? getSessionToken(sessionStorage.currentUser) : null
	if (config) {
		return axios({
			url: `${ROOT_URL}/` + config.url,
			method: 'GET',
			headers: { Authorization: token }
		})
	}
}

/**
* action for http request for delete
* @param {*} config
*/
export function doActionDelete(config) {
	const token = sessionStorage.currentUser ? getSessionToken(sessionStorage.currentUser) : null
	if (config) {
		return axios({
			url: `${ROOT_URL}/` + config.url,
			method: 'DELETE',
			data: config.data,
			headers: {
				Authorization: token
			}
		})
	}
}

/**
* action for http request for get for landing url
* @param {*} config
*/
export function doActionGetLanding(config) {
	const token = sessionStorage.currentUser ? getSessionToken(sessionStorage.currentUser) : null
	if (config) {
		return axios({
			url: `${LANDING_URL}/` + config.url,
			method: 'GET',
			headers: { Authorization: token }
		})
	}
}

/**
 * action for http request of get for admin url
 */
export function doActionGetAdmin(config) {
	const token = sessionStorage.currentUser ? getSessionToken(sessionStorage.currentUser) : null
	if (config) {
		return axios({
			url: `${ADMIN_URL}/` + config.url,
			method: 'GET',
			headers: { Authorization: token }
		})
	}
}

/**
 * action for http request of get for admin url
 */
export function doActionWorkflow(config) {
	const token = sessionStorage.currentUser ? getSessionToken(sessionStorage.currentUser) : null
	if (config) {
		return axios({
			url: `${WORKFLOW_URL}/` + config.url,
			method: 'GET',
			headers: { Authorization: token }
		})
	}
}

/**
 * action for http request of export data download
 */
export function doActionGetDownload(config) {
	if (config) {
		axios.get(config.Url, {
			responseType: 'blob',
		}).then(response => {
			const filename = config.Path.replace(/^.*[\\\/]/, '');
			FileSaver.saveAs(response.data, filename);
			return response
		});
	}
}